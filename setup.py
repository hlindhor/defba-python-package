#!/usr/bin/env python
"""
Distutils based setup script for defba package.

This uses Distutils (http://python.org/sigs/distutils-sig/) the standard
python mechanism for installing packages. For the easiest installation
just type the command (you'll probably need root privileges for that):

    python setup.py install --user

This will install the library in the default location. For instructions on
how to customize the install procedure read the output of:

    python setup.py --help install

To get a full list of avaiable commands, read the output of:

    python setup.py --help-commands
"""

from distutils.core import setup;

setup(name = 'pydefba',
      version = '0.1',
      description='import/export of deFBA models from SBML. Solving deFBA, sdeFBA, rdeFBA and RBA problems with either gurobi, cplex, or'
                  ' cvxopt.',
      author='Henning Lindhorst',
      author_email='henning.lindhorst@ovgu.de',
      url='https://bitbucket.org/hlindhor/defba-python-package',
      packages = ['defba'], requires=['numpy', 'scipy', 'cvxopt', 'libsbml']
      ) 

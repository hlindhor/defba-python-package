##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
from __future__ import division
import libsbml as sbml
import numpy as np

class RamError(Exception):
    """
    empty error class to state that something with the import of the RAM annotations gone wrong
    """
    pass

def writeSBML(deFBAmodel, documentname):
    document = createModel(deFBAmodel, documentname)
    return sbml.writeSBMLToFile(document, documentname + '.xml')


def createModel(deFBAmodel, name):
    sbmlns = sbml.SBMLNamespaces(3, 1, "fbc", 2)
    document = sbml.SBMLDocument(sbmlns)
    document.setPackageRequired("fbc", False)
    model = document.createModel()
    model.setId(name)
    model.setName(name)
    mplugin = model.getPlugin("fbc")
    mplugin.setStrict(False)
    note = '<body xmlns="http://www.w3.org/1999/xhtml"><p>This model uses the ram standard 1.0</p></body>'
    model.setNotes(note)
    stoich = deFBAmodel.stoich
    zero = model.createParameter()
    zero.setId('zero')
    zero.setValue(0.0)
    zero.setConstant(True)
    # maybe addd compartment from the original model? Simply using a dict species:compartment could do the trick.
    try:
        compartments = list(set(deFBAmodel.compartments.values()))
    except AttributeError:
        compartments = ['external', 'cytosol']
    if not compartments:
        compartments = ['external', 'cytosol']
    for com in compartments:
        c1 = model.createCompartment()
        c1.setId(com)
        c1.setConstant(True)
        c1.setSize = 1
        c1.setName('no information available')
    weight_counter = 0
    objective_counter = 0
    percentage_counter = 0
    for number, spec in enumerate(deFBAmodel.species):
        speciesType = ''
        s1 = model.createSpecies()
        s1.setId(spec)
        s1.setHasOnlySubstanceUnits(True)
        is_biomass_species = False
        s1.setConstant(False)
        if number < deFBAmodel.numbers['ext_species']:
            speciesType = 'extracellular'
            s1.setBoundaryCondition(False)
            try:
                s1.setCompartment(deFBAmodel.compartments[spec])
            except (AttributeError, KeyError) as e:
                s1.setCompartment('external')
            if deFBAmodel.external_conditions:
                s1.setInitialAmount(deFBAmodel.external_conditions[number])
        elif number < deFBAmodel.numbers['ext_species'] + deFBAmodel.numbers['metab_species']:
            speciesType = 'metabolite'
            s1.setBoundaryCondition(False)
            s1.setInitialAmount(0)
            try:
                s1.setCompartment(deFBAmodel.compartments[spec])
            except (AttributeError, KeyError) as e:
                s1.setCompartment('cytosol')
        else:
            s1.setBoundaryCondition(False)
            if len(deFBAmodel.initial_biomass) == deFBAmodel.numbers['proteins']:
                s1.setInitialAmount(deFBAmodel.initial_biomass[number - deFBAmodel.numbers['ext_species'] - deFBAmodel.numbers['metab_species']])
            try:
                s1.setCompartment(deFBAmodel.compartments[spec])
            except (AttributeError, KeyError) as e:
                s1.setCompartment('cytosol')
            is_biomass_species = True
        # identify species type
        if deFBAmodel.storage_species is not None:
            if spec in deFBAmodel.storage_species:
                speciesType = 'storage'
        if deFBAmodel.biomass[number] != 0:
            weight_string = 'weight_' + str(weight_counter)
            weight_counter += 1
            param = model.createParameter()
            param.setId(weight_string)
            param.setConstant(True)
            param.setValue(deFBAmodel.biomass[number])
        elif is_biomass_species is True:
            weight_string = 'zero'
        elif is_biomass_species is False:
            weight_string = ''

        if deFBAmodel.objective_vector[number] != 0:
            objective_string = 'ob_weight_' + str(objective_counter)
            objective_counter += 1
            ob_param = model.createParameter()
            ob_param.setId(objective_string)
            ob_param.setConstant(True)
            ob_param.setValue(deFBAmodel.objective_vector[number])
        else:
            objective_string = 'zero'

        construction_string = '<ram:RAM xmlns:ram="https://www.fairdomhub.org/sops/304"><ram:species ram:molecularWeight="' + weight_string + '" ram:objectiveWeight="' + objective_string + '" ram:biomassPercentage="'
        try:
            relevant_coloumn = deFBAmodel.HB[:, number]
            relevant_entries = relevant_coloumn[relevant_coloumn < 0]
        except IndexError:
            relevant_entries = np.array([])
        if relevant_entries.any():
            param = model.createParameter()
            param.setId('bioPercent_' + str(percentage_counter))
            param.setConstant(True)
            if (relevant_entries[0] / deFBAmodel.biomass[number]) + 1 > 1 or (relevant_entries[0] / deFBAmodel.biomass[number]) + 1 < 0:
                raise RamError('The biomass percentage value for ' + spec + ' could not be identified. Was HB scaled?')
            param.setValue((relevant_entries[0] / deFBAmodel.biomass[number]) + 1)
            construction_string += 'bioPercent_' + str(percentage_counter) + '" ram:speciesType="quota"/></ram:RAM>'
            percentage_counter += 1
        elif speciesType == 'storage':
            construction_string += 'zero" ram:speciesType="storage"/></ram:RAM>'
        elif is_biomass_species is True:
            construction_string += 'zero" ram:speciesType="enzyme"/></ram:RAM>'
        else:
            construction_string += 'zero" ram:speciesType="' + speciesType + '"/></ram:RAM>'
        s1.setAnnotation(construction_string)
    kcat_counter = 0
    scaling_counter = 0
    label_counter = 0
    for coloumn_index, reac in enumerate(deFBAmodel.reactions):
        gene_association = None
        r1 = model.createReaction()
        r1.setId(reac)
        if coloumn_index < deFBAmodel.numbers['exch_reactions'] or coloumn_index > deFBAmodel.numbers['exch_reactions'] + \
                deFBAmodel.numbers['metab_reactions']:
            r1.setFast(False)
        else:
            r1.setFast(True)
        try:
            non_zero_position = np.nonzero(deFBAmodel.HC[:, coloumn_index])[0][0]
            gene_ass = True
        except IndexError:
            kforward_string = ''
        try:
            if gene_ass:
                kforward = deFBAmodel.HC[:, coloumn_index][deFBAmodel.HC[:, coloumn_index] > 0][0]
                kforward = kforward ** -1
                kforward_string = 'kcat_' + str(kcat_counter)
                kcat_counter += 1
                param = model.createParameter()
                param.setId(kforward_string)
                param.setValue(kforward)
                param.setConstant(True)
                gene_association = deFBAmodel.species[np.nonzero(deFBAmodel.HE[non_zero_position, :])[0][0]]
        except IndexError:
            raise UnboundLocalError('No forward kcat value for reaction ' + reac + ' detected. The model is faulty.')
        gene_ass = False
        if reac in deFBAmodel.reversible:
            r1.setReversible(True)
            try:
                kbackward = -deFBAmodel.HC[:, coloumn_index][deFBAmodel.HC[:, coloumn_index] < 0][0]
                kbackward = kbackward ** -1
                kbackward_string = 'kcat_' + str(kcat_counter)
                kcat_counter += 1
                param = model.createParameter()
                param.setConstant(True)
                param.setId(kbackward_string)
                param.setValue(kbackward)
            except IndexError:
                if kforward_string:
                    kbackward_string = 'zero'
                else:
                    kbackward_string = ''
        else:
            r1.setReversible(False)
            kbackward_string = 'zero'
        annotation_string = '<ram:RAM xmlns:ram="https://www.fairdomhub.org/sops/304"><ram:reaction ram:kcatForward="' + kforward_string + '" ram:kcatBackward="' + kbackward_string + '" ram:maintenanceScaling="'
        if deFBAmodel.HM.any():
            if deFBAmodel.HM[:, coloumn_index].any():
                main_percent = 1.0 / (deFBAmodel.HM[:, coloumn_index][deFBAmodel.HM[:, coloumn_index] > 0][0])
                scaling_string = 'main_scale_' + str(scaling_counter)
                scaling_counter += 1
                annotation_string += str(scaling_string) + '"/></ram:RAM>'
                param = model.createParameter()
                param.setId(scaling_string)
                param.setValue(main_percent)
                param.setConstant(True)
            else:
                annotation_string += 'zero"/></ram:RAM>'
        else:
            annotation_string += 'zero"/></ram:RAM>'
        r1.setAnnotation(annotation_string)
        stoich_coloumn = stoich[:, coloumn_index]
        for specie_index in xrange(len(stoich_coloumn)):
            if stoich_coloumn[specie_index] > 0:
                productRef = r1.createProduct()
                productRef.setSpecies(deFBAmodel.species[specie_index])
                productRef.setStoichiometry(stoich_coloumn[specie_index])
                productRef.setConstant(True)
            if stoich_coloumn[specie_index] < 0:
                reactantRef = r1.createReactant()
                reactantRef.setSpecies(deFBAmodel.species[specie_index])
                reactantRef.setStoichiometry(-stoich_coloumn[specie_index])
                reactantRef.setConstant(True)
        if gene_association:
            rfbc = r1.getPlugin('fbc')
            geneProductAssociation = rfbc.createGeneProductAssociation()
            geneProductAssociation.setId(gene_association)
            geneRef = geneProductAssociation.createGeneProductRef()
            geneRef.setGeneProduct(gene_association)
            r1.connectToChild()
            if not mplugin.getGeneProduct(gene_association):
                geneProd = mplugin.createGeneProduct()
                geneProd.setId(gene_association)
                try:
                    given_label = deFBAmodel.gene_labels[gene_association]
                except TypeError:
                    given_label = '1*' + gene_association
                geneProd.setLabel(given_label)
                geneProd.setAssociatedSpecies(gene_association)
                model.connectToChild()
                label_counter += 1
    return document

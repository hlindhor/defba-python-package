##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
from __future__ import division
import sbmlimport
import defbamodel
import numpy as np

try:
    import libsbml as sbml

    have_sbml = True
except ImportError, err:
    have_sbml = False
    sbml_import_msg = str(err)


def gene_knockout(filename, gene_ids, generate_new_SBML=True, new_filename=""):
    """
    using given geneIds this script finds all related species and reactions.

    :param filename: name of the SBML file
    :param gene_ids: list of gene ids given as string
    :param generate_new_SBML: boolean trigger. True creates a new SBML file, representing the knockout mutant
    :return: species_list, reactions_list lists containing the species and the reactions to be deleted from the model
    """
    reader = sbml.SBMLReader()
    document = reader.readSBML(filename)
    if document.isSetModel():
        # species and reactions which are to be deleted are collected in these lists
        reactions_list = []
        species_list = []
        model = document.getModel()
        fbc_model = model.getPlugin('fbc')
        for reac in (model.getReaction(i) for i in xrange(model.getNumReactions())):
            reac_id = reac.getId()
            reaction_fbc = reac.getPlugin('fbc')
            gene_composition = ''
            try:
                gene_product_id = reaction_fbc.getGeneProductAssociation().all_elements[0].getGeneProduct()
                gene_product = fbc_model.getGeneProduct(gene_product_id)
                gene_composition = gene_product.getLabel()
                associated_species = gene_product.getAssociatedSpecies()
                if not associated_species:
                    associated_species = gene_product_id
            except (AttributeError, IndexError) as e:
                pass
            if gene_composition:
                for gene_id in gene_ids:
                    if not gene_id[0] == "*":
                        gene_id = "*" + gene_id
                    if not gene_id[-1] == " ":
                        gene_id += " "
                    if not gene_composition[-1] == " ":
                        gene_composition += " "
                    if gene_id in gene_composition:
                        reactions_list.append(reac_id)
                        if not associated_species in species_list:
                            species_list.append(associated_species)
                        break
        # we need a second round thorugh all reactions to check for the ones producing the associated species
        for reac in (model.getReaction(i) for i in xrange(model.getNumReactions())):
            reac_id = reac.getId()
            for product in reac.getListOfProducts():
                if product.getSpecies() in species_list:
                    if reac_id not in reactions_list:
                        reactions_list.append(reac_id)
            for reactant in reac.getListOfReactants():
                if reactant.getSpecies() in species_list:
                    if reac_id not in reactions_list:
                        reactions_list.append(reac_id)
        if generate_new_SBML:
            if not new_filename:
                new_filename = ('.').join(filename.split('.')[:-1]) + '_knockout'
                for gene_id in gene_ids:
                    new_filename += '_' + gene_id
                new_filename += '.xml'
            elif '.xml' not in new_filename:
                new_filename += '.xml'
            for reac_id in reactions_list:
                model.removeReaction(reac_id)
            for specie in species_list:
                model.removeSpecies(specie)
            sbml.writeSBMLToFile(document, new_filename)
        return species_list, reactions_list
    else:
        raise ValueError('The SBML file seems broken. Please check via a SBML validator.')

def delete_isoenzymes(filename, new_filename=None, eps=10**-15, solver='gurobi'):
    """
    WARNING THIS METHOD IS STILL EXPERIMENTAL
    This method is used to generate a new model file in which all unnecessary isoenzyme are deleted.
    While the moethod in itself is working, some models show a no-growth behavior after deletion of the isoenzymes.

    :param filename: string containing the name of the original SBML file
    :param new_filename:  string containing of the new SBML file. Chosen automatically if not set.
    :param eps: Decide for the cutoff reacion rate.
    :param solver: 'gurobi', 'cvxopt', 'cplex', or 'soplex'
    :return: 0 if no reactions can be deleted, [deleted_reactions], [deleted species] otherwise
    """
    model = sbmlimport.readSBML(filename)
    if new_filename is None:
        new_filename=filename + '_deleted_reactions'
    # generate all possible external conditions
    scenarios = np.zeros((2**model.numbers['ext_species'],model.numbers['ext_species']))
    for i in range(1, 2 ** model.numbers['ext_species']):
        if i % 2 == 1:
            scenarios[i, 0] = 1
    blocklength = 2
    for j in range(1,model.numbers['ext_species']):
        for i in range(0,2**model.numbers['ext_species']):
            if (i) % (blocklength*2) < 2**j:
                scenarios[i,j]= 0
            else:
                scenarios[i,j]= 1
        blocklength = blocklength * 2
    # There are 2**ext_species number of scenarios.
    # We want to check whether an isoenzyme is expressed in none of the environments
    results = np.zeros((2**model.numbers['ext_species'], len(model.reactions)))
    for i in range(2**model.numbers['ext_species']):
        initial_biomass_vec, initial_fluxes, mu = model.RBA(1, scenarios[i,:], solver=solver)
        if mu> 0:
            results[i,:] = initial_fluxes
        else:
            results[i,:] = np.zeros(len(model.reactions))
    reactions_to_delete = []
    metab_reactions = []
    species_to_delete = []
    # Check if any metabolic reactions are zero in all solutions
    for j, name in enumerate(model.reactions[model.numbers['exch_reactions']:model.numbers['exch_reactions']+model.numbers['metab_reactions']]):
          if all(-eps < v < eps for v in results[:,j]):
              metab_reactions.append(name)
    # check if the metabolic reaction has multiple isoenzymes catalyzing it
    # This means there must exists other reactions with identical stoichiometry
    remove_reactions = []
    for reac in metab_reactions:
        found_copy = False
        index = model.reactions.index(reac)
        compare_this = model.stoich[:,index]
        for i in range(model.stoich.shape[1]):
            if i != index:
                if np.array_equal(model.stoich[:, i], compare_this):
                    if reac in model.reversible:
                        if model.reactions[i] in model.reversible:
                            found_copy = True
                    else:
                        if model.reactions[i] not in model.reversible:
                            found_copy = True
        if not found_copy:
            metab_reactions.remove(reac)
    for reac in remove_reactions:
        metab_reactions.remove(reac)
    if not metab_reactions:
        # If no metabolic reactions can deleted stop the method
        print 'No reactions / isoenzymes can be deleted as all are used in some form.'
        return 0
    else:
        number_of_enzymes = 0
        number_of_protein_reactions = 0
        # check if we can eliminate some enzymes
        for enzyme in model.enzyme_catalyzes:
            if len(model.enzyme_catalyzes[enzyme]) == 1:
                if model.enzyme_catalyzes[enzyme][0] in metab_reactions:
                    nonzeros = model.stoich[model.species.index(enzyme), :].nonzero()[0]
                    if len(nonzeros) == 1:
                        species_to_delete.append(enzyme)
                        number_of_protein_reactions += 1
                        reactions_to_delete.append(model.reactions[nonzeros[0]])
                        number_of_enzymes += 1
        number_of_metab_reactions = 0
        for enzyme in species_to_delete:
            for reac in model.enzyme_catalyzes[enzyme]:
                if reac not in reactions_to_delete:
                    reactions_to_delete.append(reac)
                    number_of_metab_reactions += 1
        reader = sbml.SBMLReader()
        document = reader.readSBML(filename)
        model = document.getModel()
        for reac_id in reactions_to_delete:
            model.removeReaction(reac_id)
        for species_id in species_to_delete:
            model.removeSpecies(species_id)
        sbml.writeSBMLToFile(document, new_filename)
        print 'New SBML file written to ' + new_filename + '.xml'
        format_list = [str(number_of_enzymes),str(number_of_protein_reactions), str(number_of_metab_reactions)]
        print "Deleted {} enzymes, {} protein reactions, and {} metabolic reactions".format(*format_list)
        return reactions_to_delete, species_to_delete
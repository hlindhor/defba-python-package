##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################

"""
a package for the analysis of biochemical reaction networks

methods imported from modules brn and sbmlimport:
defba.DefbaModel - class for biochemical reaction networks in deFBA standard 1.0
defba.readSBML - import a DefbaModel from an SBML file with defba extension
"""

__all__ = ["defba"]

import defbamodel
import sbmlimport
DefbaModel = defbamodel.DefbaModel
readSBML = sbmlimport.readSBML

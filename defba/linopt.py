##################################################################
# Copyright (C) 2015 Steffen Waldherr <steffen.waldherr@kuleuven.be>
#               2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################

"""
Module ltiopt.py: Dynamic optimization of LTI systems
"""
# Time-stamp: <Last change 2015-06-18 15:48:00 by Steffen Waldherr>

from __future__ import division

import operator
from copy import copy
import warnings
from itertools import product

import numpy as np
from numpy.polynomial.legendre import legroots, legder, legval
from scipy import sparse
import scipy.linalg as scila
from scipy import integrate
from scipy import optimize
from scipy.sparse import csr_matrix

class Store(object):
    pass

globals = Store()

try:
    import cvxopt
    import cvxopt.solvers
    globals.have_cvxopt = True
except ImportError as err:
    globals.have_cvxopt = False
    globals.cvxopt_err = str(err)

try:
    import cplex
    globals.have_cplex = True
except ImportError as err:
    globals.have_cplex = False
    globals.cplex_err = str(err)

try:
    import gurobipy as grb
    globals.have_gurobi = True
except ImportError as err:
    globals.have_gurobi = False
    globals.gurobi_err = str(err)

try:
    import pyscipopt as soplex
    globals.have_soplex = True
except ImportError as err:
    globals.have_soplex = False
    globals.soplex_err = str(err)

def unitvector(n, i):
    uv = np.zeros(n)
    uv[i] = 1.0
    return uv

def block_diagonal(arrays, sparseformat=None):
    if sparseformat is None:
        res = np.zeros((sum(a.shape[0] for a in arrays), sum(a.shape[1] for a in arrays)))
        for i,a in enumerate(arrays):
            ind0 = sum(ai.shape[0] for ai in arrays[:i])
            ind1 = sum(ai.shape[1] for ai in arrays[:i])
            res[ind0:ind0+a.shape[0], ind1:ind1+a.shape[1]] = a
    else:
        res = sparse.block_diag(arrays, format=sparseformat)
    return res

def cvx_spdiag(x):
    """
    Sparse diagonal of not necessarily square matrices.
    """
    zsparse = lambda size: cvxopt.spmatrix([], [], [], size)
    xl = map(cvxopt.sparse, x)
    return cvxopt.sparse([[cvxopt.sparse([zsparse((sum(xj.size[0] for xj in xl[:i]), xi.size[1])),
                                          xi,
                                          zsparse((sum(xj.size[0] for xj in xl[(i+1):]), xi.size[1]))])]
                          for i,xi in enumerate(xl)])

def cvx_sparse(M):
    """
    Create and return a ``cvxopt.spmatrix`` sparse matrix from a scipy sparse matrix ``M``.
    """
    I,J = M.nonzero()
    if len(I)>0:
        D = np.array(M[I,J].todense()).flatten()
    else:
        D = []
    return cvxopt.spmatrix(D, map(int, I), map(int, J), M.shape)

class OptimalityError(Exception):
    pass

def bisection(testfun, start, task='maximize', tolerance=1e-3):
    """
    ``testfun`` is a function of a scalar number returning a tuple of a Boolean value
    and another object ``O``.
    
    If ``task`` is ``'maximize'``, find the largest ``x`` such that ``testfun(x)``
    returns ``True`` as first return value, using bisection.
    This assumes that ``testfun(x)`` returns ``True`` for ``x`` between 0 and
    a sufficiently small value.
    If ``task`` is ``'minimize'``, find the smallest such ``x``. This assumes that
    ``testfun(x)`` returns ``True`` for ``x`` above a sufficiently large value.

    Use ``start`` as an initial guess for ``x``.

    Returns the best estimate for ``x``, and the object ``O`` returned by ``testfun`` for
    this value of ``x``.

    This implementation assumes that ``testfun`` is only defined for non-negative arguments.
    """
    minx = 0.0
    maxx = np.inf
    nextx = start
    bestres = None
    while maxx - minx > tolerance:
        feasible, res = testfun(nextx)
        if task == 'maximize':
            if feasible:
                bestres = res
                minx = nextx
            else:
                maxx = nextx
            if not np.isfinite(maxx):
                nextx = 2 * minx
        else: # minimize
            if feasible:
                bestres = res
                maxx = nextx
            else:
                minx = nextx
            if not np.isfinite(maxx):
                nextx = 2 * minx
        if np.isfinite(maxx):
            nextx = 0.5 * (maxx + minx)
    if task == 'maximize':
        return minx, bestres
    else:
        return maxx, bestres

class Collocation(object):
    """
    Provides auxiliary routines for collocation.
    """
    basisfun_available = ("lagrange")
    collocation_available = ("lobatto","radau","radau-rev")
    def __init__(self, K, basisfun="lagrange", collocation="lobatto"):
        """
        Define a Collocation object with K > 0 collocation points.
        Implemented basis functions:
        lagrange - Lagrange polynomials
        Implemented types of collocation points:
        lobatto - Lobatto collocation points
        radau - Radau collocation points
        radau-rev - Radau collocation points with endpoint included
        """
        self.K = K
        self.basisfun = basisfun
        self.collocation = collocation
        if collocation not in self.collocation_available:
            raise ValueError("Collocation points %s not implemented, must be one of: %s" % (collocation, self.collocation_available))
        if basisfun not in self.basisfun_available:
            raise ValueError("Basis function %s not implemented, must be one of: %s" % (basisfun, self.basisfun_available))
        if K<2 and collocation == "lobatto":
            raise ValueError("Lobatto collocation requires K > 1, got K = %d." % K)

    def points(self):
        """
        Return collocation points in the interval [-1, 1].
        """
        if self.collocation == "lobatto":
            if self.K <= 2:
                return np.array([-1.0, 1.0])
            else:
                return np.concatenate(([-1.0], legroots(legder([0 for i in range(self.K-1)] + [1])), [1.0]))
        if self.collocation == "radau":
            return legroots([0 for i in range(self.K-1)] + [1, 1])
        if self.collocation == "radau-rev":
            return -legroots([0 for i in range(self.K-1)] + [1, 1])[::-1]

    def weights(self):
        """
        Return Gauss quadrature weights.
        """
        if self.collocation == "lobatto":
            if self.K > 2:
                innerweights = 2.0 / (self.K * (self.K-1) * legval(legroots(legder([0 for i in range(self.K-1)] + [1])),
                                                                   [0 for i in range(self.K-1)] + [1])**2)
            else:
                innerweights = []
            w0 = 2.0/(self.K * (self.K-1))
            return np.concatenate(([w0], innerweights, [w0]))
        if self.collocation == "radau":
            if self.K > 1:
                p = self.points()[1:]
                innerweights = 1.0 / ((1-p) * legval(p, legder([0 for i in range(self.K-1)] + [1]))**2)
            else:
                innerweights = []
            return np.concatenate(([2.0 / self.K**2], innerweights))
        if self.collocation == "radau-rev":
            if self.K > 1:
                p = -self.points()[-2::-1]
                innerweights = 1.0 / ((1-p) * legval(p, legder([0 for i in range(self.K-1)] + [1]))**2)
            else:
                innerweights = []
            return np.concatenate(([2.0 / self.K**2], innerweights))[::-1]

    def vander(self, dim=1, sparseformat=None):
        """
        Return Vandermonde matrix for vectors of length dim.
        """
        I = np.eye(dim) if sparseformat is None else sparse.eye(dim)
        p = self.points()
        if self.basisfun == "lagrange":
            P = lambda i,j: 1.0 if i==j else 0.0
            if sparseformat is not None:
                res = sparse.vstack(tuple(sparse.hstack(tuple(P(i,j) * I for i in range(self.K)))
                                          for j in range(self.K))).asformat(sparseformat)
            else:
                res = np.vstack(tuple(np.hstack(tuple(P(i,j) * I for i in range(self.K)))
                                      for j in range(self.K)))
        return res

    def int_basis(self, p, q):
        """
        Compute int_-1^(r_q) L_p(s) ds.
        """
        ps = self.points()
        if self.basisfun == "lagrange":
            return integrate.fixed_quad(Lp, -1, ps[q], n=int(self.K/2)+1)[0] if q>=0 else integrate.fixed_quad(Lp, -1, 1, n=int(self.K/2)+1)[0]

    def int_vander(self, dim=1, sparseformat=None):
        """
        Return integral Vandermonde matrix for vectors of length dim up to q-th collocation point
        """
        I = np.eye(dim) if sparseformat is None else sparse.eye(dim)
        ps = self.points()
        if self.basisfun == "lagrange":
            L = lambda s,p: reduce(operator.mul, [(s - pj)/(ps[p] - pj) for pj in ps if not pj == ps[p]], 1.0)
            Pint = lambda q,p: integrate.fixed_quad(lambda s: L(s,p), -1, ps[q], n=int(self.K/2) + 1)[0]
            if sparseformat is None:
                res = np.vstack(tuple(np.hstack(tuple(Pint(q,p) * I for p in range(self.K)))
                                      for q in range(self.K)))
            else:
                res = sparse.vstack(tuple(sparse.hstack(tuple(Pint(q,p) * I for p in range(self.K)))
                                          for q in range(self.K))).asformat(sparseformat)
        return res


    def eval(self, p, X):
        """
        Evaluate collocated function at point ``p`` in the interval ``[-1, 1]``, using
        elements of the list ``X`` as basis function coefficients.
        """
        ps = self.points()
        if self.basisfun == "lagrange":
            L = lambda s,i: reduce(operator.mul, [(s - pj)/(ps[i] - pj) for j,pj in enumerate(ps) if not j == i], 1.0)
            return sum(Xi*L(p,i) for i,Xi in enumerate(X))

    def integrate(self, q, t):
        """
        Return the integral of the ``q``-th basis function over the interval ``[-1, t]``
        """
        ps = self.points()
        if self.basisfun == "lagrange":
            L = lambda s,i: reduce(operator.mul, [(s - pj)/(ps[i] - pj) for j,pj in enumerate(ps) if not j == i], 1.0)
            return integrate.fixed_quad(lambda s: L(s,q), -1, t, n=(self.K // 2 + 1))[0]

LLCollocation = lambda K: Collocation(K, "lagrange", "lobatto")
LRCollocation = lambda K: Collocation(K, "lagrange", "radau")
LRrevCollocation = lambda K: Collocation(K, "lagrange", "radau-rev")

class CollocatedTrajectory(object):
    """
    Represents a trajectory for an LTI system approximated by collocation.
    """
    def __init__(self, Z, *args, **kwargs): # args: collocation, finaltime, timesteps, controldim, statedim, initialstate
        if len(args):
            collocation, finaltime, timesteps, controldim, statedim, initialstate = args
            self.collocation = collocation
            self._u = Z[:collocation.K*timesteps*controldim]
            self._xdot = Z[collocation.K*timesteps*controldim:collocation.K*timesteps*(controldim+statedim)]
            self._x = Z[collocation.K*timesteps*(controldim+statedim):]
            self._m = controldim
            self._n = statedim
            self._N = timesteps
            self._x0 = initialstate
            self._h = finaltime / timesteps
            self.finaltime = finaltime
        else:
            self.collocation = Z.collocation
            self._m = Z._m
            self._n = Z._n
            self._N = Z._N
            self._h = Z._h
            self.finaltime = Z.finaltime
            if "copy" in kwargs and kwargs["copy"]:
                self._u = Z._u.copy()
                self._xdot = Z._xdot.copy()
                self._x = Z._x.copy()
                self._x0 = Z._x0.copy()
            else:
                self._u = Z._u
                self._xdot = Z._xdot
                self._x = Z._x
                self._x0 = Z._x0

    def get_control(self, t):
        def get_single(ti):
            ind = self.index(ti)
            if ind >= self._N:
                ind = self._N - 1
            us = [self._u[int(ind*self.collocation.K*self._m+i*self._m):int(ind*self.collocation.K*self._m+(i+1)*self._m)] for i in range(self.collocation.K)]
            return self.collocation.eval(self.mapt(ti), us)
        return get_single(t) if np.isscalar(t) else np.array([get_single(ti) for ti in t])

    def get_state(self, t):
        def get_single(ti):
            ind = self.index(ti)
            xs = self._x[int((ind-1)*self._n):int(ind*self._n)] if ind > 0 else self._x0
            i0 = ind*self.collocation.K*self._n
            if i0 < len(self._xdot):
                return xs + sum(self._h / 2 * self._xdot[int(i0+i*self._n):int(i0+(i+1)*self._n)] * self.collocation.integrate(i, self.mapt(ti))
                                for i in range(self.collocation.K))
            else:
                return xs
        return get_single(t) if np.isscalar(t) else np.array([get_single(ti) for ti in t])

    def get_statederivative(self, t):
        def get_single(ti):
            ind = self.index(ti)
            if ind >= self._N:
                ind = self._N - 1
            xdots = [self._xdot[ind*self.collocation.K*self._n+i*self._n:ind*self.collocation.K*self._n+(i+1)*self._n] for i in range(self.collocation.K)]
            return self.collocation.eval(self.mapt(ti), xdots)
        return get_single(t) if np.isscalar(t) else np.array([get_single(ti) for ti in t])

    def index(self, t):
        return t // self._h

    def mapt(self, t):
        """
        Map the timepoint ``t`` to the range ``[-1, 1]`` in the respective discretization interval.
        """
        return 2 * (t / self._h - self.index(t)) - 1

    def split(self, step):
        """
        Return two collocated trajectories, one for the time interval [0, step * h], the other for the
        time interval [step * h, self.finaltime].
        """
        t1 = CollocatedTrajectory(self, copy=False)
        t2 = CollocatedTrajectory(self, copy=False)
        t1._u = self._u[:step*self.collocation.K*self._m]
        t1._xdot = self._xdot[:step*self.collocation.K*self._n]
        t1._x = self._x[:step*self._n]
        t1._N = step
        t1.finaltime = step * self._h
        t2._u = self._u[step*self.collocation.K*self._m:]
        t2._xdot = self._xdot[step*self.collocation.K*self._n:]
        t2._x = self._x[step*self._n:]
        t2._N = self._N - step
        t2._x0 = self.get_state(step * self._h)
        t2.finaltime = self.finaltime - step * self._h
        return t1, t2

    def __add__(self, other):
        "Concatenation of trajectories."
        if np.abs(self._h - other._h) > np.MachAr().eps:
            raise ValueError("Step sizes must be equal.")
        if self._n != other._n:
            raise ValueError("State dimensions must be equal.")
        if self._m != other._m:
            raise ValueError("Control dimensions must be equal.")
        res = CollocatedTrajectory(self, copy=False)
        res.finaltime += other.finaltime
        res._N += other._N
        res._x = np.concatenate((self._x, other._x))
        res._xdot = np.concatenate((self._xdot, other._xdot))
        res._u = np.concatenate((self._u, other._u))
        return res

# utility function for sparse matrices
speye = lambda n: sparse.spdiags(np.ones(n), 0, n, n)

class LinOpt(object):
    """
    Dynamic optimizer for an LTI system.

    Given an LTI system

        dx/dt = A x + B u + u0        (LTI)

    solves the linear optimal control problem

        min_u int_0^T (C x(t) + D u(t) + E) dt + F x(T)
        s.t.  (LTI)
              Gx x(t) + Gu u(t) + Gk <= 0
              Hx x(T) + Hk <= 0
              Ku u(t) + self.Kk= 0.
    """
    default_params = {"timesteps": 2,
                      "finaltime": 1.0,
                      "fixedterm": True,
                      "collocation": Collocation(1, 'lagrange', 'radau'),
                      "finaltimetolerance": 0.05,
                      "solver": None,
                      "sparseformat": 'lil',
                      }

    def __init__(self, *args, **kwargs):
        """
        Set up the optimizer.

        Usage:
        LinOpt(A,B): initialize the system with matrices A and B
        LinOpt(A,B,x0): initialize the system with matrices A and B, and initial condition x0
        LinOpt(A,B,x0,u0): initialize the system with matrices A and B, initial condition x0, and constant input u0
        x0 - initial condition (zero if left out)
        u0 - constant input (zero if left out)

        Parameters can be passed in kwargs. See LinOpt.default_params for options.
        """
        if len(args) == 2:
            self.A, self.B = args
            self.x0 = np.zeros(self.A.shape[1])
            self.u0 = np.zeros(self.A.shape[1])
        elif len(args) == 3:
            self.A, self.B, self.x0 = args
            self.u0 = np.zeros(self.A.shape[1])
        elif len(args) == 4:
            self.A, self.B, self.x0, self.u0 = args
        else:
            raise TypeError("LinOpt expected 2 to 4 arguments, got %d." % len(args))
        self.m = self.B.shape[1]
        self.n = self.A.shape[1]
        if isinstance(self.A, np.ndarray):
            self.A = sparse.lil_matrix(self.A)
        if isinstance(self.B, np.ndarray):
            self.B = sparse.lil_matrix(self.B)
        self.solver = None
        self.path_constraints = []
        self.terminal_constraints = []
        self.control_constraints = []
        self.initial_control_constraints = []
        self.params = self.default_params.copy()
        self.solverparams = {}
        self.set_parameters(**kwargs)
        self.set_objective(np.zeros(self.n), np.zeros(self.m), 0, np.zeros(self.n))

    # utility functions for conversion to / from sparse matrices and combinations
    def toarray(self, array):
        if sparse.issparse(array):
            return array.toarray()
        else:
            return array

    def tosparse(self, array):
        if self.params['sparseformat'] is not None:
            return sparse.block_diag((array,), format=self.params['sparseformat'])
        elif sparse.issparse(array):
            return array.toarray()
        else:
            return np.atleast_2d(array)

    block_diagonal = lambda self, b: block_diagonal(b, self.params['sparseformat'])

    def vstack(self, blocks):
        if self.params['sparseformat'] is None:
            res = np.vstack(tuple((b.toarray() if sparse.issparse(b) else b) for b in blocks))
        else:
            try:
                res = sparse.vstack(tuple((b if sparse.issparse(b) else self.tosparse(b)) for b in blocks), format=self.params['sparseformat'])
            except ValueError:
                res = []
                for b in blocks:
                    for bi in b:
                        res.append(bi)
                res = self.tosparse(res)
        return res

    def hstack(self, blocks):
        if self.params['sparseformat'] is None:
            res = np.hstack(tuple((b.toarray() if sparse.issparse(b) else b) for b in blocks))
        else:
            res = sparse.hstack(tuple((b if sparse.issparse(b) else self.tosparse(b)) for b in blocks), format=self.params['sparseformat'])
        return res

    def zeros(self, arg, **kwargs):
        if self.params['sparseformat'] is None:
            return np.zeros(arg, **kwargs)
        else:
            return sparse.lil_matrix(arg, **kwargs)

    def set_objective(self, C, D, E, F):
        """
        Set the objective functional for this optimization problem.

        The objective functional is given by J = int_0^T (C' x(t) + D' u(t) + E) dt + F' x(T).

        Arguments:
        C, F - vectors of the same size as the state variable x.
        D - a vector of the same size as the control variable u.
        E - a real number.

        Any of the arguments can be a scalar 0 or None to disregard the corresponding part in the objective.
        """
        self.C, self.D, self.E, self.F = C, D, E, F

    def add_path_constraint(self, Gxi, Gui, Gki):
        """
        Add a path inequality constraint to the optimization problem.

        The path constraint is given by:
               Gxi x(t) + Gui u(t) + Gki <= 0.
        Arguments:
        Gxi - an k x n matrix, or scalar for all equal values.
        Gui - a k x m matrix, or scalar for all equal values.
        Gki - a vector of length k.
        """
        k = 1 if np.isscalar(Gki) else len(Gki)
        if np.isscalar(Gxi):
            Gxi = Gxi * np.ones((k,self.n))
        if np.isscalar(Gui):
            Gui = Gui * np.ones((k,self.m))
        if np.isscalar(Gki):
            Gki = Gki * np.ones(k)
        self.path_constraints.append((Gxi, Gui, Gki))

    def add_terminal_constraint(self, Hxi, Hki):
        """
        Add a terminal constraint to the optimization problem.

        The terminal constraint is given by:
               Hxi x(t) + Hki <= 0.
        Arguments:
        Hxi - an k x n matrix, or scalar for all equal values.
        Hki - a vector of length k.
        """
        k = 1 if np.isscalar(Hki) else len(Hki)
        if np.isscalar(Hxi):
            Hxi = Hxi * np.ones((k, self.n))
        if np.isscalar(Hki):
            Hki = Hki * np.ones(k)
        self.terminal_constraints.append((Hxi, Hki))

    def add_control_constraint(self, Kui, Kki):
        """
        Add a path equality constraint for the control to the optimization problem.

        The path constraint is given by:
               Kui u(t) + Kki = 0.
        Arguments:
        Kui - a k x m matrix, or scalar for all equal values.
        Kki - a vector of length k.
        """
        k = 1 if np.isscalar(Kki) else len(Kki)
        if np.isscalar(Kui):
            Kui = Kui * np.ones((k,self.m))
        if np.isscalar(Kki):
            Kki = Kki * np.ones(k)
        self.control_constraints.append((Kui, Kki))

    def add_initial_control_constraint(self, Lui, Lki):
        """
        Add a initial equality constraint for the control to the optimization problem.
        Only constraining the first set of fluxes.

        The path constraint is given by:
               Lui u(0) + Lki = 0.
        Arguments:
        Lui - a k x m matrix, or scalar for all equal values.
        Lki - a vector of length k.
        """
        k = 1 if np.isscalar(Lki) else len(Lki)
        if np.isscalar(Lui):
            Lui = Lui * np.ones((k,self.m))
        if np.isscalar(Lki):
            Lki = Lki * np.ones(k)
        self.initial_control_constraints.append((Lui, Lki))

    def set_solver(self, solver):
        """
        Choose numerical solver to perform the optimization.

        Available solvers:
        cvxopt - Interior point solver cvxopt, requires the cvxopt module.
        gurobi - Gurobi, requires the gurobipy module.
        """
        self.params['solver'] = 'cvxopt'
        if solver == "cvxopt":
            import cvxopt
            import cvxopt.solvers
            self.cvxopt = cvxopt
        elif solver == "gurobi":
            try:
                import gurobipy
                self.grb = gurobipy
                self.params['solver'] = 'gurobi'
            except ImportError as err:
                print err
                print 'falling back to cvxopt as solver'
                import cvxopt
                import cvxopt.solvers
                self.cvxopt = cvxopt
        elif solver== "cplex":
            try:
                import cplex
                self.cplex = cplex
                self.params['solver'] = 'cplex'
            except ImportError as err:
                print err
                print 'falling back to cvxopt as solver'
                import cvxopt
                import cvxopt.solvers
                self.cvxopt = cvxopt
        elif solver== "soplex":
            try:
                import pyscipopt as soplex
                self.soplex = soplex
                self.params['solver'] = 'soplex'
            except ImportError as err:
                print err
                print 'falling back to cvxopt as solver'
                import cvxopt
                import cvxopt.solvers
                self.cvxopt = cvxopt
        else:
            raise ValueError("Unknown solver: %s" % solver)

    def set_parameters(self, **kwargs):
        """
        Set optimization parameters.

        Available parameters are:

        timesteps - number of discretization time steps (default = 50)
        finaltime - value of T (default = 1), maximal T in the problem with free final time
        finaltimetolerance - termination tolerance on the final time (default = 0.01)
        fixedterm - whether to keep final time fixed or not (default = True)
        collocation - which collocation scheme to use (default = Collocation(3, 'lagrange', 'radau'))

        Depending on the solver, additional parameters may be available.
        cvxopt: Parameters 'solver', 'primalstart', 'dualstart' from cvxopt.solvers.lp
        """
        for k in self.params:
            if k in kwargs:
                self.params[k] = kwargs.pop(k)
                if k == 'solver':
                    self.set_solver(self.params['solver']) # call to import solver's module
        self.solverparams.update(kwargs)

    def get_optimdim(self, collocation=None, optinitial=False):
        """
        Get the number of optimization variables in the discretized optimization problem.
        See the 'discretize' method for more details.
        """
        if collocation is None:
            collocation = self.params['collocation']
        optimdim = self.params["timesteps"] * (self.m * collocation.K + self.n * collocation.K + self.n)
        if optinitial:
            optimdim += len(self.x0)
        return optimdim

    def discretize(self, collocation=None, optinitial=False, keep_matrices=False, degradation_steps = 0):
        """
        Perform the discretization with collocation (use from parameters by default).
        The control, state derivative, and state trajectories are discretized as
        u(t_i + h/2 (r_j + 1)) = P ui (ui is a vector of length mK and P depends on collocation type)
        xdot(t_i + h/2 (r_j + 1)) = P xdoti (xdoti is a vector of length nK)
        xi(t_i) = x_i
        where t_i are the time interval boundaries, r_j the relative positions of the collocation points within each interval,
        and h the length of the time intervals.

        Optimization is then done over the vector z = (u11, ..., u1K, ..., uN1, ..., uNK,
                                                       xdot11, ..., xdot1K, ..., xdotN1, ..., xdotNk,
                                                       x1, ..., xN)

        Returns vectors c,b,d, a scalar e, and sparse matrices A,C corresponding to the linear program
            min  c' z + e
            s.t. A z = b
                 C z <= d
        which is the discrete approximation to the linear optimal control problem.

        If optinitial is True, the initial condition is also an optimization variable for the linear program:
            min  c1' z + c2' z0 + e
            s.t. A1 z + A2 z0 = b
                 C1 z + C2 z0 <= d
        and the function returns c1, b, d, e, A1, C1, c2, A2, C2.
        """
        if collocation is None:
            collocation = self.params['collocation']
        optimdim = self.params["timesteps"] * (self.m * collocation.K + self.n * collocation.K + self.n)
        weights = collocation.weights()
        cpoints = collocation.points()
        h = self.params["finaltime"] / self.params["timesteps"]
        N = self.params["timesteps"]
        Pu = collocation.vander(self.m, sparseformat=self.params['sparseformat'])
        Px = collocation.vander(self.n, sparseformat=self.params['sparseformat'])
        Qu = collocation.int_vander(self.m, sparseformat=self.params['sparseformat'])
        Qx = collocation.int_vander(self.n, sparseformat=self.params['sparseformat'])
        In = self.tosparse(np.eye(self.n))
        Im = self.tosparse(np.eye(self.m))
        Wn = self.hstack(tuple(w * In for w in weights))
        Wm = self.hstack(tuple(w * Im for w in weights))
        W = self.tosparse(weights)

        # discrete objective
        # objtup will be a 1 x optimdim matrix for the objective
        if callable(self.D):
            objtup = tuple(h / 2 * W.dot(self.block_diagonal([self.tosparse(self.D(i * h + h / 2 + h / 2 * r)) for r in cpoints])).dot(Pu)
                           for i in range(self.params['timesteps']))
        elif self.D is not None and np.any(self.toarray(self.D)):
            DWP = h / 2 * self.tosparse(self.D).dot(Wm).dot(Pu)
            objtup = self.params["timesteps"] * (DWP,)
        else:
            objtup = self.params["timesteps"] * (self.zeros((1,self.m * collocation.K)),)

        if callable(self.C):
            objtup = objtup + tuple(h**2 / 4 * W.dot(self.block_diagonal([self.tosparse(self.C(i * h + h / 2 + h / 2 * r)) for r in cpoints])).dot(Qx)
                                    for i in range(self.params['timesteps']))
            objtup = objtup + tuple(h * self.tosparse(self.C(i * h)) for i in range(self.params['timesteps'] - 1))
            if optinitial:
                objoffset = 0.0
                objinitial = h * self.tosparse(self.C(0.0))
            else:
                objoffset = h * self.C(0.0).dot(self.x0)
        elif self.C is not None and np.any(self.toarray(self.C)):
            CWQ = h**2 / 4 * self.tosparse(self.C).dot(Wn).dot(Qx)
            objtup = objtup + self.params["timesteps"] * (CWQ,)
            objtup = objtup + (self.params["timesteps"]-1) * (h * self.tosparse(self.C),)
            if optinitial:
                objoffset = 0.0
                objinitial = h * self.tosparse(self.C)
            else:
                objoffset = h * self.C.dot(self.x0)
        else:
            objtup = objtup + self.params["timesteps"] * (self.zeros((1,self.n * collocation.K)),)
            objtup = objtup + (self.params["timesteps"]-1) * (self.zeros((1,self.n)),)
            objoffset = 0.0
            objinitial = self.zeros((1,self.n))

        if self.F is not None and np.any(self.toarray(self.F)):
            objtup = objtup + (self.tosparse(self.F),)
        else:
            objtup = objtup + (self.zeros((1, self.n)),)
        objvec = self.hstack(objtup)
        assert objvec.shape[1] == optimdim

        if callable(self.E):
            objoffset += quad(self.E, 0, self.params["finaltime"])[0]
        elif self.E is not None and self.E != 0:
            objoffset += self.E * self.params["finaltime"]

        # equality constraints
        nK = self.n * collocation.K
        mK = self.m * collocation.K
        xdotoffset = self.params["timesteps"] * self.m * collocation.K
        xoffset = xdotoffset + self.params["timesteps"] * self.n * collocation.K
        AQ = self.block_diagonal(collocation.K * (self.A,)).dot(Qx)
        BP = self.block_diagonal(collocation.K * (self.B,)).dot(Pu)
        if len(self.control_constraints):
            Ku = self.vstack(tuple(Kui for (Kui, Kki) in self.control_constraints))
            self.Kk= self.vstack(tuple(Kki for (Kui, Kki) in self.control_constraints))
            KuP = self.block_diagonal(collocation.K * (Ku,)).dot(Pu)
            self.ncc = Ku.shape[0]
        else:
            self.ncc = 0

        if len(self.initial_control_constraints):
            Ru = self.vstack(tuple(Rui for (Rui, Rki) in self.initial_control_constraints))
            Rk = self.vstack(tuple(Rki for (Rui, Rki) in self.initial_control_constraints))
            RuP = self.block_diagonal(collocation.K * (Ru,)).dot(Pu)
            # initial constraints only apply to the first set of controls
            self.initial_constraint_amount = []
            for counter, ict in enumerate(self.initial_control_constraints):
                self.initial_constraint_amount.append(collocation.K * ict[0].shape[0])
            eqmatrix = sparse.lil_matrix(((self.params["timesteps"] * (collocation.K * (self.n + self.ncc) + self.n)) + np.sum(self.initial_constraint_amount),
                                           optimdim))
        else:
            eqmatrix = sparse.lil_matrix((self.params["timesteps"] * (collocation.K * (self.n + self.ncc) + self.n), optimdim))

        for i in range(N):  # iterate over time steps
            # discrete dynamics (collocation)
            eqmatrix[i*nK:(i+1)*nK, i*mK:(i+1)*mK] = - BP
            eqmatrix[i*nK:(i+1)*nK, xdotoffset+i*nK:xdotoffset+(i+1)*nK] = Px - AQ * h / 2
            if i > 0 and self.A.nnz:
                eqmatrix[i*nK:(i+1)*nK, xoffset+(i-1)*self.n:xoffset+i*self.n] = self.vstack(collocation.K * (- self.A,))
            # continuity constraints
            rowoff = N*nK
            if i > 0:
                eqmatrix[rowoff+i*self.n:rowoff+(i+1)*self.n, xoffset+(i-1)*self.n:xoffset+i*self.n] = In
            eqmatrix[rowoff+i*self.n:rowoff+(i+1)*self.n, xdotoffset+i*nK:xdotoffset+(i+1)*nK] = Wn * h / 2
            eqmatrix[rowoff+i*self.n:rowoff+(i+1)*self.n, xoffset+i*self.n:xoffset+(i+1)*self.n] = - In
            # path equality constraints on the control
            if self.ncc > 0:
                rowoff = N*nK + N*self.n
                nccK = self.ncc * collocation.K
                eqmatrix[rowoff+i*nccK:rowoff+(i+1)*nccK, i*mK:(i+1)*mK] = KuP

        if self.initial_control_constraints:
            for i, ict in enumerate(self.initial_control_constraints):
                if i > 0:
                    eqmatrix[-np.sum(self.initial_constraint_amount[:i+1]):-np.sum(self.initial_constraint_amount[:i]), 0:self.m] = ict[0].dot(Pu)
                else:
                    eqmatrix[-self.initial_constraint_amount[0]:, 0:mK] = RuP

        if optinitial:
            eqmatrix2 = sparse.lil_matrix((self.params["timesteps"] * (collocation.K * self.n + self.n), self.n))
            for i in range(collocation.K):
                if self.A.nnz:
                    eqmatrix2[i*self.n:(i+1)*self.n,:] -= self.A
            eqmatrix2[N*nK:N*nK+self.n,:] += np.eye(self.n)
            eqvector = self.tosparse(self.hstack(collocation.K * N * (self.u0,) + (0*self.x0,) +
                                                 (self.params["timesteps"] - 1) * (np.zeros(self.n),)))
        else:
            if degradation_steps > 0:
                eqvector = self.tosparse(self.hstack(collocation.K * degradation_steps *  (self.u0,) +
                                                     collocation.K * (self.params["timesteps"] - degradation_steps) * (np.zeros(len(self.u0)),)
                                                     + (-self.x0,) + (self.params["timesteps"] - 1) * (np.zeros(self.n),)))
            else:
                eqvector = self.tosparse(self.hstack(collocation.K * self.params["timesteps"] * (self.u0,) + (-self.x0,) +
                                                 (self.params["timesteps"] - 1) * (np.zeros(self.n),)))
            for i in range(collocation.K):
                if self.A.nnz:
                    eqvector[0,i*self.n:(i+1)*self.n] += self.A.dot(self.x0)
        if self.ncc > 0:
            eqvector = self.hstack((eqvector, self.hstack(collocation.K * N * (-self.Kk,))))

        if len(self.initial_control_constraints):
            eqvector = self.hstack(( eqvector , np.zeros(np.sum(self.initial_constraint_amount)) ))

        # inequality constraints
        if len(self.path_constraints):
            self.Gx = self.vstack(tuple(Gxi for (Gxi, Gui, Gki) in self.path_constraints))
            Gu = self.vstack(tuple(Gui for (Gxi, Gui, Gki) in self.path_constraints))
            self.Gk = self.hstack(tuple(Gki for (Gxi, Gui, Gki) in self.path_constraints))
            self.numpath = self.Gx.shape[0] * collocation.K * N
            GuP = self.block_diagonal(collocation.K * (Gu,)).dot(Pu)
            GxQ = self.block_diagonal(collocation.K * (self.Gx,)).dot(Qx)
            nG = self.Gx.shape[0]
        else:
            self.numpath = 0
        if len(self.terminal_constraints):
            Hx = self.vstack(tuple(Hxi for (Hxi, Hki) in self.terminal_constraints))
            self.Hk = self.hstack(tuple(Hki for (Hxi, Hki) in self.terminal_constraints))
            self.numterm = Hx.shape[0]
            # nH = Hx.shape[0]
        else:
            self.numterm = 0
        if self.numpath + self.numterm > 0:
            ineqmatrix = sparse.lil_matrix((self.numpath + self.numterm, optimdim))
            for i in range(self.params['timesteps']):
                if self.numpath:
                    ineqmatrix[i*collocation.K*nG:(i+1)*collocation.K*nG, i*mK:(i+1)*mK] = GuP
                    ineqmatrix[i*collocation.K*nG:(i+1)*collocation.K*nG,
                               xdotoffset+i*nK:xdotoffset+(i+1)*nK] = GxQ * h / 2
                    if i > 0:
                        ineqmatrix[i*collocation.K*nG:(i+1)*collocation.K*nG,
                                   xoffset+(i-1)*self.n:xoffset+i*self.n] = self.vstack(collocation.K * (self.Gx,))
            if self.numterm:
                ineqmatrix[N*collocation.K*nG:, xoffset+(N-1)*self.n:xoffset+N*self.n] = Hx
        else:
            ineqmatrix = self.zeros((1,optimdim))
        if optinitial:
            ineqmatrix2 = sparse.lil_matrix((self.numpath + self.numterm, self.n))
            if self.numpath:
                ineqmatrix2[:collocation.K*nG,:] = self.vstack(collocation.K * (self.Gx,))

        ineqvector = self.zeros((1,self.numpath + self.numterm))
        if self.numpath:
            ineqvector[0,:self.numpath] = - self.hstack(collocation.K * N * (self.Gk,))
            if not optinitial:
                ineqvector[0,:collocation.K*nG] -= self.hstack(collocation.K * (self.Gx.dot(self.x0),))
        if self.numterm:
            ineqvector[0,self.numpath:] = - self.Hk
        if optinitial:
            print 'completed collocation'
            return objvec, eqvector, ineqvector, objoffset, eqmatrix, ineqmatrix, objinitial, eqmatrix2, ineqmatrix2
        else:
            if keep_matrices:
                self.objvec = objvec
                self.eqvector =  eqvector
                self.ineqvector = ineqvector
                self.objoffset = objoffset
                self.eqmatrix = eqmatrix
                self.ineqmatrix = ineqmatrix
                self.h = h
            print 'completed collocation'
            return objvec, eqvector, ineqvector, objoffset, eqmatrix, ineqmatrix

    def update_collocation(self, new_x0, new_u0=None, degradation_steps = 0, collocation=None, **kwargs):
        """
        changes the initial values of the matrices representing the collocated problem
        :param new_x0: array with the updated initial values
        :return: collocated matrices
        """
        if hasattr(self, 'objoffset'):
            if collocation is None:
                collocation = self.params['collocation']
            N = self.params["timesteps"]
            self.x0 = np.array(new_x0)
            if new_u0 is not None:
                self.u0 = np.array(new_u0)
            # ncc = len(self.control_constraints)
            if self.C is not None and np.any(self.toarray(self.C)):
                self.objoffset = self.h * self.C.dot(self.x0)
            else:
                self.objoffset = 0.0
            if self.E is not None and self.E != 0:
                objoffset += self.E * self.params["finaltime"]
            if degradation_steps > 0:
                eqvector = self.tosparse(self.hstack(collocation.K * degradation_steps * (self.u0,) +
                                                     collocation.K * (self.params["timesteps"]-degradation_steps) * (np.zeros(len(self.u0)),)
                                                     + (-self.x0,) + (self.params["timesteps"] - 1) * (np.zeros(self.n),)))
            else:
                eqvector = self.tosparse(self.hstack(collocation.K * self.params["timesteps"] * (self.u0,) + (-self.x0,) +
                                                     (self.params["timesteps"] - 1) * (np.zeros(self.n),)))
            for i in range(collocation.K):
                if self.A.nnz:
                    eqvector[0, i * self.n:(i + 1) * self.n] += self.A.dot(self.x0)
            if self.ncc > 0:
                eqvector = self.hstack((eqvector, self.hstack(collocation.K * N * (-self.Kk,))))

            if len(self.initial_control_constraints):
                eqvector = self.hstack((eqvector, np.zeros(np.sum(self.initial_constraint_amount))))
            self.eqvector = eqvector

            ineqvector = self.zeros((1, self.numpath + self.numterm))
            nG = self.Gx.shape[0]
            if self.numpath:
                N = self.params["timesteps"]
                ineqvector[0, :self.numpath] = - self.hstack(collocation.K * N * (self.Gk,))
                ineqvector[0, :collocation.K * nG] -= self.hstack(collocation.K * (self.Gx.dot(self.x0),))
            if self.numterm:
                ineqvector[0, self.numpath:] = - self.Hk
            self.ineqvector = ineqvector
            return self.objvec, self.eqvector, self.ineqvector, self.objoffset, self.eqmatrix, self.ineqmatrix
        else:
            return self.discretize(optinitial=False, keep_matrices=True)

    def solve(self, **kwargs):
        """
        Solve the linear dynamic optimization problem.

        See LtiOpt.set_solver and LtiOpt.set_parameters for details on method arguments.

        For problems with fixed terminal time, return the optimal value and a
        :py:class:`.CollocatedTrajectory` object representing an optimal trajectory.
        For problems with variable terminal time, return a tuple of the optimal time,
        the objective value, and the trajectory.
        """
        if self.params['fixedterm']:
            return self.solve_fixedterm(**kwargs)
        else:
            # First find the "best" time where the problem is still feasible.
            other = copy(self)
            other.set_objective(0*self.C, 0*self.D, 0*self.E, 0*self.F)
            other.params = self.params.copy()
            def testfun(t):
                other.set_parameters(finaltime=t)
                try:
                    res = other.solve_fixedterm(**kwargs)
                    return True, res
                except OptimalityError:
                    return False, None
            topt, resopt = bisection(testfun, self.params['finaltime'], task='minimize' if self.E >= 0 else 'maximize',
                                     tolerance=self.params['finaltimetolerance'])
            # With a state- and control-independent objective, we're done.
            if np.allclose(self.C, 0) and np.allclose(self.D, 0) and np.allclose(self.F, 0):
                return (topt,) + resopt
            # Otherwise, do a scalar optimization between the "best" time and the user-supplied final time
            other = copy(self)
            other.params = self.params.copy()
            def objfun(t):
                other.set_parameters(finaltime=t)
                try:
                    res = other.solve_fixedterm(**kwargs)[0]
                except OptimalityError:
                    res = np.inf
                return res
            mint = min(topt, self.params['finaltime'])
            maxt = max(topt, self.params['finaltime'])
            topt = optimize.brent(objfun, brack=(mint, maxt))
            other.set_parameters(finaltime=topt)
            return (topt,) + other.solve_fixedterm(**kwargs)

    def solve_iteration(self, new_x0, new_u0=None, degradation_steps=0, **kwargs):
        """
        Use a preexisting collocation of the problem and update
        """
        # self.x0 = new_x0
        c,b,d,e,Me,Mi = self.update_collocation(new_x0, new_u0=new_u0, degradation_steps=degradation_steps)
            # self.objvec, self.eqvector, self.ineqvector, self.objoffset, self.eqmatrix, self.ineqmatrix
        x, obj = self.solve_lp(c, b, d, Me, Mi, params=kwargs)
        return (obj + e,
                CollocatedTrajectory(x, self.params['collocation'], self.params['finaltime'], self.params['timesteps'],
                                     self.m, self.n, self.x0)
            )

    def solve_fixedterm(self, **kwargs):
        """
        Solve the LTI optimization problem with cvxopt with fixed final time.

        If the optimization is successful, the method returns:

        * The optimal value.
        * A :py:class:`.CollocatedTrajectory` object representing an optimal trajectory.

        See `self.solve_lp` for available solvers and their options.
        """
        if 'keep_matrices' in kwargs and 'degradation_steps' in kwargs:
            c, b, d, e, Me, Mi = self.discretize(collocation=self.params['collocation'], keep_matrices=kwargs['keep_matrices'],
                                                 degradation_steps=kwargs['degradation_steps'])
            kwargs.pop('keep_matrices',None)
            kwargs.pop('degradation_steps',None)
            x, obj = self.solve_lp(c, b, d, Me, Mi, kwargs)
        elif 'keep_matrices' in kwargs:
            c, b, d, e, Me, Mi = self.discretize(collocation=self.params['collocation'], keep_matrices=kwargs['keep_matrices'])
            kwargs.pop('keep_matrices', None)
            x, obj = self.solve_lp(c, b, d, Me, Mi, kwargs)
        else:
            c,b,d,e,Me,Mi = self.discretize(collocation=self.params['collocation'])
            x, obj = self.solve_lp(c, b, d, Me, Mi, params=kwargs)
        return (obj + e,
                CollocatedTrajectory(x, self.params['collocation'], self.params['finaltime'], self.params['timesteps'],
                                     self.m, self.n, self.x0)
            )

    def control_variability(self, controlvec, times, optval=None, **kwargs):
        """
        Solve the state variability problem at given time points.

        Arguments::
        controlvec - Get variability in controlvec' * u(t)
        times - list of time points at which control variability should be computed.
        optval - Objective functional value which should be achieved. If None, solve
            the optimization problem first.
        Additional arguments are passed to the ``self.optimize`` method.
        """
        collocation = self.params['collocation']
        cpoints = collocation.points()
        optimdim = self.params["timesteps"] * (self.m * collocation.K + self.n * collocation.K + self.n)
        h = self.params["finaltime"] / self.params["timesteps"]
        N = self.params["timesteps"]
        xdotoffset = self.params["timesteps"] * self.m * collocation.K
        xoffset = xdotoffset + self.params["timesteps"] * self.n * collocation.K

        c,b,d,e,Me,Mi = self.discretize(collocation=self.params['collocation'])
        if optval is None:
            _, optval = self.solve_lp(c, b, d, Me, Mi, params=kwargs)
        else:
            optval = optval - e
        Miext = sparse.vstack((Mi, c)).tocsr()
        dext = np.concatenate((d, np.atleast_1d(optval)))
        times = np.atleast_1d(times)
        controlvec = np.atleast_1d(controlvec)
        control_min = np.zeros(len(times))
        control_max = np.zeros(len(times))
        # find indices of collocation points which are required for interpolation
        collpoints = []
        for i,ti in enumerate(times):
            ind = min(int(np.floor(ti / h)), N-1)
            rq = 2 * (ti - ind * h) / h - 1
            if rq >= cpoints[0]:
                collpoints.append((ind, cpoints[cpoints<=rq][-1]))
            elif ind > 0:
                collpoints.append((ind - 1, cpoints[-1]))
            else:
                collpoints.append((0, cpoints[0]))
            if rq <= cpoints[-1]:
                collpoints.append((ind, cpoints[cpoints>=rq][0]))
            elif ind < N - 1:
                collpoints.append((ind + 1, cpoints[0]))
            else:
                collpoints.append((N - 1, cpoints[-1]))
        collpoints = set(collpoints)
        uminmax = {}
        for ind, rq in collpoints:
            ti = ind * h + 0.5 * (rq + 1) * h
            ci = np.concatenate(ind * (np.zeros(collocation.K * self.m),) +              # u coefficients before this time interval
                                tuple(controlvec * collocation.eval(rq, unitvector(collocation.K, j)) for j in range(collocation.K)) +
                                (N-ind-1)* (np.zeros(collocation.K * self.m),) +         # u coefficients after this time interval
                                (np.zeros(N * (collocation.K + 1) * self.n),))            # xdot + x coefficients
            try:
                resmin, umin = self.solve_lp(ci, b, dext, Me, Miext, params=kwargs)
            except OptimalityError:
                umin = np.nan
            try:
                resmax, umax = self.solve_lp(-ci, b, dext, Me, Miext, params=kwargs)
            except OptimalityError:
                umax = np.nan
            uminmax[ti] = (umin, -umax)
        colltimes = np.sort(uminmax.keys())
        umin = np.array([uminmax[ti][0] for ti in colltimes])
        umax = np.array([uminmax[ti][1] for ti in colltimes])
        control_min = np.interp(times, colltimes, umin)
        control_max = np.interp(times, colltimes, umax)
        return control_min, control_max

    def state_variability(self, statevec, times, optval=None, **kwargs):
        """
        Solve the state variability problem at given time points.

        Arguments::
        statevec - Get variability in statevec' * x(t)
        times - list of time points at which state variability should be computed.
        optval - Objective functional value which should be achieved. If None, solve
            the optimization problem first.
        Additional arguments are passed to the ``self.solve_lp`` method.
        """
        collocation = self.params['collocation']
        optimdim = self.params["timesteps"] * (self.m * collocation.K + self.n * collocation.K + self.n)
        h = self.params["finaltime"] / self.params["timesteps"]
        N = self.params["timesteps"]
        xdotoffset = self.params["timesteps"] * self.m * collocation.K
        xoffset = xdotoffset + self.params["timesteps"] * self.n * collocation.K

        c,b,d,e,Me,Mi = self.discretize(collocation=self.params['collocation'])
        if optval is None:
            _, optval = self.solve_lp(c, b, d, Me, Mi, params=kwargs)
        else:
            optval = optval - e
        Miext = sparse.vstack((Mi, c)).tocsr()
        dext = np.concatenate((d, np.atleast_1d(optval)))
        times = np.atleast_1d(times)
        indices = np.array([i for i in range(N) if np.any(np.abs(times - (i+1) * h) < h)])
        statevec = np.atleast_1d(statevec)
        state_min = np.zeros(len(indices))
        state_max = np.zeros(len(indices))
        for i,ind in enumerate(indices):
            ci = np.concatenate((np.zeros(xoffset),) + ind * (np.zeros(self.n),) + (statevec,) + (N-ind-1)* (np.zeros(self.n),))
            try:
                resmin, smin = self.solve_lp(ci, b, dext, Me, Miext, params=kwargs)
                state_min[i] = smin
            except OptimalityError:
                state_min[i] = np.nan
            try:
                resmax, smax = self.solve_lp(-ci, b, dext, Me, Miext, params=kwargs)
                state_max[i] = -smax
            except OptimalityError:
                state_max[i] = np.nan
        timesp = np.concatenate(([0.], h * np.atleast_1d(np.float64(indices + 1))))
        sminp = np.concatenate(([statevec.dot(self.x0)], state_min))
        smaxp = np.concatenate(([statevec.dot(self.x0)], state_max))
        return (np.interp(times, timesp, sminp), np.interp(times, timesp, smaxp))

    def solve_cvxopt(self, params={}):
        """
        Solve the LTI optimization problem with cvxopt.

        For problems with fixed terminal time, return a :py:class:`.CollocatedTrajectory`
        object representing an optimal trajectory.
        For problems with variable terminal time, return a tuple of the optimal time
        and
        """
        warnings.warn("This function is deprecated, use 'self.solve' instead.", DeprecationWarning)
        return self.solve_fixedterm(solver='cvxopt', params=params)

    def solve_cvxopt_fixedterm(self, params={}):
        """
        Solve the LTI optimization problem with cvxopt with fixed final time.

        If the optimization is successful, the method returns:

        * The optimal value.
        * A :py:class:`.CollocatedTrajectory` object representing an optimal trajectory.
        """
        warnings.warn("This function is deprecated, use 'self.solve_fixedterm' instead.", DeprecationWarning)
        return self.solve_fixedterm(solver='cvxopt', params=params)

    def solve_lp(self, c, b, d, Me, Mi, params={}):
        """
        Solve the LP problem::

            min  c' x
            s.t. Me x  = b
                 Mi x <= d

        Returns an optimal solution x and objective function value c' x.
        Raises OptimalityError if solver fails.

        Available solvers (defined by params['solver']) with parameters:
        * 'cvxopt'
        * 'gurobi'
        * 'cplex'
        """
        solver = params['solver'] if 'solver' in params else self.params['solver']
        solverparams = self.solverparams.copy()
        solverparams.update(params)
        if solver == "cvxopt":
            cvxopt.solvers.options.update(solverparams)
            cvxopt.solvers.options['show_progress'] = True
            sol = cvxopt.solvers.lp(cvxopt.matrix(self.toarray(c)).T, cvx_sparse(Mi),
                                    cvxopt.matrix(self.toarray(d).ravel()), cvx_sparse(Me),
                                    cvxopt.matrix(self.toarray(b).ravel()),
                                    solver=solverparams['solver'] if 'solver' in solverparams else None)
            if sol['status'] == "dual infeasible":
                raise OptimalityError("Dual infeasibility encountered.")
            if sol['status'] == "primal infeasible":
                raise OptimalityError("Primal infeasibility encountered.")
            if sol['status'] == "unknown":
                raise OptimalityError("Optimizer returned with an unknown status.")
            x = np.array(sol['x']).ravel()
            return x, sol['primal objective']
        elif solver == "gurobi":
            m = grb.Model()
            for i in solverparams:
                m.setParam(i, solverparams[i])
            optdim = c.shape[1]
            optvar = [m.addVar(lb=-grb.GRB.INFINITY, obj=c[0,i]) for i in range(optdim)]
            m.update()
            # see http://stackoverflow.com/questions/22767608/sparse-matrix-lp-problems-in-gurobi-python for following code
            Me = Me.tocsr()
            for i in range(Me.shape[0]):
                start = Me.indptr[i]
                end   = Me.indptr[i+1]
                variables = [optvar[j] for j in Me.indices[start:end]]
                coeff     = Me.data[start:end]
                m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.EQUAL, rhs=b[0,i])
            Mi = Mi.tocsr()
            for i in range(Mi.shape[0]):
                start = Mi.indptr[i]
                end   = Mi.indptr[i+1]
                variables = [optvar[j] for j in Mi.indices[start:end]]
                coeff     = Mi.data[start:end]
                m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.LESS_EQUAL, rhs=d[0,i])
            m.setParam('NumericFocus', 3)
            m.setParam('scaleFlag' , 2)
            m.setParam('InfUnbdInfo', 1)
            m.optimize()
            if m.Status == grb.GRB.status.OPTIMAL:
                x = np.array([v.x for v in optvar])
                return x, m.getAttr('objVal')
            elif m.Status == grb.GRB.status.INFEASIBLE:
                raise OptimalityError("Primal infeasibility encountered.")
            else:
                raise OptimalityError("Gurobi returned with error status {:d}.".format(m.Status))
        elif solver == "cplex":
            model = cplex.Cplex()
            # try to populate by row
            model.objective.set_sense(model.objective.sense.minimize)
            model.variables.add(obj=c.toarray()[0,:], lb=[-cplex.infinity for i in range(c.shape[1])])
            Me = Me.tocsr()
            for i in range(Me.shape[0]):
                start = Me.indptr[i]
                end   = Me.indptr[i+1]
                variables = [np.asscalar(j) for j in Me.indices[start:end]]
                coeff     = list(Me.data[start:end])
                coeff = [np.asscalar(obj) for obj in coeff]
                btemp = np.asscalar(b[0,i])
                model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="E", rhs=[btemp])
            Mi = Mi.tocsr()
            for i in range(Mi.shape[0]):
                start = Mi.indptr[i]
                end   = Mi.indptr[i+1]
                variables = [np.asscalar(j) for j in Mi.indices[start:end]]
                coeff     = list(Mi.data[start:end])
                coeff     = [np.asscalar(obj) for obj in coeff]
                dtemp = np.asscalar(d[0,i])
                model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="L", rhs=[dtemp])
            model.parameters.simplex.tolerances.feasibility.set(1e-08)
            model.parameters.simplex.tolerances.optimality.set(1e-08)
            model.solve()
            if model.solution.status[model.solution.get_status()] == 'optimal':
                x = np.array(model.solution.get_values()).ravel()
                return x, model.solution.get_objective_value()
            # elif model.solution.get_status() == 5:
            #     print 'may contain infeasibilities'
            #     x = np.array(model.solution.get_values()).ravel()
            #     return x, model.solution.get_objective_value()
            elif model.solution.status[model.solution.get_status()] == 'infeasible':
                # model.write('faulty_problem.lp')
                raise OptimalityError("Primal infeasibility encountered.")
            else:
                raise OptimalityError("Cplex returned with error status '"+str(model.solution.status[model.solution.get_status()])+"'.")
        elif solver == "soplex":
            model = soplex.Model('deFBA_LP')
            for par in params:
                model.setParam(par, params[par])
            if not 'numerics/feastol' in params:
                model.setParam('numerics/feastol', 1e-8)
                model.setParam('numerics/lpfeastol', 1e-11)
            x = {}
            for i, j in enumerate(c.toarray()[0,:]):
                x[i]= model.addVar(lb=None, ub=None, vtype="C", name='x_'+str(i), obj=j)
            Me = csr_matrix(Me)
            for i in range(Me.shape[0]):
                start = Me.indptr[i]
                end = Me.indptr[i + 1]
                variables = [np.asscalar(j) for j in Me.indices[start:end]]
                coeff = list(Me.data[start:end])
                coeff = [np.asscalar(obj) for obj in coeff]
                btemp = np.asscalar(b[0,i])
                model.addCons(soplex.quicksum(x[variables[j]]*coeff[j] for j in range(len(variables))) == btemp, "equality_%s"%i)
            Mi = csr_matrix(Mi)
            for i in range(Mi.shape[0]):
                start = Mi.indptr[i]
                end = Mi.indptr[i + 1]
                variables = [np.asscalar(j) for j in Mi.indices[start:end]]
                coeff = list(Mi.data[start:end])
                coeff = [np.asscalar(obj) for obj in coeff]
                dtemp = np.asscalar(d[0,i])
                model.addCons(soplex.quicksum(x[variables[j]] * coeff[j] for j in range(len(variables))) <= dtemp, "inequality_%s" % i)
            model.optimize()
            status = model.getStatus()
            if status == 'optimal':
                output = np.array([model.getVal(x[j]) for j in x]).ravel()
                return output, model.getObjVal()
            elif status == 'infeasible':
                raise OptimalityError("Primal infeasibility encountered.")
            else:
                raise OptimalityError("Soplex returned with error status " + status + ".")
        else:
            raise ValueError("Unknown solver: %s" % solver)

def staticLP(c, b, d, Me, Mi, solver='cplex', params={}):
    """
    Solve the LP problem::

        min  c' x
        s.t. Me x  = b
             Mi x <= d

    Returns an optimal solution x and objective function value c' x.
    Raises OptimalityError if solver fails.

    Available solvers (defined by solver) with parameters:
    * 'cvxopt'
    * 'gurobi'
    * 'cplex'
    * 'soplex'
    """
    if solver == "cvxopt":
        sol = cvxopt.solvers.lp(cvxopt.matrix(c), cvxopt.matrix(Mi), cvxopt.matrix(d.ravel()), cvxopt.matrix(Me), cvxopt.matrix(b.ravel()))
        if sol['status'] == "dual infeasible":
            raise OptimalityError("Dual infeasibility encountered.")
        if sol['status'] == "primal infeasible":
            raise OptimalityError("Primal infeasibility encountered.")
        if sol['status'] == "unknown":
            raise OptimalityError("Optimizer returned with an unknown status.")
        x = np.array(sol['x']).ravel()
        return x, sol['primal objective']
    elif solver == "gurobi":
        try:
            m = grb.Model()
        except NameError:
            return staticLP(c, b, d, Me, Mi, solver='cvxopt')
        optdim = c.shape[0]
        optvar = [m.addVar(lb=-grb.GRB.INFINITY, obj=c[i]) for i in range(optdim)]
        m.update()
        # see http://stackoverflow.com/questions/22767608/sparse-matrix-lp-problems-in-gurobi-python for following code
        Me = csr_matrix(Me)
        for i in range(Me.shape[0]):
            start = Me.indptr[i]
            end   = Me.indptr[i+1]
            variables = [optvar[j] for j in Me.indices[start:end]]
            coeff     = Me.data[start:end]
            m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.EQUAL, rhs=b[i])
        Mi = csr_matrix(Mi)
        for i in range(Mi.shape[0]):
            start = Mi.indptr[i]
            end   = Mi.indptr[i+1]
            variables = [optvar[j] for j in Mi.indices[start:end]]
            coeff     = Mi.data[start:end]
            m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.LESS_EQUAL, rhs=d[i])
        m.optimize()
        if m.Status == grb.GRB.status.OPTIMAL:
            x = np.array([v.x for v in optvar])
            return x, m.getAttr('objVal')
        elif m.Status == grb.GRB.status.INFEASIBLE:
            raise OptimalityError("Primal infeasibility encountered.")
        else:
            raise OptimalityError("Gurobi returned with error status {:d}.".format(m.Status))
    elif solver == "cplex":
        model = cplex.Cplex()
        model.set_log_stream(None)
        model.set_error_stream(None)
        model.set_warning_stream(None)
        model.set_results_stream(None)
        model.parameters.read.scale = -1
        # try to populate by row
        model.objective.set_sense(model.objective.sense.minimize)
        model.variables.add(obj=c[:], lb=[-cplex.infinity for i in range(c.shape[0])])
        Me = csr_matrix(Me)
        for i in range(Me.shape[0]):
            start = Me.indptr[i]
            end   = Me.indptr[i+1]
            variables = [np.asscalar(j) for j in Me.indices[start:end]]
            coeff     = list(Me.data[start:end])
            coeff = [np.asscalar(obj) for obj in coeff]
            btemp = np.asscalar(b[i])
            model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="E", rhs=[btemp])
        Mi = csr_matrix(Mi)
        for i in range(Mi.shape[0]):
            start = Mi.indptr[i]
            end   = Mi.indptr[i+1]
            variables = [np.asscalar(j) for j in Mi.indices[start:end]]
            coeff     = list(Mi.data[start:end])
            coeff     = [np.asscalar(obj) for obj in coeff]
            dtemp = np.asscalar(d[i])
            model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="L", rhs=[dtemp])
        model.solve()
        if model.solution.status[model.solution.get_status()] == 'optimal':
            x = np.array(model.solution.get_values()).ravel()
            return x, model.solution.get_objective_value()
        # elif model.solution.get_status() == 5:
        #     print 'may contain infeasibilities'
        #     x = np.array(model.solution.get_values()).ravel()
        #     return x, model.solution.get_objective_value()
        elif model.solution.status[model.solution.get_status()] == 'infeasible':
            raise OptimalityError("Primal infeasibility encountered.")
        else:
            print model.solution.get_status()
            raise OptimalityError("Cplex returned with error status " + model.solution.status[model.solution.get_status()] + ".")
    elif solver == "soplex":
        model = soplex.Model('deFBA_LP')
        for par in params:
            model.setParam(par, params[par])
        if not 'numerics/feastol' in params:
            model.setParam('numerics/feastol', 1e-8)
            model.setParam('numerics/lpfeastol', 1e-11)
        x = {}
        for i, j in enumerate(c):
            x[i] = model.addVar(lb=None, ub=None, vtype="C", name='x_' + str(i), obj=j)
        Me = csr_matrix(Me)
        for i in range(Me.shape[0]):
            start = Me.indptr[i]
            end = Me.indptr[i + 1]
            variables = [np.asscalar(j) for j in Me.indices[start:end]]
            coeff = list(Me.data[start:end])
            coeff = [np.asscalar(obj) for obj in coeff]
            btemp = np.asscalar(b[i])
            model.addCons(soplex.quicksum(x[variables[j]] * coeff[j] for j in range(len(variables))) == btemp, "equality_%s" % i)
        Mi = csr_matrix(Mi)
        for i in range(Mi.shape[0]):
            start = Mi.indptr[i]
            end = Mi.indptr[i + 1]
            variables = [np.asscalar(j) for j in Mi.indices[start:end]]
            coeff = list(Mi.data[start:end])
            coeff = [np.asscalar(obj) for obj in coeff]
            dtemp = np.asscalar(d[i])
            model.addCons(soplex.quicksum(x[variables[j]] * coeff[j] for j in range(len(variables))) <= dtemp, "inequality_%s" % i)
        model.optimize()
        status = model.getStatus()
        if status == 'optimal':
            output = np.array([model.getVal(x[j]) for j in x]).ravel()
            return output, model.getObjVal()
        elif status == 'infeasible':
            raise OptimalityError("Primal infeasibility encountered.")
        else:
            raise OptimalityError("Soplex returned with error status " + status + ".")
    else:
        raise ValueError("Unknown solver: %s" % solver)
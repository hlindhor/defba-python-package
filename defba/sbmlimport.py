##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
from __future__ import division
import numpy as np
import defbamodel

try:
    import libsbml as sbml

    have_sbml = True
except ImportError, err:
    have_sbml = False
    sbml_import_msg = str(err)


class RamError(Exception):
    """
    empty error class to state that something with the import of the RAM annotations gone wrong
    """
    pass


class SBMLError(Exception):
    """
    empty error class to state that something with the import of the SBML file gone wrong
    """
    pass


def readSBML(filename, scale=1000):
    """
    Convert SBML file to an deFBA model object.
    Required arguments:
    - filename              string. Full name of the .xml file, which you want to import.

    Optional arguments:
    - scale                 float. scaling factor used to normalize the deFBA model.
    """
    if not have_sbml:
        raise ImportError("SBML support requires the libsbml module, but importing this module failed with message: " + sbml_import_msg)
    reader = sbml.SBMLReader()
    document = reader.readSBML(filename)
    if document.isSetModel():
        # Initialize the RamParser object
        parsed = __RamParser(document, scale)
        # return the DefbaModel object
        import defbamodel
        defbaModel = defbamodel.DefbaModel(parsed.stoich, parsed.molecular_weight, parsed.numbers, species=parsed.species,
                                           reactions=parsed.reactions, HC=parsed.HC_matrix, HE=parsed.HE_matrix, HB=parsed.HB_matrix,
                                           biomass_percentage=parsed.biomass_percentage, reversible_fluxes=parsed.reversible_fluxes,
                                           scale=scale, ics=parsed.initial_amounts, storage_species=parsed.storage_species,
                                           maintenance_percentage=parsed.maintenance_percentage, HM=parsed.HM_matrix,
                                           model_name=parsed.model_name, parameter_list=parsed.parameter_list,
                                           objective=parsed.objective_weight, compartments=parsed.compartments,
                                           enzyme_catalyzes=parsed.enzyme_catalyzes, gene_labels=parsed.gene_label)
        return defbaModel
    else:
        raise SBMLError('The SBML file contains no model. Maybe the filename is wrong or the file does not follow SBML standards. Please'
                        ' run the SBML validator at http://sbml.org/Facilities/Validator/index.jsp or use a generic xml checker to find'
                        ' the problem.')


class __RamParser:
    """
    read all necessary information from a SBML file supporting the Resource Allocation Modelling (RAM) annotation standard and convert them
    to the matrix representation of a deFBA model. Minimimal informationen content is the stoichiometric matrix and the molecular weights of
    objective species (macromolecules)
    """

    def __init__(self, document, scale):
        """
        Required arguments:
        - document      libsbml.reader object containing the SBML data
        - scale         float. Scaling factor to normalize the stoichiometric matrix.
        """
        self.scale = scale  # choosing scaling constant for proteins and their according production rates
        self.species = []  # list of names of all species in the model except for fixed external species
        self.HC_matrix = None  # Enzyme Capacity Constraint matrix
        self.HE_matrix = None  # Filter matrix for ECC matrix
        self.HM_matrix = None  # Maintenance matrix
        self.HB_matrix = None  # Biomass composition constraints
        self.reactions = []  # list of names of all reactions in the model
        self.reversible_fluxes = []  # counter to identify reactions, which are not constraint by the enzyme capacities
        external_species = []  # all external species
        fixed_ext_species = []  # external species, which are not dynamically modelled and are not limiting
        protein_species = []  # all macromolecules with a molecular weight (storage, quota, enzymes)
        self.storage_species = []  # list with names of storage species
        metabolite_in_ss = []  # metabolic species operating in steady-state dx/dt = 0
        reaction_catalyzed_by = {}  # dictionary mapping reaction id to the enzyme catalyzing it
        self.kcat_values = {}  # dict with the forward and backward kcats. Format kcat_values['reaction_name'] = [k_forward, k_backward]
        self.initial_amounts = {}  # dict with initial amounts for macromolecules
        self.molecular_weight = {}  # dict with molecular weights for storage, quota and enzymes
        self.objective_weight = {}  # dict with weight for building the optimization objective
        self.biomass_percentage = {}  # used to construct HB matrix
        maintenance_reactions = []  # list with all maintenance reaction ids
        self.maintenance_percentage = {}  # used to construct the HM matrix
        self.parameter_list = {}  # list with parameters for the robust deFBA. Format:
        #  parameter_list['param_name'] = [[location0, location1], value, [exponent1, exponent1], [scaling1, scaling2], 'type']
        # self.param_xxx are used for the construction of the parameter list
        self.param_kcat = {}  # Format: param_kcat['reaction_name'] = [name_string_f, name_string_b]
        self.param_weight = {}  # Format: param_weight['species'] = [parameter_name_sting]
        self.param_ob_weight = {}
        self.param_main = {}
        self.param_biomp = {}
        self.gene_label = {}
        ###
        self.numbers = {}  # dict containing the number of ext_species, metab_species, etc.
        self.compartments = {}  # dict mapping a species id to their respective compartment
        ####
        model = document.getModel()
        if not model:
            raise SBMLError(
                'The SBML file seems to be damaged. Please run the SBML validator at http://sbml.org/Facilities/Validator/index.jsp or use '
                'a generic xml checker to find the problem.')
        self.model_name = model.getId()
        fbc_model = model.getPlugin('fbc')
        # First cycle through the species
        for s in (model.getSpecies(i) for i in xrange(model.getNumSpecies())):
            # noinspection PyUnusedLocal
            bio_species = False
            ids = s.getId()
            if ids in self.species:
                raise SBMLError('The species id ' + ids + ' is used more than once. The ids must be unique species.')
            self.species.append(ids)
            self.compartments[ids] = s.getCompartment()
            annotation = s.getAnnotation()
            weight_str = ''
            # noinspection PyUnusedLocal
            biomp_string = ''
            # Decide whether the external species
            # Look at the annotation field t search for 'ram' information
            if annotation:
                # Because annotations of other types can be present we need to look at each annotation individually to find the RAM element
                found_ram = False
                for child_number in range(annotation.getNumChildren()):
                    test_elem = annotation.getChild(child_number)
                    if test_elem.getName() == 'RAM':
                        found_ram = True
                        url = test_elem.getURI()
                        ram_element = test_elem.getChild(0)
                        break
                if found_ram:
                    # We found a ram annotation. First we identify the species type
                    species_type = ram_element.getAttrValue('speciesType', url)
                    if species_type == 'enzyme' or species_type == 'quota':
                        protein_species.append(ids)
                        bio_species = True
                        if not np.isnan(s.getInitialAmount()):
                            self.initial_amounts[ids] = s.getInitialAmount()
                    elif species_type == 'storage':
                        self.storage_species.append(ids)
                        protein_species.append(ids)
                        bio_species = True
                        if not np.isnan(s.getInitialAmount()):
                            self.initial_amounts[ids] = s.getInitialAmount()
                    elif species_type == 'extracellular':
                        external_species.append(ids)
                        if not np.isnan(s.getInitialAmount()):
                            self.initial_amounts[ids] = s.getInitialAmount()
                            if s.getConstant() or s.getBoundaryCondition():
                                fixed_ext_species.append(ids)
                    elif species_type == 'metabolite':
                        metabolite_in_ss.append(ids)
                    else:
                        raise RamError('unknown species type found in the RAM annotation in species ' + ids)
                    # try to import the molecular weight
                    # Usually RAM attributes contain a sting pointing to a parameter. This try block enables absolute values instead.
                    # if bio_species is True:
                    try:
                        weight = np.fabs(float(ram_element.getAttrValue('molecularWeight', url)))
                        if bio_species:
                            self.molecular_weight[ids] = weight
                        elif weight != 0:
                            print 'The species ' + ids + ' has got a molecular weight but is not a biomass species. We will handle it as' \
                                                         ' zero.'
                            raw_input("Press Enter to continue...")
                    except ValueError:
                        weight_str = ram_element.getAttrValue('molecularWeight', url)
                        if weight_str:
                            try:
                                weight = np.fabs(float(model.getParameter(weight_str).getValue()))
                                if bio_species:
                                    self.param_weight[ids] = weight_str
                                    self.molecular_weight[ids] = weight
                                elif weight != 0:
                                    print 'The species ' + ids + ' has got a molecular weight but is not a biomass species. We will handle' \
                                                                 ' it as zero.'
                                    raw_input("Press Enter to continue...")
                            except AttributeError:
                                raise RamError('The parameter ' + weight_str + ' has no value.')
                        else:
                            if bio_species:
                                raise RamError(
                                    'The molecular weight of the species ' + ids + ' is not set. But the species ' + ids +
                                    ' is supposed to be a biomass species. Please correct the error in the SBML file')
                    # try to import the objective weight. This coincidences with the molecular weight most of the time. Usually differs for
                    # storage and quota species. Try blocks again enables absolute values as attributes.
                    try:
                        ob_weight = np.fabs(float(ram_element.getAttrValue('objectiveWeight', url)))
                        if bio_species:
                            self.objective_weight[ids] = ob_weight
                        elif ob_weight != 0:
                            print 'The species ' + ids + ' has got an objective weight but is not a biomass species. We will handle it ' \
                                                         'as zero.'
                            raw_input("Press Enter to continue...")
                    except ValueError:
                        ob_weight_str = ram_element.getAttrValue('objectiveWeight', url)
                        if ob_weight_str:
                            try:
                                ob_weight = np.fabs(float(model.getParameter(ob_weight_str).getValue()))
                                if bio_species:
                                    self.param_ob_weight[ids] = weight_str
                                    self.objective_weight[ids] = ob_weight
                                elif ob_weight != 0:
                                    print 'The species ' + ids \
                                          + ' has got an objective weight but is not a biomass species. We will handle it as zero.'
                                    raw_input("Press Enter to continue...")
                            except AttributeError:
                                raise RamError('The parameter ' + ob_weight_str + ' has no value.')
                        else:
                            if bio_species:
                                raise RamError(
                                    'The objective weight of the species ' + ids + ' is not set. But the species ' + ids +
                                    ' is supposed to be a biomass species. Please correct the error in the SBML file')
                    # Try to import the biomass percentage for non enzymatic macromolecules (construction HB matrix)
                    try:
                        biomp_value = np.fabs(float(ram_element.getAttrValue('biomassPercentage', url)))
                    except ValueError:
                        biomp_string = ram_element.getAttrValue('biomassPercentage', url)
                        if biomp_string:
                            try:
                                biomp_value = np.fabs(float(model.getParameter(biomp_string).getValue()))
                                # biomass percentage is only used in the HB matrix
                                if biomp_value > 0 and biomp_value < 1:
                                    self.param_biomp[ids] = biomp_string
                                elif biomp_value == 0:
                                    pass
                                else:
                                    raise RamError('The parameter ' + biomp_string + ' does not have a value between 0 and 1.')
                            except AttributeError:
                                print 'The parameter ' + biomp_string + ' has no value.'
                                raise RamError('The parameter ' + biomp_string + ' has no value.')
                        else:
                            biomp_value = 0
                    if not biomp_value == 0 and bio_species:
                        self.biomass_percentage[ids] = biomp_value
                    elif not biomp_value == 0 and not bio_species:
                        print 'The species ' + ids + ' has got a non-zero biomass percentage. Should  ' + ids \
                              + ' be a biomass species? We are treating it as zero.'
                        raw_input("Press Enter to continue...")
                elif s.getConstant() or s.getBoundaryCondition():
                    external_species.append(ids)
                    fixed_ext_species.append(ids)
                else:
                    raise SBMLError(
                        'The species ' + ids + ' is neither a fixed external species nor does it have a RAM annotation. Stopping import.')
            # Identify fixed external species
            elif s.getConstant() or s.getBoundaryCondition():
                external_species.append(ids)
                fixed_ext_species.append(ids)
            else:
                raise SBMLError(
                    'The species ' + ids + ' is neither a fixed external species nor does it have a RAM annotation. Stopping import.')
        # number of reactions without gene association counts the spontanious reactions. If the model contains a lot of these, you might
        # want to add energy maintenance to lower these growth rate.
        number_of_reactions_without_gene_association = 0
        self.stoich = np.zeros((len(self.species), model.getNumReactions()))  # stoic is the stoichiometric matrix
        # Loop over all reactions. gather stoichiometry, reversibility, kcats and gene associations
        for (j, r) in enumerate(model.getReaction(i) for i in xrange(model.getNumReactions())):
            rid = r.getId()
            self.reactions.append(rid)
            if r.getReversible():
                self.reversible_fluxes.append(rid)
            for thisspecies in r.getListOfReactants():
                xi = self.species.index(thisspecies.getSpecies())
                self.stoich[xi, j] -= thisspecies.getStoichiometry()
            for thisspecies in r.getListOfProducts():
                xi = self.species.index(thisspecies.getSpecies())
                self.stoich[xi, j] += thisspecies.getStoichiometry()
            reaction_fbc = r.getPlugin('fbc')
            has_gene_ass = False
            # try to identify the gene association of the reaction
            try:
                gene_product_id = reaction_fbc.getGeneProductAssociation().all_elements[0].getGeneProduct()
                gene_product = fbc_model.getGeneProduct(gene_product_id)
                has_gene_ass = True
                if gene_product.getAssociatedSpecies() == '':
                    if gene_product_id in self.species:
                        reaction_catalyzed_by[rid] = gene_product_id
                    else:
                        raise RamError('The reaction ' + rid + ' has an empty fbc:geneProductRef()')
                else:
                    enzyme_id = gene_product.getAssociatedSpecies()
                    if enzyme_id in self.species:
                        reaction_catalyzed_by[rid] = enzyme_id
                        self.gene_label[enzyme_id] = gene_product.getLabel()
                    else:
                        raise RamError('The fbc:geneAssociation for the geneProduct ' + gene_product_id + ' is pointing to an unknown'
                                                                                                          ' species')
            except (AttributeError, IndexError) as e:
                number_of_reactions_without_gene_association += 1
            # if no geneAssociation is present, the default kcats are zero.
            k_forward = 0.0
            k_backward = 0.0
            annotation = r.getAnnotation()
            if annotation:
                # look if a RAM annotation field is present
                found_ram = False
                for child_number in range(annotation.getNumChildren()):
                    test_elem = annotation.getChild(child_number)
                    if test_elem.getName() == 'RAM':
                        found_ram = True
                        url = test_elem.getURI()
                        ram_element = test_elem.getChild(0)
                        break
                if found_ram:
                    name_string_f = ''
                    name_string_b = ''
                    # noinspection PyUnusedLocal
                    value = 0
                    # try to import absolute value for scaling of maintenance reactions
                    try:
                        value = np.fabs(float(ram_element.getAttrValue('maintenanceScaling', url)))
                    # get the parameter value instead
                    except ValueError:
                        try:
                            value = np.fabs(float(model.getParameter(ram_element.getAttrValue('maintenanceScaling', url)).getValue()))
                            if value > 0:
                                # Maintenance scaling is only used in the construction of HM
                                self.param_main[rid] = ram_element.getAttrValue('maintenanceScaling', url)
                        except AttributeError:
                            raise RamError('The parameter ' + ram_element.getAttrValue('maintenanceScaling', url)
                                           + ' got no value or is not defined.')
                    if value != 0:
                        maintenance_reactions.append(rid)
                        self.maintenance_percentage[rid] = value
                    # Import kcat values. Irreversible reactions have a zero backward kcat.
                    # Try to import absolute value first.
                    try:
                        k_forward = np.fabs(float(ram_element.getAttrValue('kcatForward', url)))
                    except ValueError:
                        name_string_f = ram_element.getAttrValue('kcatForward', url)
                        if name_string_f:
                            try:
                                k_forward = np.fabs(float(model.getParameter(name_string_f).getValue()))
                            except AttributeError:
                                raise RamError('The parameter ' + name_string_f + ' got no value or is not defined.')
                    # try to import absolute value for backward kcat
                    try:
                        k_backward = np.fabs(float(ram_element.getAttrValue('kcatBackward', url)))
                    except ValueError:
                        name_string_b = ram_element.getAttrValue('kcatBackward', url)
                        if name_string_b:
                            try:
                                k_backward = np.fabs(float(model.getParameter(name_string_b).getValue()))
                            except AttributeError:
                                raise RamError('The parameter ' + name_string_b + ' got no value or is not defined.')
                    if k_forward == 0 and has_gene_ass:
                        raise RamError(
                            'The reaction ' + rid + ' has no forward kcat value but is regarded to be catalyzed by the enzyme ' +
                            reaction_catalyzed_by[rid])
                    elif k_forward == 0 and k_backward != 0:
                        raise RamError(
                            'The reaction ' + rid + ' has no forward kcat value but a non-zero backward kcat. '
                                                    'This is not allowed in the RAM standard.')
                    elif k_forward != 0 and not has_gene_ass:
                        print 'The reaction ' + rid + ' has a forward kcat value but no gene association. There may be a problem.'
                        raw_input("Press Enter to continue...")
                    elif k_backward != 0 and rid not in self.reversible_fluxes:
                        print 'The reaction ' + rid + ' has a non zero backward kcat value, but the reaction is defined to be irreversible.' \
                                                      ' We\'ll handle it as zero.'
                        if has_gene_ass:
                            self.param_kcat[rid] = [name_string_f, '']
                            self.kcat_values[rid] = [k_forward, 0.0]
                        raw_input("Press Enter to continue...")
                    elif k_backward == 0 and rid in self.reversible_fluxes and has_gene_ass:
                        raise SBMLError(
                            'The reaction ' + rid + ' has a zero backward kcat value, but the reaction is defined to be reversible --> divide by zero error.')
                    elif has_gene_ass:
                        # save the parameter names
                        self.param_kcat[rid] = [name_string_f, name_string_b]
                        # save the parameter values
                        self.kcat_values[rid] = [k_forward, k_backward]
        # Sorting process of reactions to create deFBA standard form ~[exchange_reactions, metabolic reactions, biomass_production]
        external_species_location = []
        protein_species_location = []
        # locate the external species in the 'species' vector
        for location, specie in enumerate(self.species):
            if specie in external_species:
                external_species_location.append(location)
        ext_species_number = len(external_species_location)
        # sort the external species to the beginning the species list
        # and adapt the stoichiometric matrix
        for index in external_species_location:
            name = self.species[index]
            del self.species[index]
            self.species.insert(0, name)
            temp_left = self.stoich[index:index + 1, :].copy()
            temp_middle = self.stoich[:index, :].copy()
            temp_right = self.stoich[index + 1:, :].copy()
            self.stoich = np.vstack((temp_left, temp_middle, temp_right))
        del external_species_location
        # locate all macromolecules (storage, quota, enzymes)
        for location, specie in enumerate(self.species):
            if specie in protein_species:
                protein_species_location.append(location)
        protein_number = len(protein_species_location)
        metab_species_number = len(metabolite_in_ss)
        # sort the macromolecules to the end of the species list
        counter = 0
        for index in protein_species_location:
            name = self.species[index - counter]
            del self.species[index - counter]
            self.species.append(name)
            temp_left = self.stoich[:index - counter, :].copy()
            temp_middle = self.stoich[index - counter + 1:, :].copy()
            temp_right = self.stoich[index - counter:index - counter + 1, :].copy()
            self.stoich = np.vstack((temp_left, temp_middle, temp_right))
            counter += 1
        del protein_species_location
        self.protein_reactions = []
        exchange_reactions_location = []
        protein_reactions_location = []
        # locate protein production reactions
        for column in xrange(self.stoich.shape[1]):
            if np.count_nonzero(self.stoich[-protein_number:, column]) > 0:
                protein_reactions_location.append(column)
        for column, reac in enumerate(self.reactions):
            if reac in maintenance_reactions:
                protein_reactions_location.append(column)
        protein_reactions_location.sort()
        protein_reactions_number = len(protein_reactions_location)
        # sort the macromolecule producing reactions to the end of the array
        counter = 0
        for index in protein_reactions_location:
            temp = self.reactions[index - counter]
            self.protein_reactions.append(temp)
            del self.reactions[index - counter]
            self.reactions.append(temp)
            temp_left = self.stoich[:, :index - counter].copy()
            temp_middle = self.stoich[:, index - counter + 1:].copy()
            temp_right = self.stoich[:, index - counter:index - counter + 1].copy()
            self.stoich = np.hstack((temp_left, temp_middle, temp_right))
            counter += 1
        # locate exchange reactions
        for column in xrange(self.stoich.shape[1] - protein_reactions_number):
            if np.count_nonzero(self.stoich[0:ext_species_number, column]) > 0:
                exchange_reactions_location.append(column)
        exch_reactions_number = len(exchange_reactions_location)
        # sort the exchange reactions to the beginning of the reactions list
        for index in exchange_reactions_location:
            name = self.reactions[index]
            del self.reactions[index]
            self.reactions.insert(0, name)
            temp_left = self.stoich[:, index:index + 1].copy()
            temp_middle = self.stoich[:, :index].copy()
            temp_right = self.stoich[:, index + 1:].copy()
            self.stoich = np.hstack((temp_left, temp_middle, temp_right))
        del exchange_reactions_location
        del protein_reactions_location
        # start deleting boundary species, which are not modelled dynamically
        for name in fixed_ext_species:
            index = self.species.index(name)
            self.stoich = np.vstack((self.stoich[0:index], self.stoich[index + 1:]))
            external_species.remove(name)
            self.species.remove(name)
        ext_species_number -= len(fixed_ext_species)
        # end deleting boundary species
        metab_reactions_number = len(self.reactions) - protein_reactions_number - exch_reactions_number
        # end of sorting procecces
        # because a single enzyme can catalyze multiple reactions, we dont get a 1-to-1 mapping and need the inverse dict as well
        self.enzyme_catalyzes = {}
        for j, reactionname in enumerate(reaction_catalyzed_by):
            try:
                self.enzyme_catalyzes[reaction_catalyzed_by[reactionname]].append(reactionname)
            except KeyError:
                self.enzyme_catalyzes[reaction_catalyzed_by[reactionname]] = [reactionname]
        # Construct HC, HE matrices
        # eliminate the empty set from the dict
        self.enzyme_catalyzes = {k: v for k, v in self.enzyme_catalyzes.items() if v != []}
        # Go through enzymes and construct the enzyme capacity constraints accordingly
        for enz, reac_array in self.enzyme_catalyzes.items():
            reversible_reactions = []
            irreversible_reactions = []
            for reac in reac_array:
                if reac in self.reversible_fluxes:
                    reversible_reactions.append(reac)
                else:
                    irreversible_reactions.append(reac)
            self.__constructHCHEmatrix(enz, reversible_reactions, irreversible_reactions)
        self.numbers['ext_species'] = ext_species_number
        self.numbers['exch_reactions'] = exch_reactions_number
        self.numbers['metab_reactions'] = metab_reactions_number
        self.numbers['metab_species'] = metab_species_number
        self.numbers['protein_reactions'] = protein_reactions_number
        self.numbers['proteins'] = protein_number
        self.__constructHBmatrix()
        self.__constructHMmattrix()

        list_of_gene_products = fbc_model.getListOfGeneProducts()
        for gene_product in list_of_gene_products:
            associated_species = gene_product.getAssociatedSpecies()
            label = gene_product.getLabel()
            self.gene_label[associated_species] = label
        print 'Number of free Reactions (no gene association)'
        print number_of_reactions_without_gene_association

    def __constructHMmattrix(self):
        """
        Constructs the HM matrix
        """
        if self.maintenance_percentage:
            self.HM_matrix = np.zeros((len(self.maintenance_percentage), len(self.reactions)))
            for row, reac in enumerate(self.maintenance_percentage.iterkeys()):
                self.HM_matrix[row, self.reactions.index(reac)] = 1.0 / self.maintenance_percentage[reac]
                if reac in self.protein_reactions:
                    self.parameter_list[self.param_main[reac]] = [['HM[' + str(row) + ',' + str(self.reactions.index(reac)) + ']'],
                                                                  self.maintenance_percentage[reac], [-1], [1], 'maintenance']
                else:
                    self.parameter_list[self.param_main[reac]] = [
                        ['HM[' + str(row) + ',' + str(self.reactions.index(reac)) + ']'],
                        self.maintenance_percentage[reac], [-1], [self.scale], 'maintenance']

    def __constructHBmatrix(self):
        """
        Construct the HB matrix
        """
        for specie_w in self.param_ob_weight:
            if not specie_w in self.param_weight:
                self.parameter_list[self.param_ob_weight[specie_w]] = [
                    ['objective[' + str(self.species.index(specie_w)) + ']'], self.objective_weight[specie_w], [1], [1],
                    'objective']
            else:
                self.parameter_list[self.param_ob_weight[specie_w]] = [
                    ['objective[' + str(self.species.index(specie_w)) + ']'], self.objective_weight[specie_w], [1], [1],
                    'ob_weight']
        for specie_w in self.param_weight:
            if not specie_w in self.param_ob_weight:
                self.parameter_list[self.param_weight[specie_w]] = [[], self.molecular_weight[specie_w], [], [],
                                                                    'weight']
        if self.biomass_percentage:
            self.HB_matrix = np.zeros((len(self.biomass_percentage), len(self.species)))
            biomass_vector = np.zeros(len(self.species))
            for j, specie in enumerate(self.species[-self.numbers['proteins']:]):
                biomass_vector[j - self.numbers['proteins']] = self.molecular_weight[specie]
            for i in self.param_biomp.iterkeys():
                self.parameter_list[self.param_biomp[i]] = [[], self.biomass_percentage[i], [], [], 'biom_percent']
            for row, specie in enumerate(self.biomass_percentage.iterkeys()):
                for specie_w in self.param_weight:
                    self.parameter_list[self.param_weight[specie_w]][0].append(
                        'HB[' + str(row) + ',' + str(self.species.index(specie_w)) + ']')
                    self.parameter_list[self.param_weight[specie_w]][2].append(1)
                    if specie_w == specie:
                        self.parameter_list[self.param_weight[specie_w]][3].append(self.biomass_percentage[specie] - 1)
                    else:
                        self.parameter_list[self.param_weight[specie_w]][3].append(self.biomass_percentage[specie])
                for coloumn in range(self.numbers['proteins']):
                    self.HB_matrix[row, coloumn - self.numbers['proteins']] = self.molecular_weight[
                                                                                  self.species[coloumn - self.numbers['proteins']]] * \
                                                                              self.biomass_percentage[specie]
                    try:
                        self.parameter_list[self.param_biomp[specie]][0].append(
                            'HB[' + str(row) + ',' + str(coloumn - self.numbers['proteins']) + ']')
                        self.parameter_list[self.param_biomp[specie]][2].append(1)
                        if specie == self.species[coloumn - self.numbers['proteins']]:
                            self.HB_matrix[row, coloumn - self.numbers['proteins']] = (self.biomass_percentage[specie] - 1) * \
                                                                                      self.molecular_weight[specie]
                            self.parameter_list[self.param_biomp[specie]][3].append(-1)
                        else:
                            self.parameter_list[self.param_biomp[specie]][3].append(1)
                    except KeyError:
                        if specie == self.species[coloumn - self.numbers['proteins']]:
                            self.HB_matrix[row, coloumn - self.numbers['proteins']] = (self.biomass_percentage[specie] - 1) * \
                                                                                  self.molecular_weight[specie]



    def __constructHCHEmatrix(self, enz, reversible_reactions, irreversible_reactions):
        """
        Construct the enzyme capacity constraints iterativly.
        Adds the constraints for the enzyme enz to the already present HC, HE matrices.
        """
        # include forward kcat values for the irreversible reactions
        new_row_number = 2 ** len(reversible_reactions)
        new_HC = np.zeros((new_row_number, len(self.reactions)))
        new_HCtemp = np.zeros((1, len(self.reactions)))
        # check if HC is already present.
        if not hasattr(self.HC_matrix, 'shape'):
            HC_size = 0
        else:
            HC_size = self.HC_matrix.shape[0]
        # First we set the elements for the irreversible reactions.
        # While RAM enforces the backward kcat for irreversible reactions to be zero. This method can handle irreversible reactions with
        # k_forward = 0 and k_backward != 0.
        for irr in irreversible_reactions:
            if not self.kcat_values[irr][0] == 0:
                # Decide whether the parameter will be scaled in the deFBA model.
                if irr in self.protein_reactions:
                    self.parameter_list[self.param_kcat[irr][0]] = [
                        ['HC[' + str(HC_size) + ',' + str(self.reactions.index(irr)) + ']'], self.kcat_values[irr][0],
                        [-1], [1], 'kcat']
                else:
                    self.parameter_list[self.param_kcat[irr][0]] = [
                        ['HC[' + str(HC_size) + ',' + str(self.reactions.index(irr)) + ']'], self.kcat_values[irr][0],
                        [-1], [self.scale], 'kcat']
                # Add all positions of the parameter
                for i in range(1, new_row_number):
                    self.parameter_list[self.param_kcat[irr][0]][0].append(
                        'HC[' + str(HC_size + i) + ',' + str(self.reactions.index(irr)) + ']')
                    self.parameter_list[self.param_kcat[irr][0]][2].append(-1)
                    if irr in self.protein_reactions:
                        self.parameter_list[self.param_kcat[irr][0]][3].append(1)
                    else:
                        self.parameter_list[self.param_kcat[irr][0]][3].append(self.scale)
                # Put the values in
                new_HCtemp[0, self.reactions.index(irr)] = 1.0 / (self.kcat_values[irr][0])
            # handle backward pointing reactions
            elif not self.kcat_values[irr][1] == 0:
                if irr in self.protein_reactions:
                    self.parameter_list[self.param_kcat[irr][1]] = [
                        ['HC[' + str(HC_size) + ',' + str(self.reactions.index(irr)) + ']'], self.kcat_values[irr][1],
                        [-1], [-1], 'kcat']
                else:
                    self.parameter_list[self.param_kcat[irr][1]] = [
                        ['HC[' + str(HC_size) + ',' + str(self.reactions.index(irr)) + ']'], self.kcat_values[irr][1],
                        [-1], [-self.scale], 'kcat']
                for i in range(1, new_row_number):
                    self.parameter_list[self.param_kcat[irr][1]][0].append(
                        'HC[' + str(HC_size + i) + ',' + str(self.reactions.index(irr)) + ']')
                    self.parameter_list[self.param_kcat[irr][1]][2].append(-1)
                    if irr in self.protein_reactions:
                        self.parameter_list[self.param_kcat[irr][1]][3].append(-1)
                    else:
                        self.parameter_list[self.param_kcat[irr][1]][3].append(-self.scale)
                new_HCtemp[0, self.reactions.index(irr)] = -1.0 / (self.kcat_values[irr][1])
        # state which enzyme is in play
        new_HE = np.zeros((new_row_number, len(self.species)))
        new_HEtemp = np.zeros((1, len(self.species)))
        new_HEtemp[0, self.species.index(enz)] = 1
        # generate templates
        for rows in xrange(2 ** (len(reversible_reactions))):
            new_HC[rows, :] = new_HCtemp
            new_HE[rows, :] = new_HEtemp
        # first column
        if reversible_reactions:
            for rev_reac in reversible_reactions:
                self.parameter_list[self.param_kcat[rev_reac][0]] = [[], self.kcat_values[rev_reac][0], [], [], 'kcat']
                self.parameter_list[self.param_kcat[rev_reac][1]] = [[], self.kcat_values[rev_reac][1], [], [], 'kcat']
            for i in xrange(new_row_number):
                if (-1) ** i == 1:
                    self.parameter_list[self.param_kcat[reversible_reactions[0]][0]][0].append(
                        'HC[' + str(HC_size + i) + ',' + str(self.reactions.index(reversible_reactions[0])) + ']')
                    self.parameter_list[self.param_kcat[reversible_reactions[0]][0]][2].append(-1)
                    if reversible_reactions[0] in self.protein_reactions:
                        self.parameter_list[self.param_kcat[reversible_reactions[0]][0]][3].append(1)
                    else:
                        self.parameter_list[self.param_kcat[reversible_reactions[0]][0]][3].append(self.scale)
                    new_HC[i, self.reactions.index(reversible_reactions[0])] = 1.0 / (
                        self.kcat_values[reversible_reactions[0]][0])
                else:
                    self.parameter_list[self.param_kcat[reversible_reactions[0]][1]][0].append(
                        'HC[' + str(HC_size + i) + ',' + str(self.reactions.index(reversible_reactions[0])) + ']')
                    self.parameter_list[self.param_kcat[reversible_reactions[0]][1]][2].append(-1)
                    if reversible_reactions[0] in self.protein_reactions:
                        self.parameter_list[self.param_kcat[reversible_reactions[0]][1]][3].append(-1)
                    else:
                        self.parameter_list[self.param_kcat[reversible_reactions[0]][1]][3].append(-self.scale)
                    new_HC[i, self.reactions.index(reversible_reactions[0])] = -1.0 / (
                        self.kcat_values[reversible_reactions[0]][1])
            for j in xrange(1, len(reversible_reactions)):
                for i in xrange(new_row_number):
                    if not ((i % (2 ** (j + 1))) < 2 ** j):
                        self.parameter_list[self.param_kcat[reversible_reactions[j]][1]][0].append(
                            'HC[' + str(HC_size + i) + ',' + str(self.reactions.index(reversible_reactions[j])) + ']')
                        self.parameter_list[self.param_kcat[reversible_reactions[j]][1]][2].append(-1)
                        if reversible_reactions[j] in self.protein_reactions:
                            self.parameter_list[self.param_kcat[reversible_reactions[j]][1]][3].append(-1)
                        else:
                            self.parameter_list[self.param_kcat[reversible_reactions[j]][1]][3].append(-self.scale)
                        new_HC[i, self.reactions.index(reversible_reactions[j])] = -1.0 / (
                            self.kcat_values[reversible_reactions[j]][1])
                    else:
                        self.parameter_list[self.param_kcat[reversible_reactions[j]][0]][0].append(
                            'HC[' + str(HC_size + i) + ',' + str(self.reactions.index(reversible_reactions[j])) + ']')
                        self.parameter_list[self.param_kcat[reversible_reactions[j]][0]][2].append(-1)
                        if reversible_reactions[j] in self.protein_reactions:
                            self.parameter_list[self.param_kcat[reversible_reactions[j]][0]][3].append(1)
                        else:
                            self.parameter_list[self.param_kcat[reversible_reactions[j]][0]][3].append(self.scale)
                        new_HC[i, self.reactions.index(reversible_reactions[j])] = 1.0 / (
                            self.kcat_values[reversible_reactions[j]][0])
        # if no the method is called the first time, create a HC matrix
        if not hasattr(self.HC_matrix, 'shape'):
            self.HC_matrix = new_HC
            self.HE_matrix = new_HE
        # else add the new constraints
        else:
            self.HC_matrix = np.r_[self.HC_matrix, new_HC]
            self.HE_matrix = np.r_[self.HE_matrix, new_HE]

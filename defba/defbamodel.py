##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
from __future__ import division

import numpy as np
from numpy import linalg
import csv
import scipy.sparse as spr
from scipy.sparse import csr_matrix
from scipy.optimize import newton
import linopt
import midopt
import sbmlexport
import sbmlimport
import sbmltricks


class Store(object):
    pass


globals = Store()
# check which solvers are installed
try:
    import gurobipy as grb

    globals.have_gurobi = True
except ImportError:
    globals.have_gurobi = False
    print 'gurobi not installed'
try:
    import cvxopt
    import cvxopt.solvers

    globals.have_cvxopt = True
except ImportError:
    print 'cvxopt not installed'
    globals.have_cvxopt = False

try:
    import cplex

    globals.have_cplex = True
except ImportError:
    print 'cplex not installed'
    globals.have_cplex = False

try:
    import pyscipopt as soplex

    globals.have_soplex = True
except ImportError:
    print 'soplex not installed'
    globals.have_soplex = False

try:
    import matplotlib.pyplot as plt

    globals.have_matplotlib = True
except ImportError:
    print 'matplotlib not installed'
    globals.have_matplotlib = False


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def export_states(times, trajectory, species, scale, non_protein_species, file_name=None):
    """
    generate csv data files for external plotting

    Required arguments:
    - times                     list with discretization points for time
    - traj                      either a linopt.collocatedTrajectory, a midopt.MidpointTrajectory or a ndarray containing the solution
                                curves for the states
    - species                   list with names for the species
    - scale                     scaling factor for proteins
    - non_protein_species       list with names of external species and metabolites

    Optional arguments:
    - file_name                 string containing the file_name to which the states are saved
    """
    if file_name is not None:
        resfile = open(file_name + '.csv', "w")
    else:
        resfile = open("species.csv", "w")
    reswriter = csv.writer(resfile, delimiter="\t")
    reswriter.writerow(['time'] + [name for name in species])
    try:
        for i in range(len(times)):
            states = trajectory.get_state(times[i])
            states = [j if number < non_protein_species else j / float(scale) for number, j in enumerate(states)]
            reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], states))])
    except AttributeError:
        for i in range(len(times)):
            states = trajectory[i]
            states = [j if number < non_protein_species else j / float(scale) for number, j in enumerate(states)]
            reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], states))])
    resfile.close()


def export_fluxes(times, trajectory, reactions, scale, non_protein_reac_number, file_name=None):
    """
    generate csv files for external plotting of result fluxes

    Required arguments:
    - times                     list with discretization points for time
    - traj                      either a linopt.collocatedTrajectory, a midopt.MidpointTrajectory or a ndarray containing the solution
                                curves for the states
    - species                   list with names for the species
    - scale                     scaling factor for protein producing reactions
    - non_protein_species       list with names of exchange and metabolic reactions

    Optional arguments:
    - file_name                 string containing the file_name to which the states are saved

    """
    if file_name:
        resfile = open(file_name + '.csv', "w")
    else:
        resfile = open("fluxes.dat", "w")
    reswriter = csv.writer(resfile, delimiter="\t")
    reswriter.writerow(['time'] + [name for name in reactions])
    try:
        for i in xrange(len(times)):
            # if reduced:
            #     controls = np.dot(U, trajectory.get_control(times[i]))
            #     controls = [j if number < non_protein_reac_number else j / float(scale) for number, j in enumerate(controls)]
            #     reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], controls))])
            # else:
            controls = trajectory.get_control(times[i])
            controls = [j if number < non_protein_reac_number else j / float(scale) for number, j in enumerate(controls)]
            reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], controls))])
    except AttributeError:
        min_size = np.min((len(times), trajectory.shape[0]))
        for i in xrange(min_size):
            controls = trajectory[i]
            controls = [j if number < non_protein_reac_number else j / float(scale) for number, j in enumerate(controls)]
            reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], controls))])
    resfile.close()


def block_diagonal(arrays, sparseformat=None):
    """
    Returns as parse matrix in block-diagonal form containing the matrices from arrays as blocks

    Required arguments:
    - arrays                list of matrices

    Optional arguments:
    - sparseformat          string defining the sparsity pattern (lil, coo, dok, csr, crs, ccs, csc)
                            In the DefbaModel class we only use the 'lil' format.
    """
    if sparseformat is None:
        res = np.zeros((sum(a.shape[0] for a in arrays), sum(a.shape[1] for a in arrays)))
        for i, a in enumerate(arrays):
            ind0 = sum(ai.shape[0] for ai in arrays[:i])
            ind1 = sum(ai.shape[1] for ai in arrays[:i])
            res[ind0:ind0 + a.shape[0], ind1:ind1 + a.shape[1]] = a
    else:
        res = spr.block_diag(arrays, format=sparseformat)
    return res


class DefbaModel(object):
    """
    This class represents a biochemical reaction network, specifically the ODE of the form
    zdot = S * v(z), where S is the stoichiometric matrix, v the reaction rate vector, and
    x the species concentration vector.
    The reactions vector consists of v=(v_y, v_x, v_p), with exchange reactions v_y,
    metabolite transformations v_x, and biomass/enzyme production v_p.
    Species vector consists of z=(y,x,p), with external species y, metabolites x, and
    proteins p.
    The information about catalyzing enzymes is encoded via H_c * v <= H_E * p.
    Biomass composition constraint H_B * x <=0;
    Maintenance constraints H_A *v \geq H_F p
    """

    def __init__(self, stoich, biomass, numbers, species=None, reactions=None, objective=None, ics=None, initial_fluxes=None, scale=1,
                 storage_species=None, HC=None, HE=None, HB=None, biomass_percentage=None, HM=None, maintenance_percentage=None,
                 reversible_fluxes=None, model_name='deFBA_model', R=None, q=None, parameter_list=None, compartments=None,
                 enzyme_catalyzes=None, gene_labels=None, eps=10**-6, delete_metabolites=False):
        """
        DeFbaModel constructor.
        
        Required arguments:
        - stoich                        stoichiometric matrix of the network
        - biomass                       Either a dict containing the molecular weights of the macromolecules P
                                        keys are species names, values are weight or a list with all weights as entries
        - numbers                       dict containing the amounts external and metabolic species, proteins, and the different amounts of
                                        reactions resp.

        Optional arguments:
        - species:                      list of species names
        - reactions:                    list of reaction names
        - objective                     contains the weight of the macromolecules that are to be counted towards the objective function
                                        usually identical to biomass_vec
        - storage_species               list of names of the storage species
                                        names must be included in species argument. storage species are not counted towards objective
        - biomass_percentage            dict containing names of macromolecules and their resp ratio in the total biomass
                                        used to construct H_B matrix if not given directly
        - maintenance_percentage        dict containing reaction names and the scaling factor to biomass
                                        used to construct H_M matrix if not given directly
        - HC                            enzyme capacity constraint (ECC) matrix
        - HE                            filter matrix to determine ECC constraint
        - HB                            biomass composition matrix
        - HM                            maintenance constraint matrix
        - ics:                          dict of initial conditions
                                        keys are species names, values are numbers for initial amounts
        - initial_fluxes               list of initial values for fluxes
        - scale:                        integer value for scaling the system
        - reversible fluxes             list of names of reversible reactions
        - parameter_list                dict with all parameter names, their locations and their values. Necessary for robust deFBA
                                        format: parameter_list['param_name'] = [[location0, location1], value, [exponent1, exponent1],
                                         [scaling1, scaling2], 'type']
        - enzyme_catalyzes              dict mapping enzymes to the reaction they are catalyzing
        - gene_labels                   dict stating the recipe for the macromolecules in terms of gene ids
        """
        # self.preds is a helper variable to save the different prediction horizons during the use of shortterm deFBA
        self.preds = []
        self.uncertain_environment = None
        self.eps = eps
        # Stoichiometric matrix
        self.stoich = np.copy(np.atleast_2d(stoich))

        # numbers contains 'ext_species' , 'exch_reactions', 'metab_reactions', 'metab_species', 'proteins', 'protein_reduction'
        self.numbers = numbers

        self.enzyme_catalyzes = enzyme_catalyzes
        self.gene_labels = gene_labels
        # fallback if species names are not given
        if species is None:
            self.species = ['x' + str(i) for i in xrange(self.stoich.shape[0])]
        else:
            self.species = species

        # fall back if reactions have no given names
        if reactions is None:
            self.reactions = ['v' + str(i) for i in xrange(self.stoich.shape[1])]
        else:
            self.reactions = reactions

        # initialize the biomass vector containing the molecular weight of all storage, enzyme or quota species
        # dicts are preferred to ensure correct mappings between weight and species
        if isinstance(biomass, dict):
            self.biomass = np.zeros(len(species))
            try:
                for j, species_name in enumerate(species):
                    if species_name in biomass.iterkeys():
                        self.biomass[j] = biomass[species_name]
            except KeyError:
                print 'molecular weights for some macromolecules or storage species missing! Please add the needed information'
                raise ValueError('no molecular weight given for ' + str(species_name))
        elif isinstance(biomass, list) or isinstance(biomass, np.ndarray):
            if len(biomass) == len(self.species):
                self.biomass = biomass
            else:
                raise ValueError('The given biomass vector for the species has the wrong length')
        else:
            raise ValueError('molecular weights must be given as either dict or a list')

        # Create an empty variable to save the latest results of the simulation methods
        self.results = None

        # Set the name of the model
        self.name = model_name

        # set scaling value for storage and proteins
        self.scale = float(scale)

        # if compartments are known, save these for export
        if compartments is None:
            self.compartments = {}
        else:
            self.compartments = compartments

        # parameter list has the form parameters['name_of_parameter']=[location, value, exponent, scale],
        # e.g. location = HC[1,2], value = 20, exponent = -1, scale = 100
        if parameter_list is None:
            self.parameter_list = {}
        else:
            self.parameter_list = parameter_list

        # Handling the vector used in the objective functional
        self.storage_species = storage_species
        if objective is not None:
            self.objective_vector = np.zeros(len(self.species))
            for specie in objective:
                self.objective_vector[self.species.index(specie)] = objective[specie]
        elif storage_species is not None:
            self.storage_species = storage_species
            self.objective_vector = self.biomass
            for specie in storage_species:
                self.objective_vector[self.species.index(specie)] = 0.0
        else:
            self.objective_vector = self.biomass

        # if no Enzyme Capacity Constraints present set these matrices to zero
        if HC is None or HE is None:
            self.HC = np.array([[]])
            self.HE = np.array([[]])
        else:
            self.HC = HC
            self.HE = HE

        # if no Biomasss Composition Constraints are present set the H_B matrix to zero.
        # We handle both cases if the biomass was given by a list or a dict
        if HB is None:
            if biomass_percentage and isinstance(biomass, dict):
                self.HB = np.zeros((len(biomass_percentage), len(species)))
                for row, specie in enumerate(biomass_percentage.iterkeys()):
                    for coloumn in range(self.numbers['proteins']):
                        self.HB[row, coloumn - self.numbers['proteins']] = biomass[species[coloumn - self.numbers['proteins']]] * \
                                                                           biomass_percentage[specie]
                        if specie == species[coloumn - self.numbers['proteins']]:
                            self.HB[row, coloumn - self.numbers['proteins']] = (biomass_percentage[specie] - 1) * biomass[specie]
            elif biomass_percentage and isinstance(biomass, list):
                biomass_dict = {spec: biomass.index(self.species[specie]) for specie in self.species}
                self.HB = np.zeros((len(biomass_percentage), len(species)))
                for row, specie in enumerate(biomass_percentage.iterkeys()):
                    for coloumn in range(self.numbers['proteins']):
                        self.HB[row, coloumn - self.numbers['proteins']] = biomass_dict[species[coloumn - self.numbers['proteins']]] * \
                                                                           biomass_percentage[specie]
                        if specie == species[coloumn - self.numbers['proteins']]:
                            self.HB[row, coloumn - self.numbers['proteins']] = (biomass_percentage[specie] - 1) * biomass_dict[specie]
            else:
                self.HB = np.array([[]])
        else:
            self.HB = np.atleast_2d(HB)

        # if no Maintenance Reactions are present H_M matrix is empty
        # if HM is not given directly, try to create it from maintenance_percentage dict
        if HM is None:
            if isinstance(maintenance_percentage, dict):
                HM = np.zeros((len(maintenance_percentage), len(reactions)))
                for row, reac in enumerate(maintenance_percentage.iterkeys()):
                    HM[row, reactions.index(reac)] = 1.0 / maintenance_percentage[reac]
                self.HM = HM
            else:
                self.HM = np.array([[]])
        else:
            self.HM = HM

        # check for additional dynamics dz/dt = R z + S v + q
        if R is None:
            self.R = np.zeros((len(species), len(species)))
        else:
            self.R = R
        if q is None:
            self.q = np.zeros(len(species))
        else:
            self.q = q

        # prepare the submatrices of the stoichiometric matrix for easier access in the scaling process
        Sy = self.stoich[0:self.numbers['ext_species'], :]
        Syx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
              0:self.numbers['exch_reactions']]
        Sxx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
              self.numbers['exch_reactions']: self.numbers['exch_reactions'] + self.numbers['metab_reactions']]
        if self.numbers['protein_reactions'] > 0:
            Spx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
                  -self.numbers['protein_reactions']:]
        else:
            Spx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
                  self.numbers['exch_reactions'] + self.numbers['metab_reactions']:len(reactions)]
        Sp = self.stoich[-self.numbers['proteins']:, :]
        self.Sx = np.hstack((Syx, Sxx, (1.0 / scale) * Spx))

        # Scaling takes place here via the self.scale attribute
        self.scaled_S = self.stoich.copy()
        self.scaled_S[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'], :] = self.Sx
        self.scaled_HC = np.hstack((scale * self.HC[:, :self.numbers['exch_reactions'] + self.numbers['metab_reactions']],
                                    self.HC[:, self.numbers['exch_reactions'] + self.numbers['metab_reactions']: len(self.reactions)]))
        if self.HM.any():
            self.scaled_HM = np.hstack((scale * self.HM[:, :self.numbers['exch_reactions'] + self.numbers['metab_reactions']],
                                        self.HM[:, self.numbers['exch_reactions'] + self.numbers['metab_reactions']: len(self.reactions)]))
        else:
            self.scaled_HM = np.array([[]])
        # check whether information on reversibility of the fluxes is present. If no data are given, assume only the exchange reaction
        # are invertible, while metabolic and protein producing reactions are irreversible.
        if reversible_fluxes is not None:
            self.reversible = reversible_fluxes
        else:
            # We assume that if no reversibility is given, that the exchange can happen in both directions while all others are force to be
            # positive
            self.reversible = self.reactions[:self.numbers['exch_reactions']]

        # self.secretion = {}
        self.uptake = {}
        for number_reac, reac in enumerate(self.reactions[:self.numbers['exch_reactions']]):
            if reac in self.reversible:
                # self.uptake[reac] = []
                for number_specie, specie in enumerate(self.species[:self.numbers['ext_species']]):
                    if self.stoich[number_specie, number_reac] > 0:
                        if reac not in self.uptake:
                            self.uptake[reac] = []
                        self.uptake[reac].append([specie, 1])
                    elif self.stoich[number_specie, number_reac] < 0:
                        if reac not in self.uptake:
                            self.uptake[reac] = []
                        self.uptake[reac].append([specie, -1])
            else:
                if any(n < 0 for n in self.stoich[:, number_reac]):
                    # self.uptake[reac] = []
                    for number_specie, specie in enumerate(self.species[:self.numbers['ext_species']]):
                        if self.stoich[number_specie, number_reac] < 0:
                            if reac not in self.uptake:
                                self.uptake[reac] = []
                            self.uptake[reac].append([specie, -1])

        # if initial conditions are given, save these for the environment and the initial biomass components
        if ics is not None:
            try:
                self.external_conditions = []
                for specie in self.species[:self.numbers['ext_species']]:
                    self.external_conditions.append(ics[specie])
            except KeyError:
                print 'not all external species have an initial value'
                self.external_conditions = None
            try:
                self.initial_biomass = []
                for specie in self.species[-self.numbers['proteins']:]:
                    self.initial_biomass.append(ics[specie])
            except KeyError:
                print 'not all biomass species have an initial value'
                self.initial_biomass = None
        else:
            self.external_conditions = None
            self.initial_biomass = None
        if initial_fluxes is None:
            self.initial_fluxes = []
        else:
            self.initial_fluxes = initial_fluxes
        self.delete_metabolites = delete_metabolites
        if delete_metabolites:
            self.HB = np.hstack(( self.HB[:,:self.numbers['ext_species']], self.HB[:, -self.numbers['proteins']:]))
            self.HE = np.hstack((self.HE[:, :self.numbers['ext_species']], self.HE[:, -self.numbers['proteins']:]))
            self.R = np.vstack((self.R[:self.numbers['ext_species'], :], self.R[-self.numbers['proteins']:, :]))
            self.R = np.hstack((self.R[:, :self.numbers['ext_species']], self.R[:, -self.numbers['proteins']:]))
            # self.scaled_HC
            # self.scaled_HM
            self.scaled_S = np.vstack(( Sy, Sp))
            self.stoich = np.vstack((Sy, Sp))
            # self.HC
            self.q = np.concatenate((self.q[:self.numbers['ext_species']], self.q[-self.numbers['proteins']:]))
            self.biomass = np.concatenate((self.biomass[:self.numbers['ext_species']], self.biomass[-self.numbers['proteins']:]))
            self.objective_vector = np.concatenate((self.objective_vector[:self.numbers['ext_species']],
                                                    self.objective_vector[-self.numbers['proteins']:]))
            self.species = self.species[:self.numbers['ext_species']] + self.species[-self.numbers['proteins']:]
            self.numbers['metab_species']=0
        # If the fluxes are supposed to be reduced, we implement a singular value decomposition of the stoichiometric matrix.
        # This reduces the amount of reactions to rank(S). We usually do not recommend using the reduction as the sparsity of the problem
        # gets lost.
        # if reduce_fluxes:
        #     U, sv, V = linalg.svd(Sx)
        #     maxabs = np.max(sv)
        #     maxdim = max(Sx.shape)
        #     tol = maxabs * maxdim * np.MachAr().eps
        #     rankS = np.sum(sv > tol)
        #     self.U = V.T[:, rankS:]
        #     self.numbers['reduced_reactions'] = self.U.shape[1]
        #     self.numbers['reduced_species'] = self.numbers['ext_species'] + self.numbers['proteins']
        #     red_S_y = np.hstack((Syy, np.zeros((Syy.shape[0], len(self.reactions) - Syy.shape[1]))))
        #     red_S_p = np.hstack((np.zeros((Spp.shape[0], len(self.reactions) - Spp.shape[1])), Spp))
        #     self.red_S = np.dot(np.vstack((red_S_y, red_S_p)), self.U)
        #     self.red_HC = np.dot(np.hstack((scale * self.HC[:, 0:self.numbers['exch_reactions'] + self.numbers['metab_reactions']],
        #                                     self.HC[:, self.numbers['exch_reactions'] + self.numbers['metab_reactions']:])), self.U)
        #     self.red_HE = np.hstack((self.HE[:, 0:self.numbers['ext_species']], self.HE[:, -self.numbers['proteins']:]))
        #     self.red_HB = np.hstack((self.HB[:, 0:self.numbers['ext_species']], self.HB[:, -self.numbers['proteins']:]))
        #     if self.HM.any():
        #         self.red_HM = np.dot(np.hstack((scale * self.HM[:, 0:self.numbers['exch_reactions'] + self.numbers['metab_reactions']],
        #                                         self.HM[:, self.numbers['exch_reactions'] + self.numbers['metab_reactions']:])), self.U)
        #     else:
        #         self.red_HM = np.array([[]])
        #     if not R is None:
        #         Ryy = np.hstack((R[: self.numbers['ext_species'], :self.numbers['ext_species']],
        #                          np.zeros((self.numbers['ext_species'], R.shape[1] - self.numbers['ext_species']))))
        #         Rpp = np.hstack((np.zeros((self.numbers['proteins'], R.shape[1] - self.numbers['proteins'])),
        #                          R[-self.numbers['proteins']:, -self.numbers['proteins']:]))
        #         self.red_R = np.dot(np.vstack((Ryy, Rpp)), self.U)
        #     else:
        #         self.red_R = np.zeros(( self.numbers['ext_species'] + self.numbers['proteins'], self.U.shape[1] ))
        #
        #     if not q is None:
        #         self.red_q = np.concatenate((q[:self.numbers['ext_species']], q[-self.numbers['proteins']:]))
        #     else:
        #         self.red_q = np.zeros(self.numbers['ext_species'] + self.numbers['proteins'])
        #
        #     self.red_biomass = np.hstack((self.biomass[0:self.numbers['ext_species']], self.biomass[-self.numbers['proteins']:]))
        #     self.red_objective_vector = np.hstack((
        #         self.objective_vector[0:self.numbers['ext_species']], self.objective_vector[-self.numbers['proteins']:]))

    def addDynamicsFromDict(self, dic):
        """
        add dynamics on states via a dictionary. Only capable to change self.R and self.q.
        E.g. consider states x,y,z. Then dx/dt = -5*x + 3 y + 10 translates to the dictionary {'x':'-5*x + 3 y + 10'}.

        :param dic: dictionary containing the new dynamics
        :return:
        """
        for key in dic.iterkeys():
            row = self.species.index(key)
            expr = dic[key].replace(" ", "")
            expr = expr.replace("*", "")
            expr = expr.replace("+-", "-")
            expr = expr.replace("-+", "-")
            expr = expr.replace("-", "+-")
            if expr[0] == '+':
                expr = expr[1:]
            while '+' in expr:
                while True:
                    next_plus = expr.index('+')
                    if next_plus > 0:
                        subexpr = expr[:next_plus]
                        expr = expr[next_plus + 1:]
                        break
                    else:
                        expr = expr[1:]
                if any(ext in subexpr for ext in self.species):
                    for spec in self.species:
                        if spec in subexpr:
                            index = subexpr.index(spec)
                            coloumn = self.species.index(spec)
                            number = float(subexpr[:index])
                            self.R[row, coloumn] += number
                elif is_number(subexpr):
                    self.q[row] += float(subexpr)
            if any(ext in expr for ext in self.species):
                for spec in self.species:
                    if spec in expr:
                        index = expr.index(spec)
                        coloumn = self.species.index(spec)
                        number = float(expr[:index])
                        self.R[row, coloumn] += number
            elif is_number(expr):
                self.q[row] += float(expr)

    def addLinearDynamics(self, R=None, q=None):
        """
        add linear dynamics in the form
        \dot{x} = S v + R x + q
        mostly used for external dynamics, but can be used to model protein degeneration
        Optional parameters:
        - R         ndarray, representing additional species dependent system dynamics
        - q         ndarray or list. Time and state independent dynamics
        """
        if R is not None:
            if self.delete_metabolites and R.shape[0]>self.numbers['ext_species']+self.numbers['proteins']:
                R_temp = np.vstack((R[:self.numbers['ext_species'], :], R[-self.numbers['proteins']:,:]))
                R_temp = np.hstack((R_temp[:,:self.numbers['ext_species']], R_temp[:,-self.numbers['proteins']: ]))
                self.R = self.R + R_temp
            else:
                self.R = self.R + np.atleast_2d(R)

        if q is not None:
            if self.delete_metabolites and len(q) > len(self.q):
                self.q = self.q + np.concatenate((q[:self.numbers['ext_species']], q[-self.numbers['proteins']:]))
            else:
                self.q = self.q + q
            # if self.reduced:
            #     if R is not None:
            #         Ryy = np.hstack((R[: self.numbers['ext_species'], :self.numbers['ext_species']],
            #                          np.zeros((self.numbers['ext_species'], R.shape[1] - self.numbers['ext_species']))))
            #         Rpp = np.hstack((np.zeros((self.numbers['proteins'], R.shape[1] - self.numbers['proteins'])),
            #                          R[-self.numbers['proteins']:, -self.numbers['proteins']:]))
            #         self.red_R = self.red_R + np.dot(np.vstack((Ryy, Rpp)), self.U)
            #     if q is not None:
            #         self.red_q = self.q + np.concatenate((q[:self.numbers['ext_species']], q[-self.numbers['proteins']:]))

    def addReaction(self, reaction_name, substrates=None, products=None, reversible=False, catalyzed=None, maintenance_percentage=None):
        """
        Method to add a reaction to the model. Reaction must have at least 1 substrate or a product.
        :param reaction_name: string to name the reaction; must be unique
        :param substrates:    list   Left side of reaction given in form [[sub1, stoich1], [sub2, stoich2]]
        :param products:      ride side of reaction given in form [[prod1, stoich1], [prod2, stoich2]]
        :param reversible:    Boolean, defines whether reaction is reversible
        :param catalyzed:     list. Defines which enzyme catalyzes this reaction [enzyme_name, [kcat_forward, kcat_backward]]
        :param maintenance_percentage: float; used to construct HM matrix
        :return: None
        """
        if self.delete_metabolites:
            raise AttributeError('Metabolites are deleted from model. Please add reactions before deleting the metabolites.')
        if substrates is None and products is None:
            raise ValueError('Both substrates and products are empty.')
        if reaction_name in self.reactions:
            raise ValueError('Reaction name already in use.')
        if catalyzed is not None:
            if catalyzed[0] not in self.species:
                raise ValueError('Catalyzing species unknown')
        found_protein = False
        found_external = False
        # First identify reaction type
        if substrates is not None:
            for sub in substrates:
                if len(sub) != 2:
                    raise ValueError('Format of substrates is wrong. Use substrates = [[sub1, stoich1], [sub2, stoich2]]')
                else:
                    if sub[0] in self.species[:self.numbers['ext_species']]:
                        found_external = True
                    elif sub[0] in self.species[-self.numbers['proteins']:]:
                        found_protein = True
                    elif sub[0] not in self.species:
                        raise ValueError('Species ' + sub[0] + ' not known. Please add via addSpecies()')
        if products is not None:
            for prod in products:
                if len(prod) != 2:
                    raise ValueError('Format of products is wrong. Use products = [[prod1, stoich1], [prod2, stoich2]]')
                else:
                    if prod[0] in self.species[:self.numbers['ext_species']]:
                        found_external = True
                    elif prod[0] in self.species[-self.numbers['proteins']:]:
                        found_protein = True
                    elif prod[0] not in self.species:
                        raise ValueError('Species ' + prod[0] + ' not known. Please add via addSpecies()')
        if reversible:
            self.reversible.append(reaction_name)
        # Change system matrices depending on reaction type
        if found_protein:
            self.reactions.append(reaction_name)
            self.numbers['protein_reactions'] += 1
            if self.stoich.any():
                self.stoich = np.hstack((self.stoich, np.zeros((self.stoich.shape[0], 1))))
            else:
                self.stoich = np.zeros((len(self.reactions), 1))
            if self.HC.any():
                self.HC = np.hstack((self.HC, np.zeros((self.HC.shape[0], 1))))
            if self.HM.any():
                self.HM = np.hstack((self.HM, np.zeros((self.HM.shape[0], 1))))
        elif found_external:
            self.reactions.insert(0, reaction_name)
            self.numbers['exch_reactions'] += 1
            if self.stoich.any():
                self.stoich = np.hstack((np.zeros((self.stoich.shape[0], 1)), self.stoich))
            else:
                self.stoich = np.zeros((len(self.reactions), 1))
            if self.HC.any():
                self.HC = np.hstack((np.zeros((self.HC.shape[0], 1)), self.HC))
            if self.HM.any():
                self.HM = np.hstack((np.zeros((self.HM.shape[0], 1)), self.HM))
        else:
            self.reactions.insert(self.numbers['exch_reactions'], reaction_name)
            self.numbers['metab_reactions'] += 1
            if self.stoich.any():
                self.stoich = np.hstack((self.stoich[:, :self.numbers['exch_reactions']], np.zeros((self.stoich.shape[0], 1)),
                                         self.stoich[:, self.numbers['exch_reactions']:]))
            else:
                self.stoich = np.zeros((len(self.reactions), 1))
            if self.HC.any():
                self.HC = np.hstack((self.HC[:, :self.numbers['exch_reactions']], np.zeros((self.HC.shape[0], 1)),
                                     self.HC[:, self.numbers['exch_reactions']:]))
            if self.HM.any():
                self.HC = np.hstack((self.HM[:, :self.numbers['exch_reactions']], np.zeros((self.HM.shape[0], 1)),
                                     self.HM[:, self.numbers['exch_reactions']:]))

        # add new entries to stoichiometric matrix
        reac_pos = self.reactions.index(reaction_name)
        if substrates is not None:
            for substr in substrates:
                sub_index = self.species.index(substr[0])
                self.stoich[sub_index, reac_pos] = -np.abs(substr[1])
        if products is not None:
            for prod in products:
                prod_index = self.species.index(prod[0])
                self.stoich[prod_index, reac_pos] = np.abs(prod[1])

        # scaling of stoichiometric matrix
        Syx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
              0:self.numbers['exch_reactions']]
        Sxx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
              self.numbers['exch_reactions']: self.numbers['exch_reactions'] + self.numbers['metab_reactions']]
        if self.numbers['protein_reactions'] > 0:
            Spx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
                  -self.numbers['protein_reactions']:]
        else:
            Spx = self.stoich[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'],
                  self.numbers['exch_reactions'] + self.numbers['metab_reactions']:len(reactions)]
        self.Sx = np.hstack((Syx, Sxx, (1.0 / self.scale) * Spx))
        self.scaled_S = self.stoich.copy()
        self.scaled_S[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'], :] = self.Sx

        # Catalyzing changes
        if catalyzed is not None:
            if self.HC.any():
                if len(catalyzed) != 2 or len(catalyzed[1]) != 2:
                    raise ValueError('Catalyzing species must be given in form catalyzed = [species_name, [kcat_forward, kcat_backward]]')
                try:
                    protein_at = self.species.index(catalyzed[0])
                except ValueError:
                    raise ValueError('Enzyme ' + catalyzed[0] + ' not found in species list')
                entry_number = np.count_nonzero(self.HE[:, protein_at])
                if entry_number > 0:  # enzyme in use
                    temp_HC = np.zeros((entry_number, self.HC.shape[1]))
                    temp_HE = np.zeros((entry_number, self.HE.shape[1]))
                    temp_row = 0
                    for row_number in range(self.HE.shape[0]):
                        if self.HE[row_number, protein_at] != 0:
                            self.HC[row_number, reac_pos] = np.abs(1 / catalyzed[1][0])
                            temp_HC[temp_row, :] = self.HC[row_number, :]
                            temp_HC[temp_row, reac_pos] = -np.abs(1 / catalyzed[1][1])
                            temp_HE[temp_row, :] = self.HE[row_number, :]
                            temp_row += 1
                    self.HC = np.vstack((self.HC, temp_HC))
                    self.HE = np.vstack((self.HE, temp_HE))
                else:  # enzyme not used yet
                    if not reversible:
                        temp = np.zeros((1, self.HC.shape[1]))
                        temp[0, reac_pos] = 1 / catalyzed[1][0]
                        self.HC = np.vstack((self.HC, temp))
                        temp = np.zeros((1, self.HE.shape[1]))
                        temp[0, protein_at] = 1
                        self.HE = np.vstack((self.HE, temp))
                    else:
                        temp = np.zeros((2, self.HC.shape[1]))
                        temp[0, reac_pos] = 1 / catalyzed[1][0]
                        temp[1, reac_pos] = -1 / catalyzed[1][1]
                        self.HC = np.vstack((self.HC, temp))
                        temp = np.zeros((2, self.HE.shape[1]))
                        temp[0, protein_at] = 1
                        temp[1, protein_at] = 1
                        self.HE = np.vstack((self.HE, temp))
            else:
                if not reversible:
                    self.HC = np.zeros((1, len(self.reactions)))
                    self.HC[0, reac_pos] = 1 / catalyzed[1][0]
                    self.HE = np.zeros((1, len(self.species)))
                    self.HE[0, protein_at] = 1
                else:
                    self.HC = np.zeros((2, len(self.reactions)))
                    self.HC[0, reac_pos] = 1 / catalyzed[1][0]
                    self.HC[1, reac_pos] = -1 / catalyzed[1][1]
                    self.HE = np.zeros((2, len(self.species)))
                    self.HE[0, protein_at] = 1
                    self.HE[1, protein_at] = 1
        self.scaled_HC = np.hstack((self.scale * self.HC[:, :self.numbers['exch_reactions'] + self.numbers['metab_reactions']],
                                    self.HC[:, self.numbers['exch_reactions'] + self.numbers['metab_reactions']: len(self.reactions)]))
        # Maintenance changes
        if maintenance_percentage is not None:
            if self.HM.any():
                temp_HM = np.zeros((1, len(self.reactions)))
                temp_HM[0, reac_pos] = 1 / maintenance_percentage
                self.HM = np.vstack((self.HM, temp_HM))
            else:
                self.HM = np.zeros((1, len(self.reactions)))
                self.HM[0, reac_pos] = 1 / maintenance_percentage
        self.scaled_HM = np.hstack((self.scale * self.HM[:, :self.numbers['exch_reactions'] + self.numbers['metab_reactions']],
                                    self.HM[:, self.numbers['exch_reactions'] + self.numbers['metab_reactions']: len(self.reactions)]))

    def addSpecies(self, species_name, species_type, weights=None):
        """

        :param species_name:    string  contains the new species name. Must be unique amongst already present names.
        :param species_type:    string  either 'extracellular', 'metabolite', 'enzyme', 'storage', 'quota'
        :param weights:         list    contains the weights for macromolecules [molecular_weight, objective_weight]
        :return: None
        """
        if self.delete_metabolites and species_type=='metabolite':
            raise ValueError('Metabolites are deleted from model. Species can only be added before the deletion.')
        if not isinstance(species_name, str):
            raise ValueError('First argument must a string')
        if species_name not in self.species:
            if species_type == "extracellular":
                self.species.insert(0, species_name)
                self.stoich = np.vstack((np.zeros((1, len(self.reactions))), self.stoich))
                self.scaled_S = np.vstack((np.zeros((1, len(self.reactions))), self.scaled_S))
                self.HE = np.hstack((np.zeros((self.HE.shape[0], 1)), self.HE))
                self.HB = np.hstack((np.zeros((self.HB.shape[0], 1)), self.HB))
                self.biomass = np.insert(self.biomass, 0, 0)
                self.objective_vector = np.insert(self.objective_vector, 0, 0)
                self.q = np.insert(self.q, 0, 0)
                self.R = np.vstack((np.zeros((1, len(self.species))), np.hstack((np.zeros((len(self.species) - 1, 1)), self.R))))
                self.numbers['ext_species'] += 1
                self.external_conditions.insert(0, 0)
            elif species_type == "metabolite":
                self.species.insert(self.numbers['ext_species'], species_name)
                self.stoich = np.vstack((self.stoich[:self.numbers['ext_species'], :], np.zeros((1, len(self.reactions))),
                                         self.stoich[self.numbers['ext_species']:, :]))
                self.scaled_S = np.vstack((self.scaled_S[:self.numbers['ext_species'], :], np.zeros((1, len(self.reactions))),
                                           self.scaled_S[self.numbers['ext_species']:, :]))
                self.HE = np.hstack((self.HE[:, :self.numbers['ext_species']], np.zeros((self.HE.shape[0], 1)),
                                     self.HE[:, self.numbers['ext_species']:]))
                self.HB = np.hstack((self.HB[:, :self.numbers['ext_species']], np.zeros((self.HB.shape[0], 1)),
                                     self.HB[:, self.numbers['ext_species']:]))
                self.biomass = np.insert(self.biomass, self.numbers['ext_species'], 0)
                self.objective_vector = np.insert(self.objective_vector, self.numbers['ext_species'], 0)
                self.q = np.insert(self.q, self.numbers['ext_species'], 0)
                R_temp = np.vstack((self.R[:self.numbers['ext_species'], :], np.zeros((1, len(self.species) - 1)),
                                    self.R[self.numbers['ext_species']:, :]))
                self.R = np.hstack(
                    (R_temp[:, :self.numbers['ext_species']], np.zeros((len(self.species), 1)), R_temp[:, self.numbers['ext_species']:]))
                self.numbers['metab_species'] += 1

            elif species_type in ['enzyme', 'storage', 'quota']:
                if weights is None or len(weights) != 2 or not isinstance(weights[0], (int, long, float, complex)) or not isinstance(
                        weights[1], (int, long, float, complex)):
                    raise ValueError('Macromolecules need an molecular weight and objective weight given as weights=[weight, obj_weight]')
                extpmetab = self.numbers['ext_species'] + self.numbers['metab_species']
                self.species.insert(extpmetab, species_name)
                self.stoich = np.vstack((self.stoich[:extpmetab, :], np.zeros((1, len(self.reactions))),
                                         self.stoich[extpmetab:, :]))
                self.scaled_S = np.vstack((self.scaled_S[:extpmetab, :], np.zeros((1, len(self.reactions))),
                                           self.scaled_S[extpmetab:, :]))
                self.HE = np.hstack((self.HE[:, :extpmetab], np.zeros((self.HE.shape[0], 1)),
                                     self.HE[:, extpmetab:]))
                self.HB = np.hstack((self.HB[:, :extpmetab], np.zeros((self.HB.shape[0], 1)),
                                     self.HB[:, extpmetab:]))
                self.biomass = np.insert(self.biomass, extpmetab, weights[0])
                self.objective_vector = np.insert(self.objective_vector, extpmetab, weights[1])
                self.q = np.insert(self.q, extpmetab, 0)
                R_temp = np.vstack((self.R[:extpmetab, :], np.zeros((1, len(self.species) - 1)),
                                    self.R[extpmetab:, :]))
                self.R = np.hstack((
                    R_temp[:, :extpmetab], np.zeros((len(self.species), 1)), R_temp[:, extpmetab:]))
                self.numbers['proteins'] += 1
                self.biomass.append(0.0)
            else:
                raise ValueError('Species Type not recognized')
        else:
            raise ValueError('Species name already in use')

    def addUncertainEnvironment(self, transport_reaction, decreasing):
        """
        This adds an artificial species a representing an uptake limit for the transport reaction. This reaction can have additional
        constraints from the original model. The rate of 'transport_reaction' will be limited depending on how decreasing is set. For
        decreasing=False a scenario is created in which

        :param transport_reaction:
        :param decreasing:
        :return:
        """
        if not isinstance(transport_reaction, str):
            raise AttributeError('First argument (transport_reaction) must given as string.')
        if transport_reaction not in self.reactions:
            raise ValueError('The reaction ' + transport_reaction + ' is unknown to the model. Please add it first via '
                                                                    'DefbaModel.addReaction()')
        if not isinstance(decreasing, bool):
            raise AttributeError('Second argument (decreasing) must be given as boolean value.')
        found = False
        counter = 0
        # temp_HC = np.zeros((1, len(self.reactions)))
        # if not decreasing:
        #     temp_HC[0, self.reactions.index(transport_reaction)] = 1
        # self.HC = np.vstack((self.HC, temp_HC))
        # self.scaled_HC = np.hstack((self.scale * self.HC[:, :self.numbers['exch_reactions'] + self.numbers['metab_reactions']],
        #                             self.HC[:, self.numbers['exch_reactions'] + self.numbers['metab_reactions']:]))
        # HC_pos = 'HC[' + str(self.HC.shape[0] - 1) + ',' + str(self.reactions.index(transport_reaction)) + ']'
        # temp_HE = np.zeros((1, len(self.species)))
        # if decreasing:
        #     temp_HE[0, position_species] = 1
        # # else:
        # #     temp_HE[0, position_species] = 0
        # self.HE = np.vstack((self.HE, temp_HE))
        while not found:
            if 'env_' + str(counter) not in self.parameter_list:
                found = True
                # self.parameter_list['env_' + str(counter)] = [[HC_pos], 0, [1], [1.0], 'environment']
                self.parameter_list['env_' + str(counter)] = [[], 0, [0], [0], 'environment']
            else:
                counter += 1
        if self.uncertain_environment is None:
            self.uncertain_environment = {}
        if decreasing:
            self.uncertain_environment['env_' + str(counter)] = [transport_reaction, 'decreasing']
        else:
            self.uncertain_environment['env_' + str(counter)] = [transport_reaction, 'increasing']
        return 'env_' + str(counter)

    def calcPredictionHorizon(self, biomass_comp, environment=None, solver='cplex'):
        """
        While the method calcPredictionHorizon() needs an initial p vector, containing the biomass products, this method uses another
        problem formulation and can predict a suitable horizon without needing this.

        :param solver: Defines the solver. Choose from 'cplex', 'gurobi', 'soplex', 'cvxopt'
        :param biomass_comp: The biomass composition for which the horizons should be calculated given as list or dict.
        :param environment: List with the current environmental setting.
        :return: pred, control
        """
        # optimization variable for linear solution is given as x=[ vlin, p0].
        # We calculate an upper bound on linear growth modes by ignoring the nutrient situation and the maintenance cost
        # objective min c^T x
        if biomass_comp is None:
            initial_biomass_amount = 1.0 * self.scale
        elif isinstance(biomass_comp, np.ndarray) or isinstance(biomass_comp, list):
            biomass_comp = np.array(biomass_comp)
        elif isinstance(biomass_comp, dict):
            temp = []
            for specie in self.species[-self.numbers['proteins']:]:
                temp.append(biomass_comp[specie])
            biomass_comp = np.array(temp)
        else:
            raise ValueError('biomass composition - argument not understood. Please use a list, ndarray, or dict.')
        if biomass_comp is not None:
            if len(biomass_comp) >= self.numbers['proteins']:
                initial_biomass_amount = self.scale * np.dot(self.biomass[-self.numbers['proteins']:],
                                                             biomass_comp[-self.numbers['proteins']:])
            else:
                raise ValueError(
                    'biomass composition - argument is too short. ' + str(len(biomass_comp)) + ' < ' + str(self.numbers['proteins']))
        if environment is None:
            environment = np.ones(self.numbers['ext_species'])
        elif isinstance(environment, np.ndarray) or isinstance(environment, list):
            environment = np.array(environment[:self.numbers['ext_species']])
        elif isinstance(environment, dict):
            temp = []
            for specie in self.species[:self.numbers['ext_species']:]:
                temp.append(environment[specie])
            environment = np.array(temp)
        else:
            raise ValueError('environment - argument not understood. Please use a list, ndarray, or dict.')
        if len(environment) < self.numbers['ext_species']:
            raise ValueError('environment - argument is too short. ' + str(len(environment)) + '<' + str(self.numbers['ext_species']))
        # To ensure the environment is correctly mapped to the reaction bounds, we construct these beforehand
        # If a reaction transports an external species to inside of the cell in positive flux direction, we must enforce the flux to be
        # negative
        VMAX = None
        VMIN = None
        for number, reaction in enumerate(self.reactions[:self.numbers['exch_reactions']]):
            if reaction in self.uptake:
                pos_constr = False
                neg_constr = False
                for elem in self.uptake[reaction]:
                    index = self.species.index(elem[0])
                    if environment[index] <= self.eps:
                        if not pos_constr:
                            if elem[1] == 1 and reaction in self.reversible:
                                if VMIN is None:
                                    VMIN = np.zeros((1, len(self.reactions)))
                                    VMIN[0, number] = -1
                                else:
                                    temp = np.zeros((1, len(self.reactions)))
                                    temp[0, number] = -1
                                    VMIN = np.vstack((VMIN, temp))
                        if not neg_constr:
                            if elem[1] == -1:
                                if VMAX is None:
                                    VMAX = np.zeros((1, len(self.reactions)))
                                    VMAX[0, number] = 1
                                else:
                                    temp = np.zeros((1, len(self.reactions)))
                                    temp[0, number] = 1
                                    VMAX = np.vstack((VMAX, temp))
        # Construct optimization problem to determine maximal achievable linear growth rate \lambda
        # Objective vector c. min c^T x with c = (0,0, w_o^T S_P, 0), x = (V, P)
        c = np.concatenate((np.zeros(self.numbers['metab_reactions'] + self.numbers['exch_reactions']),
                            np.dot(-self.objective_vector[-self.numbers['proteins']:],
                                   self.scaled_S[-self.numbers['proteins']:, -self.numbers['protein_reactions']:]),
                            np.zeros(self.numbers['proteins'])))
        # equality matrix Me * x == b. Used here for Steady-state constraint
        Sx = np.hstack((self.Sx, np.zeros((self.Sx.shape[0], self.numbers['proteins']))))
        sx = np.zeros(self.Sx.shape[0])
        # weightM enforces the biomass amount to equal to 'initial_biomass_amount'
        weightM = np.hstack((np.zeros((1, len(self.reactions))), np.atleast_2d(self.biomass[-self.numbers['proteins']:])))
        Me = np.vstack((Sx, weightM))
        me = np.hstack((sx, np.array([initial_biomass_amount])))
        # inequality matrix Mi * x <= mi. Used for reversibility of reactions, enzyme capacity constraint, biomass composition,
        # environmental constraints, and maintenance constraints
        reversible_matrix = np.zeros((len(self.reactions) - len(self.reversible), len(self.reactions)))
        counter = 0
        for reaction in self.reactions:
            if reaction not in self.reversible:
                reversible_matrix[counter, self.reactions.index(reaction)] = -1.0
                counter += 1
        Mi = np.hstack((reversible_matrix, np.zeros((reversible_matrix.shape[0], self.numbers['proteins']))))
        mi = np.zeros(reversible_matrix.shape[0])
        # proteins must larger than zero
        positivity = -np.eye(self.numbers['proteins'])
        Mi = np.vstack((Mi, np.hstack((np.zeros((self.numbers['proteins'], len(self.reactions))), positivity))))
        mi = np.hstack((mi, np.zeros(positivity.shape[0])))
        if self.HC.any():
            Mi = np.vstack((Mi,
                            np.hstack((self.scaled_HC, - self.HE[:, -self.numbers['proteins']:]))))
            mi = np.hstack((mi, np.zeros(self.HC.shape[0])))
        if self.HB.any():
            Mi = np.vstack((Mi,
                            np.hstack((np.zeros((self.HB.shape[0], len(self.reactions))), self.HB[:, -self.numbers['proteins']:]))))
            mi = np.hstack((mi, np.zeros(self.HB.shape[0])))
        if self.HM.any():
            Mi = np.vstack((Mi,
                            np.hstack((- self.scaled_HM, np.zeros((self.HM.shape[0], self.numbers['proteins']))))))
            mi = np.hstack((mi, np.array([-initial_biomass_amount for i in range(self.HM.shape[0])])))
        # add environmental constraints
        if VMAX is not None:
            Mi = np.vstack((Mi, np.hstack((VMAX, np.zeros((VMAX.shape[0], self.numbers['proteins']))))))
            mi = np.hstack((mi, np.zeros(VMAX.shape[0])))
        if VMIN is not None:
            Mi = np.vstack((Mi, np.hstack((VMIN, np.zeros((VMIN.shape[0], self.numbers['proteins']))))))
            mi = np.hstack((mi, np.zeros(VMIN.shape[0])))
        result, ob = linopt.staticLP(c, me, mi, Me, Mi, solver)
        ob = np.fabs(ob / self.scale)
        vlin = np.zeros(len(self.reactions))
        for number, flux in enumerate(result[:len(self.reactions)]):
            if number < self.numbers['exch_reactions'] + self.numbers['metab_reactions']:
                vlin[number] = flux
            else:
                vlin[number] = flux / self.scale
        if biomass_comp is None:
            p0 = result[len(self.reactions):]
        elif len(biomass_comp) == self.numbers['proteins']:
            p0 = biomass_comp * self.scale
        elif len(biomass_comp) > self.numbers['proteins']:
            p0 = biomass_comp[-self.numbers['proteins']:] * self.scale
        else:
            p0 = result[len(self.reactions):]
        # with the linear solution in place, we calculate a lower bound on exponential growth by enforcing balanced growth dP/dt = mu*P
        # experience shows, that a 'bad' solution usually is enough to get a nice bound. Hence, we suggest using the p0 just calculated to
        # determine the balanced growth rate mu.
        # exp growth  x = [ vexp, p0?, mu ]
        # min fi^T * x = 0 - mu
        # s.t D * x == di
        #     E * x <= ei
        mu_exp = 0
        init_bm_scalar = initial_biomass_amount / self.scale
        lambda_r = ob / init_bm_scalar
        if lambda_r <= 0:
            raise RuntimeError('Linear growth rate could not be calculated!')
            return 1, 1
        # Construct the second problem to determine the balanced growth rate
        # Objective is min fi^T x, fi=(0,-1) x = (V, mu)
        fi = np.concatenate((np.zeros(len(self.reactions)), [-1.00]))
        # D * x = di
        Sx = self.scaled_S[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'], :].copy()
        D = np.vstack((np.hstack((Sx, np.zeros((self.numbers['metab_species'], 1)))),
                       np.hstack((self.scaled_S[-self.numbers['proteins']:, :].copy(),
                                  np.transpose(np.atleast_2d([-i if i > 0 else 0 for i in p0]))))))
        di = np.zeros(D.shape[0])
        # E * x <= ei
        E = np.vstack((np.hstack((self.scaled_HC, np.zeros((self.HC.shape[0], 1)))),
                       np.hstack((reversible_matrix, np.zeros((reversible_matrix.shape[0], 1))))))
        ei = np.concatenate((np.dot(self.HE[:, -self.numbers['proteins']:], p0), np.zeros(counter)))
        if self.scaled_HM.any():
            E = np.vstack((E, np.hstack((- self.scaled_HM, np.zeros((self.HM.shape[0], 1))))))
            ei = np.hstack((ei, np.array([-initial_biomass_amount for i in range(self.HM.shape[0])])))
        if VMIN is not None:
            E = np.vstack((E, np.hstack((VMIN, np.zeros((VMIN.shape[0], 1))))))
            ei = np.hstack((ei, np.zeros(VMIN.shape[0])))
        if VMAX is not None:
            E = np.vstack((E, np.hstack((VMAX, np.zeros((VMAX.shape[0], 1))))))
            ei = np.hstack((ei, np.zeros(VMAX.shape[0])))
        result, mu = linopt.staticLP(fi, di, ei, D, E, solver)
        mu_exp = np.abs(mu)
        if mu_exp <= 0:
            # use relaxed problem
            D = np.hstack((self.scaled_S[-self.numbers['proteins']:, :].copy(),
                           np.transpose(np.atleast_2d([-i if i > 0 else 0 for i in p0]))))
            di = np.zeros(D.shape[0])
            E = np.vstack((E, np.hstack((Sx, np.zeros((self.numbers['metab_species'], 1))))))
            ei = np.hstack((ei, np.zeros(Sx.shape[0])))
            result, mu = linopt.staticLP(fi, di, ei, D, E, solver)
            mu_exp = np.abs(mu)
            if mu_exp <= 0:
                raise RuntimeError('Balanced growth rate can not be calculated!')
        if mu_exp > ob:
            raise RuntimeError('exponential solution is always achievable. Prediction horizon and iteration must be chosen by hand.')
        meet_func = lambda tp: 1 / 2.0 * lambda_r * tp ** 2 - 1.0 / mu_exp * np.exp(mu_exp * tp) + tp + 1.0 / mu_exp
        horizon_guess = 2 * np.log(lambda_r / mu_exp) / mu_exp
        zero = newton(meet_func, horizon_guess, maxiter=50000, tol=1.48e-6)
        # backscaling of p0
        # p0 = np.array(p0) / self.scale
        if mu_exp * init_bm_scalar - ob < 0:
            iteration_time = (-2.0 / mu_exp + 2 * init_bm_scalar / ob + zero)
        else:
            iteration_time = (zero / 2.0)
        if zero > 0:
            print 'rescaled linear growth rate ' + str(ob / init_bm_scalar)
            print 'balanced growth rate ' + str(mu_exp)
            print 'Prediction horizon is calculated with variable biomass composition as ' + str(zero)
            print 'Iteration time is calculated with fixed biomass composition as ' + str(iteration_time)
            return zero, iteration_time
        elif zero == 0:
            raise ValueError('Can not automatically choose prediction horizon')
        else:
            print 'exponential solution is always achievable. Prediction horizon can be arbitrarily chosen.'
            return 0, 0

    def deFBA(self, Tend, stepsize, external_conditions=None, RBA_init=False, initial_biomass_amount=1, initial_biomass=None,
              eps=10 ** (-8), solver='cplex', export=True, output_name='', midpoint=True):
        """
        Solve the Linear Program, described by the model with the chosen external_conditions.\
        The initial biomass composition is either taken from DeFBA.initial_biomass, are calculated by a Resource Balance Analysis (RBA).
        :param Tend:                    End-time for simulation.
        :param stepsize:                width of the time grid
        :param external_conditions:     list with initial nutrient amounts in the substrate. Can be given as list or dict
                                        if set to None initial conditions from DefbaModel object are used
        :param RBA_init:                boolean value whether RBA should be used to determine initial biomass P_0
        :param initial_biomass_amount:  float representing the amount of biomass (gram dryweight) at t=0 (only needed if RBA is used)
        :param initial_biomass:         list with initial values for all biomass products
        :param eps:                     float representing the precision used as tolerance for linear solver
        :param solver:                  string for choosing the solver. Available are cvxopt, gurobi, cplex, soplex
        :param export:                  boolean switch whether to export to file. standardly called 'control' and 'species'
        :param output_name:             if export is chosen, you can change the output name of the data to 'control_output_name' and
                                        'states_output_name'
        :param midpoint:                boolean. Define whether midpoint collocation or radau collation is used
        :return:                        linopt.CollocatedTrajectory or midopt.MidpointTrajectory
        """
        # check if external conditions are defined in method call
        if not output_name and self.name:
            output_name = self.name
        # check if external conditions are given or the ones from DefbaModel should be used
        if external_conditions is None:
            if self.external_conditions is not None:
                external_conditions = self.external_conditions
                print 'using initial values from SBML file'
            else:
                raise ValueError('No external conditions defined')
        elif isinstance(external_conditions, dict):
            try:
                new_ext = []
                for specie in self.species[: self.numbers['ext_species']]:
                    new_ext.append(external_conditions[specie])
                external_conditions = new_ext
            except KeyError:
                raise ValueError('Not all external species are defined. Missing initial condition for ' + specie)
        elif isinstance(external_conditions, list) or isinstance(external_conditions, np.ndarray):
            pass
        else:
            raise ValueError('No external conditions defined')
        if isinstance(initial_biomass, dict):
            new_biomass = []
            for name in self.species[-self.numbers['proteins']:]:
                new_biomass.append(initial_biomass[name])
            initial_biomass = np.array(new_biomass)
        solver_present = True
        exec ('if not globals.have_' + solver + ': solver_present=False')
        steps = int(np.ceil(Tend / float(stepsize)))
        if not solver_present:
            print 'The solver ' + solver + ' is not installed. Falling back to cvxopt'
            solver = 'cvxopt'
        # Set initial values. Starts automatically if no initial biomass given by user or DefbaModel object
        if initial_biomass is None:
            if RBA_init:
                initial_biomass, initial_fluxes, mu = self.RBA(initial_biomass_amount, ext_conditions=external_conditions, eps=eps,
                                                               solver=solver)
            elif self.initial_biomass is not None:
                initial_biomass = self.initial_biomass
            else:
                print 'No initial values given. Falling back for RBA initialization'
                initial_biomass, initial_fluxes, mu = self.RBA(initial_biomass_amount, ext_conditions=external_conditions, eps=eps,
                                                               solver=solver)

        print 'starting with full model and solver ' + solver
        initial_biomass = [self.scale[i] * x if not np.isscalar(self.scale) else self.scale * x for i, x in enumerate(initial_biomass)]
        if midpoint:
            optimizer_tool = midopt.MidOpt(self.R, self.scaled_S, np.concatenate(
                (external_conditions, np.zeros(self.numbers['metab_species']), np.array(initial_biomass))), self.q)
        else:
            optimizer_tool = linopt.LinOpt(self.R, self.scaled_S, np.concatenate(
                (external_conditions, np.zeros(self.numbers['metab_species']), np.array(initial_biomass))), self.q)
        # set the objective \int_0^Tend b^T P dt
        optimizer_tool.set_objective(-self.objective_vector, np.zeros(len(self.reactions)), 0, np.zeros(len(self.objective_vector)))
        # construct Enzyme Capacity Constraint + Biomass Composition Constraint + Positivity constraints + Maintenance
        Gx_1 = -np.eye(len(self.species))  # all species are bound to be positive -x \leq 0
        Gu_1 = np.zeros((len(self.species), len(self.reactions)))
        Gx_2 = np.zeros((len(self.reactions) - len(self.reversible), Gx_1.shape[1]))  # constrain only irreversible reactions
        Gu_2 = np.zeros((len(self.reactions) - len(self.reversible), len(self.reactions)))
        counter = 0
        for coloumn, reaction in enumerate(self.reactions):
            if reaction not in self.reversible:
                Gu_2[counter, self.reactions.index(reaction)] = -1
                counter += 1
        # check if constraints are in the model
        # enzyme capacity constraints
        if self.HE.any():
            Gx_HE = -self.HE
            Gu_HC = self.scaled_HC
            if midpoint:
                Gx = np.vstack((Gx_HE, Gx_2))
                Gu = np.vstack((Gu_HC, Gu_2))
                optimizer_tool.add_state_constraint(Gx_1, np.zeros(Gx_1.shape[0]))
            else:
                Gx = np.vstack((Gx_HE, Gx_1, Gx_2))
                Gu = np.vstack((Gu_HC, Gu_1, Gu_2))
        elif midpoint:
            Gx = np.vstack((Gx_2))
            Gu = np.vstack((Gu_2))
            optimizer_tool.add_state_constraint(Gx_1, np.zeros(Gx_1.shape[0]))
        else:
            Gx = np.vstack((Gx_1, Gx_2))
            Gu = np.vstack((Gu_1, Gu_2))
        # biomass composition constraints
        if self.HB.any():
            Gx_HB = self.HB
            Gx = np.vstack((Gx, Gx_HB))
            Gu_HB = np.zeros((self.HB.shape[0], len(self.reactions)))
            Gu = np.vstack((Gu, Gu_HB))
        # maintenance reaction constraints
        if self.HM.any():
            Gx_5 = np.hstack((np.zeros((self.HM.shape[0], self.numbers['ext_species'] + self.numbers['metab_species'])),
                              np.dot(np.eye(self.HM.shape[0]), np.atleast_2d(self.biomass[-self.numbers['proteins']:]))))
            Gx = np.vstack((Gx, Gx_5))
            Gu_5 = -self.scaled_HM
            Gu = np.vstack((Gu, Gu_5))
        # All constraints are constructed, s.t. the right-hand side is always zero
        Gk = np.zeros(Gx.shape[0])
        # add path constraints to linear program
        optimizer_tool.add_path_constraint(Gx, Gu, Gk)
        # add steady-state constraints as control constraints
        optimizer_tool.add_control_constraint(self.Sx, np.zeros(self.Sx.shape[0]))
        # set solver parameters
        optimizer_tool.set_solver(solver)
        optimizer_tool.set_parameters(timesteps=steps)
        optimizer_tool.set_parameters(finaltime=Tend)
        optimizer_tool.set_parameters(fixedterm=True)
        optimizer_tool.set_parameters(ScaleFlag=2)
        results = optimizer_tool.solve()
        times = [i * float(Tend / steps) for i in xrange(steps + 1)]
        # handle the export of results to a csv file.
        if export:
            export_states(times, results[1], self.species, self.scale, self.numbers['ext_species'] + self.numbers['metab_species'],
                          file_name=output_name + '_species')
            export_fluxes(times, results[1], self.reactions, self.scale, self.numbers['metab_reactions'] +
                          self.numbers['exch_reactions'], file_name=output_name + '_fluxes')
        self.results = [times, results]
        # results are given in linOpt.CollocatedTrajectory or midopt.MidpointTrajectory
        return [times, results]

    def deleteReactions(self, reactions_list):
        """
        Deletes the given list of reactions. Method does not check whether the deletions make sense.

        :param reactions_list:
        :return: None
        """
        if not isinstance(reactions_list, list):
            try:
                reactions_list = [reactions_list]
            except:
                raise AttributeError('reaction list not understood.')
        reactions_list = list(set(reactions_list))
        reaction_numbers = [self.reactions.index(reac) for reac in reactions_list]
        reaction_numbers.sort()
        for reac in reactions_list:
            try:
                self.reactions.remove(reac)
            except ValueError as e:
                print 'The reaction ' + reac + 'is not part of the model.'
            if reac in self.reversible:
                self.reversible.remove(reac)
            if reac in self.uptake:
                self.uptake.remove(reac)
        for index in reversed(reaction_numbers):
            if self.HC.any():
                self.HC = np.hstack((self.HC[:, :index], self.HC[:, index + 1:]))
                self.scaled_HC = np.hstack((self.scaled_HC[:, :index], self.scaled_HC[:, index + 1:]))
            if self.HM.any():
                self.HM = np.hstack((self.HM[:, :index], self.HM[:, index + 1:]))
                self.scaled_HM = np.hstack((self.scaled_HM[:, :index], self.scaled_HM[:, index + 1:]))
            if self.stoich.any():
                self.stoich = np.hstack((self.stoich[:, :index], self.stoich[:, index + 1:]))
                self.scaled_S = np.hstack((self.scaled_S[:, :index], self.scaled_S[:, index + 1:]))
            if self.Sx.any():
                self.Sx = np.hstack((self.Sx[:,:index], self.Sx[:,index+1:] ))
            if self.initial_fluxes:
                self.initial_fluxes = np.concatenate((self.initial_fluxes[:index], self.initial_fluxes[index + 1:]))
            exchange = self.numbers['exch_reactions']
            metab_reac = self.numbers['exch_reactions'] + self.numbers['metab_reactions']
            if index >= metab_reac:
                self.numbers['protein_reactions'] -= 1
            elif index >= exchange:
                self.numbers['metab_reactions'] -= 1
            else:
                self.numbers['exch_reactions'] -= 1
        return None

    def deleteSpecies(self, species_list):
        """
         Deletes the given list of species. Method does not check whether the deletions make sense.

        :param species_list:
        :return:
        """
        if self.delete_metabolites:
            raise ValueError('Metabolites deleted from model. This method is only available for the full model.')
        if not isinstance(species_list, list):
            try:
                species_list = [species_list]
            except:
                raise AttributeError('species list not understood.')
        species_list = list(set(species_list))
        species_numbers = [self.species.index(specie) for specie in species_list]
        species_numbers.sort()
        for species in species_list:
            try:
                self.species.remove(species)
            except ValueError as e:
                print 'The species ' + species + 'is not part of the model.'
            if species in self.storage_species:
                self.storage_species.remove(species)
            if species in self.enzyme_catalyzes:
                self.enzyme_catalyzes.pop(species, None)
        for index in reversed(species_numbers):
            self.R = np.vstack((self.R[:index, :], self.R[index + 1:, :]))
            self.R = np.hstack((self.R[:, :index], self.R[:, index + 1:]))
            self.q = np.concatenate((self.q[:index], self.q[index + 1:]))
            if self.HE.any():
                nonzeros = self.HE[:,index].nonzero()[0]
                nonzeros.sort()
                for new_index in reversed(nonzeros):
                    self.HE = np.vstack((self.HE[:new_index, :], self.HE[new_index+1:,:]))
                    self.HC = np.vstack((self.HC[:new_index, :], self.HC[new_index+1:,:]))
                    self.scaled_HC = np.vstack((self.scaled_HC[:new_index, :], self.scaled_HC[new_index + 1:, :]))
                self.HE = np.hstack((self.HE[:, :index], self.HE[:, index + 1:]))
            if self.HB.any():
                if any(n < 0 for n in self.HB[:,index]):
                    seti = []
                    for ind, number in enumerate(self.HB[:,index]):
                        if number < 0:
                            seti.append(ind)
                    seti.sort()
                    for new_index in reversed(seti):
                        self.HB = np.vstack((self.HB[:new_index, :], self.HB[new_index+1:, :]))
                self.HB = np.hstack((self.HB[:, :index], self.HB[:, index + 1:]))
            if self.stoich.any():
                self.stoich = np.vstack((self.stoich[:index, :], self.stoich[index + 1:, :]))
                self.scaled_S = np.vstack((self.scaled_S[:index, :], self.scaled_S[index + 1:, :]))
            if self.Sx.any() and self.numbers['ext_species']<=index<self.numbers['ext_species']+self.numbers['proteins']:
                self.Sx = np.vstack((self.Sx[:index, :], self.Sx[index + 1:, :]))
            if self.biomass.any():
                self.biomass = np.concatenate((self.biomass[:index], self.biomass[index + 1:]))
            if self.objective_vector.any():
                self.objective_vector = np.concatenate((self.objective_vector[:index], self.objective_vector[index + 1:]))
            external = self.numbers['ext_species']
            metab_species = self.numbers['ext_species'] + self.numbers['metab_species']
            if np.array(self.initial_biomass).any() and index>=metab_species:
                self.initial_biomass = np.concatenate(
                    (self.initial_biomass[:index - metab_species], self.initial_biomass[index - metab_species + 1:]))
            if index >= metab_species:
                self.numbers['proteins'] -= 1
            elif index >= external:
                self.numbers['metab_species'] -= 1
            else:
                self.numbers['ext_species'] -= 1

    def deleteMetabolites(self):
        self.delete_metabolites = True
        self.HB = np.hstack((self.HB[:, :self.numbers['ext_species']], self.HB[:, -self.numbers['proteins']:]))
        self.HE = np.hstack((self.HE[:, :self.numbers['ext_species']], self.HE[:, -self.numbers['proteins']:]))
        self.R = np.vstack((self.R[:self.numbers['ext_species'], :], self.R[-self.numbers['proteins']:, :]))
        self.R = np.hstack((self.R[:, :self.numbers['ext_species']], self.R[:, -self.numbers['proteins']:]))
        # self.scaled_HC
        # self.scaled_HM
        Sp = self.stoich[-self.numbers['proteins']:, :]
        Sy = self.stoich[0:self.numbers['ext_species'], :]
        self.scaled_S = np.vstack((Sy, Sp))
        self.stoich = np.vstack((Sy, Sp))
        # self.HC
        self.q = np.concatenate((self.q[:self.numbers['ext_species']], self.q[-self.numbers['proteins']:]))
        self.biomass = np.concatenate((self.biomass[:self.numbers['ext_species']], self.biomass[-self.numbers['proteins']:]))
        self.objective_vector = np.concatenate((self.objective_vector[:self.numbers['ext_species']],
                                                self.objective_vector[-self.numbers['proteins']:]))
        self.species = self.species[:self.numbers['ext_species']] + self.species[-self.numbers['proteins']:]
        self.numbers['metab_species'] = 0

    def exportToSbml(self, documentname=None):
        """
        Uses the sbmlexport package to write the deFBA-model to an .xml file in SBML format using RAM annotations.

        Optional arguments:
        - documentname      string containing the desired file name for .xml file.
        """
        if self.delete_metabolites:
            raise ValueError('Metabolites are deleted from model. This function is only for the full model available.')
        if documentname is None:
            sbmlexport.writeSBML(self, self.name)
        else:
            sbmlexport.writeSBML(self, documentname)
        print('export successful')

    def plotResults(self, species, reactions=None, results=None, plot_biomass=False):
        """
        Plot the time courses using matplotlib
        :param species:      list with species name and/or numbers of species in self.species
        :param reactions:    list with reactions name and/or numbers of species in self.reactions
        :param results:      set with (times, species_trajectories, reaction_trajectories)
        :param plot_biomass: boolean. Plots the percentage of biomass.
        :return:             empty
        """
        if not globals.have_matplotlib:
            print 'Matplotlib is not installed. Plotting functions are not available.'
            return None
        if results is not None:
            pass
        elif self.results is not None:
            results = self.results
        else:
            print 'No simulation results yet'
            return None

        if isinstance(results[0], list) or isinstance(results[0], np.ndarray):
            time = results[0]
        else:
            raise AttributeError('the first entry in results must be a list with the time discretization.')
        colloc_results = None

        try:
            colloc_results = results[1][1]
        except IndexError:
            pass
        if colloc_results.__class__.__name__ == 'CollocatedTrajectory' or colloc_results.__class__.__name__ =='MidpointTrajectory':
            species_trajectories = [results[1][1].get_state(i) for i in time]
            flux_trajectories = [results[1][1].get_control(i) for i in time]
        elif len(results) == 2:
            if len(results[1][0]) == len(self.species):
                species_trajectories = results[1]
            elif len(results[1][0]) == len(self.reactions):
                flux_trajectories = results[1]
            else:
                raise AttributeError('results format not recognized')
        elif len(results) == 3:
            if len(results[1][0]) == len(self.species):
                species_trajectories = results[1]
            else:
                raise AttributeError('Results format not recognized')
            if len(results[2][0]) == len(self.reactions):
                flux_trajectories = results[2]
            else:
                raise AttributeError('Results format not recognized')
        else:
            raise AttributeError('Results format not recognized')
        plot_species_names = []
        plot_species = None
        if isinstance(species, str):
            species = [species]
        if species is not None:
            for number, specie in enumerate(species):
                if isinstance(specie, str):
                    index = self.species.index(specie)
                    plot_species_names.append(specie)
                elif isinstance(specie, int):
                    index = specie
                    plot_species_names.append(self.species[index])
                else:
                    raise TypeError(
                        'Species type not understood. Please use either the species name or their entry index in the self.species'
                        ' list')
                if plot_species is None:
                    plot_species = [[]]
                else:
                    plot_species.append([])
                min_size = np.min((len(time), np.array(species_trajectories).shape[0]))
                for i in xrange(min_size):
                    plot_species[number].append(species_trajectories[i][index])
            number_of_plots = len(species)
            plt.figure(1)
            plt.title('species')
            column_number = int(np.ceil(number_of_plots / 5.0))
            if number_of_plots < 5:
                row_number = number_of_plots
            else:
                row_number = 5
            for plotnumber in range(number_of_plots):
                plt.subplot(row_number, column_number, plotnumber + 1)
                if plotnumber == 0:
                    plt.title('species')
                plt.xlabel('time')
                plt.ylabel('Amount')
                plt.plot(time[0:min_size], plot_species[plotnumber], label=plot_species_names[plotnumber])
                plt.legend(loc="upper right")
        # plt.show()
        # Species plot ready. Check if some fluxes should also be plotted
        if reactions is not None:
            time = time[:-1]
            plot_reactions_names = []
            plot_reactions = None
            if isinstance(reactions, str):
                reactions = [reactions]
            for number, reac in enumerate(reactions):
                if isinstance(reac, str):
                    index = self.reactions.index(reac)
                    plot_reactions_names.append(reac)
                elif isinstance(reac, int):
                    index = reac
                    plot_reactions_names.append(self.reactions[index])
                else:
                    raise TypeError(
                        'Species type not understood. Please use either the species name or their entry index in the self.species'
                        ' list')
                if plot_reactions is None:
                    plot_reactions = [[]]
                else:
                    plot_reactions.append([])
                min_size = np.min((len(time), np.array(flux_trajectories).shape[0]))
                for i in xrange(min_size):
                    plot_reactions[number].append(flux_trajectories[i][index])
            number_of_plots = len(reactions)
            plt.figure(2)
            plt.title('reactions')
            row_number = 5
            column_number = int(np.ceil(number_of_plots / 5.0))
            if number_of_plots < 5:
                row_number = number_of_plots
            for plotnumber in range(number_of_plots):
                plt.subplot(row_number, column_number, plotnumber + 1)
                if plotnumber == 0:
                    plt.title('reactions')
                plt.xlabel('time')
                plt.ylabel('mol/h')
                plt.plot(time[0:min_size], plot_reactions[plotnumber], label=plot_reactions_names[plotnumber])
                plt.legend(loc="upper right")
        if plot_biomass is True:
            total_biomass = []
            biomass_name = []
            biomass_percent = [[]]
            min_size = np.min((len(time), np.array(species_trajectories).shape[0]))
            for position in range(min_size):
                total_biomass.append(np.dot(self.biomass, species_trajectories[position]))
            for entry, specie in enumerate(self.species[-self.numbers['proteins']:]):
                biomass_name.append(specie)
                for position in range(min_size):
                    biomass_percent[entry].append(self.biomass[entry - self.numbers['proteins']] * 100 * species_trajectories[position][
                        entry - self.numbers['proteins']] / total_biomass[position])
                biomass_percent.append([])
            plt.figure(3)
            plt.title('biomass percentage')
            for i in range(self.numbers['proteins']):
                plt.plot(time[0:min_size], biomass_percent[i], label=biomass_name[i])
            plt.legend(loc="upper left")
        plt.show()
        return None

    def RBA(self, initial_biomass_amount, ext_conditions, eps=1e-8, solver='cplex', mu=1.0):
        """
        Calculate the optimal enzyme composition for fixed starting biomass for given external conditions.
        We use self designed cvxopt and cplex problems instead of LinOpt.staticLP, because we can easily manipulate gurobi/cplex models,
        s.t. these models do not need to be recreated with every new iteration

        Required arguments:
        - initial_biomass_amount            float. Amount of biomass at t = 0.
        - ext_conditions                    list / np.array . External conditions at t=0.

        Optional Arguments:
        - eps                               float. Accuracy of solver in terms of convergence for the bisection method.
        - solver                            string. Accepts either 'cvxopt', 'gurobi', 'cplex', 'soplex'
        - mu                                float. Initial guess for achievable growth rate.

        Return?
        """
        if isinstance(ext_conditions, dict):
            new_ext_cond = []
            for spec in self.species[:self.numbers['ext_species']]:
                new_ext_cond.append(ext_conditions[spec])
            ext_conditions = new_ext_cond
        if solver == 'gurobi':
            if not globals.have_gurobi:
                print 'Solver Gurobi not available. Fallback to cvxopt'
                return self.RBA(initial_biomass_amount, ext_conditions, eps=eps, solver='cvxopt', mu=mu)
            model = grb.Model('RBA')
            fluxes = []
            proteins = []
            # this utilizes a bisection algorithm
            # first set up the model with chosen mu

            # set up all decision variables
            # depending on reversibility of the fluxes set the lower bounds
            for flux in self.reactions:
                if flux in self.reversible:
                    fluxes.append(model.addVar(lb=-grb.GRB.INFINITY, vtype=grb.GRB.CONTINUOUS, name=flux))
                else:
                    fluxes.append(model.addVar(lb=0, vtype=grb.GRB.CONTINUOUS, name=flux))
            # all proteins must be >= 0
            for protein in self.species[-self.numbers['proteins']:]:
                proteins.append(model.addVar(lb=0, vtype=grb.GRB.CONTINUOUS, name=protein))
            model.update()

            # set qss constraints \dot{x}=Sx * V = 0
            Sx = csr_matrix(self.Sx)
            for i in range(Sx.shape[0]):
                start1 = Sx.indptr[i]
                end1 = Sx.indptr[i + 1]
                variables = [fluxes[j] for j in Sx.indices[start1:end1]]
                coeff = Sx.data[start1:end1]
                model.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.EQUAL, rhs=0.0, name='Steady-state_' + str(i))

            # add Enzyme Capacity Constraints HC * V <= HE * p
            HC = csr_matrix(self.scaled_HC)
            HE = csr_matrix(self.HE[:, -self.numbers['proteins']:])
            for i in range(HC.shape[0]):
                start1 = HC.indptr[i]
                end1 = HC.indptr[i + 1]
                variables = [fluxes[j] for j in HC.indices[start1:end1]]
                coeff = HC.data[start1:end1]
                start2 = HE.indptr[i]
                end2 = HE.indptr[i + 1]
                variables2 = [proteins[j] for j in HE.indices[start2:end2]]
                coeff2 = HE.data[start2:end2]
                model.addConstr(lhs=grb.LinExpr(coeff, variables) - grb.LinExpr(coeff2, variables2), sense=grb.GRB.LESS_EQUAL, rhs=0.0,
                                name='Enzyme_constraint_' + str(i))

            # add biomass composition constraints HB * P <= 0
            if self.HB.any():
                HB = csr_matrix(self.HB[:, -self.numbers['proteins']:])
                for i in range(HB.shape[0]):
                    start1 = HB.indptr[i]
                    end1 = HB.indptr[i + 1]
                    variables = [proteins[j] for j in HB.indices[start1:end1]]
                    coeff = HB.data[start1:end1]
                    model.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.LESS_EQUAL, rhs=0,
                                    name='biomass_composition_' + str(i))

            # add maintenance constraint HM * V \geq b^T p
            if self.HM.any():
                HM = csr_matrix(self.HM)
                for i in range(HM.shape[0]):
                    start1 = HM.indptr[i]
                    end1 = HM.indptr[i + 1]
                    variables = [fluxes[j] for j in HM.indices[start1:end1]]
                    coeff = HM.data[start1:end1]
                    model.addConstr(lhs=grb.LinExpr(-coeff, variables) + np.dot(self.biomass[-self.numbers['proteins']:], proteins),
                                    sense=grb.GRB.LESS_EQUAL, rhs=0, name='maintenance_' + str(i))

            # add growth constraints mu*p <= S_p * V
            Sp = csr_matrix(self.scaled_S[-self.numbers['proteins']:, :])
            for i in range(Sp.shape[0]):
                start1 = Sp.indptr[i]
                end1 = Sp.indptr[i + 1]
                variables = [fluxes[j] for j in Sp.indices[start1:end1]]
                coeff = Sp.data[start1:end1]
                model.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.EQUAL, rhs=mu * proteins[i],
                                name="change_me_" + str(i))

            # add initial biomass amount constraint
            model.addConstr(lhs=np.dot(self.biomass[-self.numbers['proteins']:], proteins) - self.scale * initial_biomass_amount,
                            sense=grb.GRB.EQUAL, rhs=0.0, name='initial_biomass_amount_constraint')

            # add external conditions
            VMAX = None
            VMIN = None
            for number, reaction in enumerate(self.reactions[:self.numbers['exch_reactions']]):
                if reaction in self.uptake:
                    pos_constr = False
                    neg_constr = False
                    for elem in self.uptake[reaction]:
                        index = self.species.index(elem[0])
                        if ext_conditions[index] <= self.eps:
                            if not pos_constr:
                                if elem[1] == 1 and reaction in self.reversible:
                                    if VMIN is None:
                                        VMIN = np.zeros((1, len(self.reactions)))
                                        VMIN[0, number] = -1
                                    else:
                                        temp = np.zeros((1, len(self.reactions)))
                                        temp[0, number] = -1
                                        VMIN = np.vstack((VMIN, temp))
                            if not neg_constr:
                                if elem[1] == -1:
                                    if VMAX is None:
                                        VMAX = np.zeros((1, len(self.reactions)))
                                        VMAX[0, number] = 1
                                    else:
                                        temp = np.zeros((1, len(self.reactions)))
                                        temp[0, number] = 1
                                        VMAX = np.vstack((VMAX, temp))
            if VMAX is not None:
                VMAX = csr_matrix(VMAX)
                for i in range(VMAX.shape[0]):
                    start1 = VMAX.indptr[i]
                    end1 = VMAX.indptr[i + 1]
                    variables = [fluxes[j] for j in VMAX.indices[start1:end1]]
                    coeff = VMAX.data[start1:end1]
                    model.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.LESS_EQUAL, rhs=0,
                                    name="VMAX_" + str(i))
            if VMIN is not None:
                VMIN = csr_matrix(VMIN)
                for i in range(VMIN.shape[0]):
                    start1 = VMIN.indptr[i]
                    end1 = VMIN.indptr[i + 1]
                    variables = [fluxes[j] for j in VMIN.indices[start1:end1]]
                    coeff = VMIN.data[start1:end1]
                    model.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.LESS_EQUAL, rhs=0,
                                    name="VMIN_" + str(i))
            model.setParam('OutputFlag', False)
            # model.setParam('ScaleFlag', 0)
            model.setParam('NumericFocus', 3)
            model.update()
            model.optimize()
            # if no solution can be found for the chosen value of mu start the bisection
            if model.status == 3:
                model, mu = self.__bisec_gurobi(model, mu / 2.0, eps, 0, mu, fluxes, proteins)
            # if a solution exists for this mu, double mu and try until you find a mu value with no solution
            elif model.status == 2:
                model, mu = self.__bisec_gurobi(model, 2 * mu, eps, 0, 4 * mu, fluxes, proteins, down=False)
            elif model.status == 4:
                model.setParam('DualReductions', 0)
                model, mu = self.__bisec_gurobi(model, mu, eps, 0, mu, fluxes, proteins)
            else:
                raise ValueError('RBA not solvable')
            initial_biomass_vec = []
            initial_fluxes = []
            for i, protein in enumerate(proteins):
                initial_biomass_vec.append(protein.X / self.scale)
            for i, flux in enumerate(fluxes):
                if i >= self.numbers['exch_reactions'] + self.numbers['metab_reactions']:
                    initial_fluxes.append(flux.X / self.scale)
                else:
                    initial_fluxes.append(flux.X)
            initial_biomass_vec = np.array([i if i > 0 else 0.0 for i in initial_biomass_vec])
            print 'RBA predicts a growth rate mu = ' + str(mu)
            return initial_biomass_vec, initial_fluxes, mu
        elif solver == 'cvxopt':
            # if self.reduced:
            #     x, mu = self.__bisec_cvxopt(mu, eps, eps, 20, initial_biomass_amount, ext_conditions, down=False)
            #     initial_fluxes = x[:self.numbers['reduced_reactions']]
            #     for i in range(self.numbers['proteins']):
            #         initial_fluxes[-i] = initial_fluxes[-i] / self.scale
            #     initial_biomass = []
            #     for i, j in enumerate(x[self.numbers['reduced_reactions']:]):
            #         initial_biomass.append(float(j) / self.scale)
            #     return initial_biomass, initial_fluxes, mu
            # else:
            x, mu = self.__bisec_cvxopt(mu, eps, eps, 20, initial_biomass_amount, ext_conditions, down=False)
            initial_fluxes = x[:len(self.reactions)]
            for i in range(self.numbers['proteins']):
                initial_fluxes[-i] = initial_fluxes[-i] / self.scale
            initial_biomass = []
            for i, j in enumerate(x[len(self.reactions):]):
                initial_biomass.append(float(j) / self.scale)
            initial_biomass = np.array([i if i > 0 else 0.0 for i in initial_biomass])
            if self.initial_biomass is None:
                self.initial_biomass = initial_biomass
            if self.initial_fluxes is not None:
                self.initial_fluxes = initial_fluxes
            return initial_biomass, initial_fluxes, mu
        elif solver == 'cplex':
            initial_biomass_amount = self.scale * initial_biomass_amount
            if not globals.have_cplex:
                print 'Solver CPLEX not available. Fallback to cvxopt'
                return self.RBA(initial_biomass_amount, ext_conditions, eps=eps, solver='cvxopt', mu=mu)
            model = cplex.Cplex()
            model.set_log_stream(None)
            model.set_error_stream(None)
            model.set_warning_stream(None)
            model.set_results_stream(None)
            # try to populate by row
            # variables  = [V, P]
            model.objective.set_sense(model.objective.sense.minimize)
            model.variables.add(obj=np.zeros(len(self.reactions)), lb=[-cplex.infinity for i in range(len(self.reactions))],
                                names=['reac_' + str(j) for j in range(len(self.reactions))])
            model.variables.add(obj=np.zeros(self.numbers['proteins']), lb=[0.0 for i in range(self.numbers['proteins'])],
                                names=self.species[-self.numbers['proteins']:])
            # reversibility of fluxes
            for column, reac in enumerate(self.reactions):
                if reac not in self.reversible:
                    new_row = np.zeros((1, len(self.reactions)))
                    new_row[0, column] = -1
                    try:
                        pos = np.vstack((pos, new_row))
                    except UnboundLocalError:
                        pos = new_row
            HI = csr_matrix(pos)
            for i in range(HI.shape[0]):
                start1 = HI.indptr[i]
                end1 = HI.indptr[i + 1]
                variables = [np.asscalar(j) for j in HI.indices[start1:end1]]
                coeff = list(HI.data[start1:end1])
                model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="L", rhs=[0], names=['reversible_' + str(i)])

            # set qss constraints \dot{x}=Sx * V = 0
            Sx = csr_matrix(self.Sx)
            for i in range(Sx.shape[0]):
                start1 = Sx.indptr[i]
                end1 = Sx.indptr[i + 1]
                variables = [np.asscalar(j) for j in Sx.indices[start1:end1]]
                coeff = list(Sx.data[start1:end1])
                model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="E", rhs=[0.0])

            # Enzyme capacity constraints
            HC = csr_matrix(self.scaled_HC)
            HE = csr_matrix(self.HE[:, -self.numbers['proteins']:])
            for i in range(HC.shape[0]):
                start1 = HC.indptr[i]
                end1 = HC.indptr[i + 1]
                variables = [np.asscalar(j) for j in HC.indices[start1:end1]]
                coeff = list(HC.data[start1:end1])
                start2 = HE.indptr[i]
                end2 = HE.indptr[i + 1]
                variables2 = [j + len(self.reactions) for j in HE.indices[start2:end2]]
                coeff2 = list(-1 * HE.data[start2:end2])
                var = variables + variables2
                coe = coeff + coeff2
                model.linear_constraints.add(lin_expr=[[var, coe]], senses="L", rhs=[0.0])

            # add biomass composition constraints HB * P <= 0
            if self.HB.any():
                HB = csr_matrix(self.HB[:, -self.numbers['proteins']:])
                for i in range(HB.shape[0]):
                    start1 = HB.indptr[i]
                    end1 = HB.indptr[i + 1]
                    variables = [j + len(self.reactions) for j in HB.indices[start1:end1]]
                    coeff = list(HB.data[start1:end1])
                    model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="L", rhs=[0.0])
            # add maintenance constraint HM * V \geq b^T p
            if self.HM.any():
                HM = csr_matrix(self.scaled_HM)
                for i in range(HM.shape[0]):
                    start1 = HM.indptr[i]
                    end1 = HM.indptr[i + 1]
                    variables = np.array(
                        [j for j in HM.indices[start1:end1]] + [i - 1 + len(self.reactions) for i in range(self.numbers['proteins'])])
                    coeff = np.array(list(-1 * (HM.data[start1:end1])) + list(self.biomass[-self.numbers['proteins']:]))
                    model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="L", rhs=[0.0])

            # growth rate constraint
            Sp = csr_matrix(self.scaled_S[-self.numbers['proteins']:, :])
            for i in range(Sp.shape[0]):
                start1 = Sp.indptr[i]
                end1 = Sp.indptr[i + 1]
                vari = [int(j) for j in Sp.indices[start1:end1]]
                coef = list(Sp.data[start1:end1])
                vari.append(i + len(self.reactions))
                coef.append(float(-mu))
                model.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=vari, val=coef)], senses="E", rhs=[0.0],
                                             names=['delete_me_' + str(i)])

            # add initial biomass amount constraint
            model.linear_constraints.add(lin_expr=[
                [[j + len(self.reactions) for j in range(self.numbers['proteins'])], self.biomass[-self.numbers['proteins']:]]],
                senses="E", rhs=[initial_biomass_amount], names=['initial_biomass_constraint'])

            # add external conditions constraints
            VMAX = None
            VMIN = None
            for number, reaction in enumerate(self.reactions[:self.numbers['exch_reactions']]):
                if reaction in self.uptake:
                    pos_constr = False
                    neg_constr = False
                    for elem in self.uptake[reaction]:
                        index = self.species.index(elem[0])
                        if ext_conditions[index] <= self.eps:
                            if not pos_constr:
                                if elem[1] == 1 and reaction in self.reversible:
                                    if VMIN is None:
                                        VMIN = np.zeros((1, len(self.reactions)))
                                        VMIN[0, number] = -1
                                    else:
                                        temp = np.zeros((1, len(self.reactions)))
                                        temp[0, number] = -1
                                        VMIN = np.vstack((VMIN, temp))
                            if not neg_constr:
                                if elem[1] == -1:
                                    if VMAX is None:
                                        VMAX = np.zeros((1, len(self.reactions)))
                                        VMAX[0, number] = 1
                                    else:
                                        temp = np.zeros((1, len(self.reactions)))
                                        temp[0, number] = 1
                                        VMAX = np.vstack((VMAX, temp))
            if VMAX is not None:
                VMAX = csr_matrix(VMAX)
                for i in range(VMAX.shape[0]):
                    start1 = VMAX.indptr[i]
                    end1 = VMAX.indptr[i + 1]
                    vari = [int(j) for j in VMAX.indices[start1:end1]]
                    coef = list(VMAX.data[start1:end1])
                    vari.append(i + len(self.reactions))
                    coef.append(float(-mu))
                    model.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=vari, val=coef)], senses="L", rhs=[0.0],
                                                 names=['VMAX_' + str(i)])
            if VMIN is not None:
                VMIN = csr_matrix(VMIN)
                for i in range(VMIN.shape[0]):
                    start1 = VMIN.indptr[i]
                    end1 = VMIN.indptr[i + 1]
                    vari = [int(j) for j in VMIN.indices[start1:end1]]
                    coef = list(VMIN.data[start1:end1])
                    vari.append(i + len(self.reactions))
                    coef.append(float(-mu))
                    model.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=vari, val=coef)], senses="L", rhs=[0.0],
                                                 names=['VMIN_' + str(i)])
            # solve
            model.solve()
            if model.solution.status[model.solution.get_status()] == 'optimal':
                return self.__bisec_cplex(model, eps, mu, 2 * mu, initial_biomass_amount, down=False)
            else:
                return self.__bisec_cplex(model, eps, 0, mu, initial_biomass_amount, down=True)
        elif solver == 'soplex':
            initial_biomass_amount = self.scale * initial_biomass_amount
            if not globals.have_soplex:
                print 'Solver SOPLEX not available. Fallback to cvxopt'
                return self.RBA(initial_biomass_amount, ext_conditions, eps=eps, solver='cvxopt', mu=mu)
            model = soplex.Model()
            model.setParam('numerics/feastol', eps)
            model.setParam('reoptimization/enable', True)
            sol_reac = {}
            for i, reac in enumerate(self.reactions):
                if reac in self.reversible:
                    sol_reac[i] = model.addVar(lb=None, ub=None, vtype="C", name=reac)
                else:
                    sol_reac[i] = model.addVar(lb=0, ub=None, vtype="C", name=reac)
            sol_prot = {}
            for i, prot in enumerate(self.species[-self.numbers['proteins']:]):
                sol_prot[i] = model.addVar(lb=0, ub=None, vtype="C", name=prot)
            # mu = model.addVar
            # Define biomass amount in model
            biomass = soplex.quicksum([sol_prot[j] * self.biomass[-self.numbers['proteins'] + j] for j in range(self.numbers['proteins'])])
            # set qss constraints \dot{x}=Sx * V = 0
            Sx = csr_matrix(self.Sx)
            for i in range(Sx.shape[0]):
                start1 = Sx.indptr[i]
                end1 = Sx.indptr[i + 1]
                variables = [np.asscalar(j) for j in Sx.indices[start1:end1]]
                coeff = list(Sx.data[start1:end1])
                model.addCons(soplex.quicksum(sol_reac[variables[j]] * coeff[j] for j in range(len(variables))) == 0.0, "qss_%s" % i)
            # Enzyme capacity constraints
            HC = csr_matrix(self.scaled_HC)
            HE = csr_matrix(self.HE[:, -self.numbers['proteins']:])
            for i in range(HC.shape[0]):
                start_reac = HC.indptr[i]
                end_reac = HC.indptr[i + 1]
                variables_reac = [np.asscalar(j) for j in HC.indices[start_reac:end_reac]]
                coeff_reac = list(HC.data[start_reac:end_reac])
                start_prot = HE.indptr[i]
                end_prot = HE.indptr[i + 1]
                variables_prot = [j for j in HE.indices[start_prot:end_prot]]
                coeff_prot = list(-1 * HE.data[start_prot:end_prot])
                # var = variables + variables2
                # coe = coeff + coeff2
                model.addCons(soplex.quicksum(
                    [soplex.quicksum(sol_reac[variables_reac[j]] * coeff_reac[j] for j in range(len(variables_reac))),
                     soplex.quicksum(sol_prot[variables_prot[j]] * coeff_prot[j] for j in range(len(variables_prot)))]) <= 0.0,
                              "ecc_%s" % i)
            # add biomass composition constraints HB * P <= 0
            if self.HB.any():
                HB = csr_matrix(self.HB[:, -self.numbers['proteins']:])
                for i in range(HB.shape[0]):
                    start1 = HB.indptr[i]
                    end1 = HB.indptr[i + 1]
                    variables = [j for j in HB.indices[start1:end1]]
                    coeff = list(HB.data[start1:end1])
                    model.addCons(soplex.quicksum(sol_prot[variables[j]] * coeff[j] for j in range(len(variables))) <= 0.0, "bcc_%s" % i)

            # add maintenance constraint HM * V \geq b^T p
            if self.HM.any():
                HM = csr_matrix(self.scaled_HM)
                for i in range(HM.shape[0]):
                    start1 = HM.indptr[i]
                    end1 = HM.indptr[i + 1]
                    variables = np.array([j for j in HM.indices[start1:end1]])
                    coeff = np.array(list(-1 * (HM.data[start1:end1])))
                    model.addCons(soplex.quicksum(
                        [soplex.quicksum(sol_reac[variables[j]] * coeff[j] for j in range(len(variables))), biomass]) <= 0.0, "mc_%s" % i)

            # growth rate constraint mu*P = S*V
            Sp = csr_matrix(self.scaled_S[-self.numbers['proteins']:, :])
            for i in range(Sp.shape[0]):
                start1 = Sp.indptr[i]
                end1 = Sp.indptr[i + 1]
                vari = [int(j) for j in Sp.indices[start1:end1]]
                coef = list(Sp.data[start1:end1])
                model.addCons(soplex.quicksum([
                    soplex.quicksum(sol_reac[vari[j]] * coef[j] for j in range(len(vari))),
                    -mu * sol_prot[i]]) == 0.0, "mu_%s" % i)

                # add initial biomass amount constraint
                model.addCons(biomass == initial_biomass_amount, "initial_biomass_con")

            # add external conditions constraints
            VMAX = None
            VMIN = None
            for number, reaction in enumerate(self.reactions[:self.numbers['exch_reactions']]):
                if reaction in self.uptake:
                    pos_constr = False
                    neg_constr = False
                    for elem in self.uptake[reaction]:
                        index = self.species.index(elem[0])
                        if ext_conditions[index] <= self.eps:
                            if not pos_constr:
                                if elem[1] == 1 and reaction in self.reversible:
                                    if VMIN is None:
                                        VMIN = np.zeros((1, len(self.reactions)))
                                        VMIN[0, number] = -1
                                    else:
                                        temp = np.zeros((1, len(self.reactions)))
                                        temp[0, number] = -1
                                        VMIN = np.vstack((VMIN, temp))
                            if not neg_constr:
                                if elem[1] == -1:
                                    if VMAX is None:
                                        VMAX = np.zeros((1, len(self.reactions)))
                                        VMAX[0, number] = 1
                                    else:
                                        temp = np.zeros((1, len(self.reactions)))
                                        temp[0, number] = 1
                                        VMAX = np.vstack((VMAX, temp))
            if VMAX is not None:
                VMAX = csr_matrix(VMAX)
                for i in range(VMAX.shape[0]):
                    VMAX = csr_matrix(VMAX[:, -self.numbers['proteins']:])
                    for i in range(VMAX.shape[0]):
                        start1 = VMAX.indptr[i]
                        end1 = VMAX.indptr[i + 1]
                        variables = [j for j in VMAX.indices[start1:end1]]
                        coeff = list(VMAX.data[start1:end1])
                        model.addCons(soplex.quicksum(sol_prot[variables[j]] * coeff[j] for j in range(len(variables))) <= 0.0,
                                      "VMAX_%s" % i)
            if VMIN is not None:
                VMIN = csr_matrix(VMIN)
                for i in range(VMIN.shape[0]):
                    VMIN = csr_matrix(VMIN[:, -self.numbers['proteins']:])
                    for i in range(VMIN.shape[0]):
                        start1 = VMIN.indptr[i]
                        end1 = VMIN.indptr[i + 1]
                        variables = [j for j in VMIN.indices[start1:end1]]
                        coeff = list(VMIN.data[start1:end1])
                        model.addCons(soplex.quicksum(sol_prot[variables[j]] * coeff[j] for j in range(len(variables))) <= 0.0,
                                      "VMIN_%s" % i)
            # solve
            model.optimize()
            # model.data = sol_reac, sol_prot
            status = model.getStatus()
            del model
            if status == 'optimal':
                return self.__bisec_soplex(eps, mu, 2 * mu, initial_biomass_amount, down=False)
                # return self.__bisec_soplex(model, eps, mu, 2 * mu, initial_biomass_amount, down=False)
            else:
                return self.__bisec_soplex(eps, 0, mu, initial_biomass_amount, down=True)
                # return self.__bisec_soplex(model, eps, 0, mu, initial_biomass_amount, down=True)
        else:
            print 'solver unknown or not installed'
            return None

    def robust(self, uncertain_values, Tend, stepsize, external_conditions, prediction_horizon=None, robust_horizon=None, RBA_init=False,
               initial_biomass=None, initial_fluxes=None, solver='cplex', RBA_solver=None, export=True, output_name='robust_', params={}):
        """
        uncertain_values is a dict containing the parameter-name (cf. DefbaModel.parameter_list) and the different values for the
        construction of the scenarios. The robust simulation is not compatible with reduced models, which incorporate the steady-state
        assumption via the SVD of the stoichiometric matrix. The parameter names are derived from the parameter ids in the SBML file.

        Required parameters:
        - uncertain values          dict containing the uncertain parameters. Format {'param_name':[value_1, value_2, ..., value_n]}
        - Tend                      float. end-time for simulation.
        - steps                     int. number of discretization steps used

        Optional parameters:
        - prediction_horizon        float. Length of prediction horizon. If not given prediction horizon is calculated automatically.
        - robust_horizon            float. Length of robust horizon. Set to stepsize if not given.
        - initial_biomass           list/ndarray with initial values for all biomass products
        - external_conditions:      list with initial nutrient amounts in the substrate. Can be given as list or dict
                                    if set to None initial conditions from DefbaModel object are used
        - solver                    string for choosing the solver. Available are cvxopt, gurobi, cplex
        - export                    boolean switch whether to export to file. standartly called 'control' and 'species'
        - output_name               if export is chosen, you can change the output name of the data to 'control_output_name' and
                                    'states_output_name'
        """
        midpoint = True
        if external_conditions is not None:
            if isinstance(external_conditions, list) or isinstance(external_conditions, np.ndarray):
                if len(external_conditions) != self.numbers['ext_species']:
                    raise ValueError('Length of external conditions vector is wrong. ' + str(len(external_conditions)) + '!=' + str(
                        self.numbers['ext_species']))
            elif isinstance(external_conditions, dict):
                temp = []
                for specie in self.species[:self.numbers['ext_species']]:
                    temp.append(external_conditions[specie])
                external_conditions = temp
        if initial_fluxes is not None:
            if isinstance(initial_fluxes, dict):
                temp = []
                for reac in self.reactions:
                    temp.append(initial_fluxes[reac])
                initial_fluxes = temp
            elif isinstance(initial_fluxes, list):
                if len(initial_fluxes) != len(self.reactions):
                    raise ValueError('Length of initial fluxes is wrong ' + str(len(initial_fluxes)) + '!=' + str(len(self.reactions)))
            elif isinstance(initial_fluxes, np.ndarray):
                if np.max(initial_fluxes.shape) != len(self.reactions):
                    raise ValueError(
                        'Length of initial fluxes is wrong ' + str(np.max(initial_fluxes.shape)) + '!=' + str(len(self.reactions)))
        if initial_biomass is not None:
            if isinstance(initial_biomass, dict):
                init_bm_temp = []
                for specie_name in self.species[-self.numbers['proteins']:]:
                    init_bm_temp.append(initial_biomass[specie_name])
                initial_biomass = init_bm_temp
        if RBA_solver is None:
            RBA_solver = solver
        number_of_scenarios = 1
        steps = int(np.ceil(Tend / float(stepsize)))
        # Sort uncertainties by type
        weight_param = {}
        objective_param = {}
        ob_weight_param = {}
        kcat_param = {}
        main_param = {}
        biomp_param = {}
        env_param = {}
        parameter_list = self.parameter_list
        degradation_time = {}
        for param_name in uncertain_values:
            if parameter_list[param_name][-1] == 'weight':
                weight_param[param_name] = parameter_list[param_name]
                number_of_scenarios = number_of_scenarios * len(uncertain_values[param_name])
            elif parameter_list[param_name][-1] == 'objective':
                objective_param[param_name] = parameter_list[param_name]
                number_of_scenarios = number_of_scenarios * len(uncertain_values[param_name])
            elif parameter_list[param_name][-1] == 'ob_weight':
                ob_weight_param[param_name] = parameter_list[param_name]
                number_of_scenarios = number_of_scenarios * len(uncertain_values[param_name])
            elif parameter_list[param_name][-1] == 'kcat':
                kcat_param[param_name] = parameter_list[param_name]
                number_of_scenarios = number_of_scenarios * len(uncertain_values[param_name])
            elif parameter_list[param_name][-1] == 'maintenance':
                main_param[param_name] = parameter_list[param_name]
                number_of_scenarios = number_of_scenarios * len(uncertain_values[param_name])
            elif parameter_list[param_name][-1] == 'biom_percent':
                biomp_param[param_name] = parameter_list[param_name]
                number_of_scenarios = number_of_scenarios * len(uncertain_values[param_name])
            elif parameter_list[param_name][-1] == 'environment':
                env_param[param_name] = parameter_list[param_name]
                degradation_time[param_name]= np.max(uncertain_values[param_name])
                if not 0 < degradation_time[param_name] < 1:
                    raise AttributeError('The uncertain environment '+str(param_name)+' has a faulty degration time. This attribute must '
                                                                                      'be between 0 and 1.')
                number_of_scenarios = number_of_scenarios * 2
        # construct the scenarios by creating all possible parameter combinations
        # What is the form on 'scenarios'?
        scenarios = {i: {} for i in range(number_of_scenarios)}
        block_length = 1
        for uncertainty in uncertain_values:
            step_counter = 0
            value_counter = 0
            if uncertainty in env_param:
                list_with_uncertain_values = [0, 1]
            else:
                list_with_uncertain_values = uncertain_values[uncertainty]
            for scenario_number in range(number_of_scenarios):
                scenarios[scenario_number][uncertainty] = list_with_uncertain_values[value_counter]
                step_counter += 1
                if step_counter == block_length:
                    step_counter = 0
                    value_counter += 1
                    if value_counter == len(list_with_uncertain_values):
                        value_counter = 0
            block_length = block_length * len(list_with_uncertain_values)
        # depending on which kind of parameters are uncertain, we must recreate parts of the model for each scenario.
        # Basically, we build a lot of different deFBA models and couple these via the shared objective
        # J = \sum_{i=1}^{number_of_scenarios} J^i
        # and enforcing the first few set of fluxes to be identical for all scenarios. This is controlled by the robust horizon.
        # Before adding the handler for all possible inputs of the initial_values, external_conditions, etc. let us assume these are all
        # given by the model dynamics are given as \dot{x} = Sv + Rx + q
        if external_conditions is None:
            if self.external_conditions is not None:
                external_conditions = self.external_conditions
            else:
                raise ValueError('no external conditions are given')
        # handle initial biomass
        if initial_biomass is not None:
            init_bm_amount = np.dot(self.biomass[-len(initial_biomass):], initial_biomass)
        else:
            init_bm_amount = 1
        if RBA_init:
            initial_biomass, initial_fluxes, mu_max = self.RBA(init_bm_amount, external_conditions, solver=RBA_solver)
        if initial_biomass is None:
            if self.initial_biomass is not None:
                initial_biomass = self.initial_biomass
            else:
                initial_biomass, initial_fluxes, mu_max = self.RBA(init_bm_amount, external_conditions, solver=RBA_solver)
        if initial_fluxes is None and env_param:
            initial_fluxes = self.RBA(init_bm_amount, external_conditions, solver=RBA_solver)[1]
        # Handle prediction horizon
        if prediction_horizon is None:
            pred_temp, control = self.calcPredictionHorizon(solver=solver, initial_values=initial_biomass)
            prediction_horizon = int(np.ceil(pred_temp / float(stepsize)))
            exact_pred_time = prediction_horizon * stepsize
        else:
            prediction_horizon = int(np.ceil(prediction_horizon / float(stepsize)))
            exact_pred_time = prediction_horizon * stepsize
        if robust_horizon is None:
            robust_horizon_steps = 1
        else:
            robust_horizon_steps = int(np.floor(robust_horizon / float(stepsize)))
            if robust_horizon_steps <= 0:
                robust_horizon_steps = 1
        # Additional dynamics
        if self.R.any():
            R = spr.lil_matrix(self.R)
            R = spr.block_diag([R for i in range(number_of_scenarios)], 'lil')
        else:
            R = np.zeros((self.R.shape[0] * number_of_scenarios, self.R.shape[1] * number_of_scenarios))
        scaled_S = spr.lil_matrix(self.scaled_S.copy())
        scaled_S = spr.block_diag([scaled_S for i in range(number_of_scenarios)], 'lil')
        q = np.concatenate([self.q for i in range(number_of_scenarios)])
        if 'mu_max' not in locals():
            mu_max = self.RBA(init_bm_amount, np.ones(self.numbers['ext_species']) * 1000000, solver=RBA_solver)[2]
        exp_grow_constant = np.exp(mu_max * stepsize * robust_horizon_steps)
        # set initial values with new
        initial_values = np.concatenate(
            [np.concatenate((external_conditions, np.zeros(self.numbers['metab_species']), self.scale * np.array(initial_biomass))) for i in
             range(number_of_scenarios)])
        # Initialize the Optimizer
        if midpoint:
            optimizer_tool = midopt.MidOpt(R, scaled_S, initial_values, q)
        else:
            optimizer_tool = linopt.LinOpt(R, scaled_S, initial_values, q)
        # Initialization completed. This contains all linear dynamics already.
        # Next we add the objective
        if objective_param or ob_weight_param:
            overall_objective = []
            uncertain_position = []
            for entry in objective_param:
                for maybe_obj_position in objective_param[entry][0]:
                    if 'biomass' in maybe_obj_position:
                        if self.objective_vector[int(maybe_obj_position[8:-1])] != 0:
                            uncertain_position.append([entry, int(maybe_obj_position[8:-1])])
                        break
            for entry in ob_weight_param:
                for maybe_obj_position in ob_weight_param[entry][0]:
                    if 'biomass' in maybe_obj_position:
                        if self.objective_vector[int(maybe_obj_position[8:-1])] != 0:
                            uncertain_position.append([entry, int(maybe_obj_position[8:-1])])
                        break
            for number in range(number_of_scenarios):
                scenario_objective = self.objective_vector.copy()
                for position in uncertain_position:
                    scenario_objective[position[1]] = scenarios[number][position[0]]
                overall_objective.append(scenario_objective)
            overall_objective = np.concatenate(overall_objective)
            optimizer_tool.set_objective(np.array([-sc_objective for sc_objective in overall_objective]),
                                         np.zeros(number_of_scenarios * len(self.reactions)), 0,
                                         np.zeros(number_of_scenarios * len(self.objective_vector)))
        else:
            optimizer_tool.set_objective(np.concatenate([-self.objective_vector for i in range(number_of_scenarios)]),
                                         np.zeros(number_of_scenarios * len(self.reactions)),
                                         0,
                                         np.zeros(number_of_scenarios * len(self.objective_vector)))
        # setup the path constraints. The are Enzyme Capacity Constr, Biomass Composition Constraint, Positivity constraints, Maintenance,
        # reversibility and the nonanticipativity constraints.
        # Format for the constraints is Gx x(t) + Gu u(t) + Gk <= 0
        # We construct the constraints via blockdiagonal matrices
        # We begin with the positivity constraints X >= 0
        Gx_positivity = spr.eye(len(self.species) * number_of_scenarios)
        Gu_positivity = np.zeros((len(self.species) * number_of_scenarios, len(self.reactions) * number_of_scenarios))
        Gk_positivity = np.zeros(len(self.species) * number_of_scenarios)
        if midpoint:
            optimizer_tool.add_state_constraint(-Gx_positivity, Gk_positivity)
        else:
            optimizer_tool.add_path_constraint(-Gx_positivity, Gu_positivity, Gk_positivity)
        # reversibility of reactions
        temp = np.zeros((len(self.reactions) - len(self.reversible), len(self.reactions)))
        counter = 0
        for coloumn, reaction in enumerate(self.reactions):
            if reaction not in self.reversible:
                temp[counter, self.reactions.index(reaction)] = -1
                counter += 1
        temp = spr.csr_matrix(temp)
        Gu_reversibility = spr.block_diag([temp for i in range(number_of_scenarios)])
        Gx_reversibility = np.zeros((Gu_reversibility.shape[0], len(self.species) * number_of_scenarios))
        Gk_reversibility = np.zeros(Gu_reversibility.shape[0])
        optimizer_tool.add_path_constraint(Gx_reversibility, Gu_reversibility, Gk_reversibility)
        # Biomass Composition Constraint
        if self.HB.any():
            HB_collection = [self.HB.copy() for i in range(number_of_scenarios)]
            if weight_param or ob_weight_param:
                for weight_name in weight_param:
                    for param_entry_number, maybe_obj_position in enumerate(weight_param[weight_name][0]):
                        if 'HB[' in maybe_obj_position:
                            obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
                            obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
                            for number in range(number_of_scenarios):
                                HB_collection[number][obj_position_row, obj_position_coloumn] = (scenarios[number][weight_name] ** (
                                    weight_param[weight_name][2][param_entry_number])) * weight_param[weight_name][3][param_entry_number]
                for weight_name in ob_weight_param:
                    for param_entry_number, maybe_obj_position in enumerate(ob_weight_param[weight_name][0]):
                        if 'HB[' in maybe_obj_position:
                            obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
                            obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
                            for number in range(number_of_scenarios):
                                HB_collection[number][obj_position_row, obj_position_coloumn] = (scenarios[number][weight_name] ** (
                                    ob_weight_param[weight_name][2][param_entry_number])) * ob_weight_param[weight_name][3][
                                                                                                    param_entry_number]
            if biomp_param:
                for biomp_name in biomp_param:
                    for param_entry_number, maybe_obj_position in enumerate(biomp_param[biomp_name][0]):
                        if 'HB[' in maybe_obj_position:
                            obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
                            obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
                            for number in range(number_of_scenarios):
                                if biomp_param[biomp_name][3][param_entry_number] == -1:
                                    HB_collection[number][obj_position_row, obj_position_coloumn] *= (scenarios[number][biomp_name] - 1) / (
                                        biomp_param[biomp_name][1] - 1)
                                else:
                                    HB_collection[number][obj_position_row, obj_position_coloumn] *= scenarios[number][biomp_name] / \
                                                                                                     biomp_param[biomp_name][1]
            Gx_biomass = spr.block_diag(HB_collection)
            Gu_biomass = np.zeros((Gx_biomass.shape[0], number_of_scenarios * len(self.reactions)))
            Gk_biomass = np.zeros(Gx_biomass.shape[0])
            if midpoint:
                optimizer_tool.add_state_constraint(Gx_biomass, Gk_biomass)
            else:
                optimizer_tool.add_path_constraint(Gx_biomass, Gu_biomass, Gk_biomass)
        # Enzyme Capacity Constraints
        if self.HC.any():
            HC_collection = [self.scaled_HC.copy() for i in range(number_of_scenarios)]
            HE_collection = [-self.HE.copy() for i in range(number_of_scenarios)]
            if kcat_param:
                for kcat_name in kcat_param:
                    for param_entry_number, maybe_obj_position in enumerate(kcat_param[kcat_name][0]):
                        if 'HC[' in maybe_obj_position:
                            obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
                            obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
                            for number in range(number_of_scenarios):
                                HC_collection[number][obj_position_row, obj_position_coloumn] = (scenarios[number][kcat_name] **
                                                                                                 kcat_param[kcat_name][2][
                                                                                                     param_entry_number]) * \
                                                                                                kcat_param[kcat_name][3][param_entry_number]
            Gx_ecc = spr.block_diag(HE_collection)
            Gu_ecc = spr.block_diag(HC_collection)
            Gk_ecc = np.zeros(Gx_ecc.shape[0])
            optimizer_tool.add_path_constraint(Gx_ecc, Gu_ecc, Gk_ecc)
        # Maintenance Constraints
        if self.HM.any():
            HM_collection = [-self.scaled_HM.copy() for i in range(number_of_scenarios)]
            if main_param:
                for main_name in main_param:
                    for param_entry_number, maybe_obj_position in enumerate(main_param[main_name][0]):
                        if 'HM[' in maybe_obj_position:
                            obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
                            obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
                            for number in range(number_of_scenarios):
                                HM_collection[number][obj_position_row, obj_position_coloumn] = -(scenarios[number][main_name] **
                                                                                                  main_param[main_name][2][
                                                                                                      param_entry_number]) * \
                                                                                                main_param[main_name][3][param_entry_number]
                                # not yet working part
                                #       if weight_param or ob_weight_param:
                                #           biomass_collection = biomass_matrix = np.array([self.biomass for i in range(self.HM.shape[0])])
                                #           for weight_name in weight_param:
            biomass_matrix = np.array([self.biomass for i in range(self.HM.shape[0])])
            Gx_main = spr.block_diag([biomass_matrix for i in range(number_of_scenarios)])
            Gu_main = spr.block_diag(HM_collection)
            Gk_main = np.zeros(Gu_main.shape[0])
            optimizer_tool.add_path_constraint(Gx_main, Gu_main, Gk_main)
        # add steady-state constraint for all scenarios as control constraints
        Sx_collection = [self.Sx.copy() for i in range(number_of_scenarios)]
        Hu_steady = spr.block_diag(Sx_collection)
        Hk_steady = np.zeros(Hu_steady.shape[0])
        optimizer_tool.add_control_constraint(Hu_steady, Hk_steady)
        # add control constraints for the first set of controls
        Hu_robust = np.zeros(((number_of_scenarios - 1) * len(self.reactions), number_of_scenarios * len(self.reactions)))
        for iteration in range(0, number_of_scenarios - 1):
            for runner in range(len(self.reactions)):
                Hu_robust[iteration * len(self.reactions) + runner, runner] = 1
                Hu_robust[iteration * len(self.reactions) + runner, (iteration + 1) * len(self.reactions) + runner] = -1
        Hk_robust = np.zeros(Hu_robust.shape[0])
        if Hu_robust.any():
            if midpoint:
                optimizer_tool.add_initial_control_constraint(Hu_robust, Hk_robust, r_horizon=robust_horizon_steps)
            else:
                optimizer_tool.add_initial_control_constraint(Hu_robust, Hk_robust)
        # What is the form of env_param. What do we need to construct the new type of constraint?
        # fluxes are structured [V^0; V^1; .. ;V^{number_of_scenarios}]
        # We need to know which reaction is constrained. See self.uncertain_environment[env_name][0], created by addUncertainEnvironment
        # and if this is an increasing or decreasing environment. See self.uncertain_environment[env_name][1]
        # how are the values for the uncertainty determined? answer: look at variable 'scenarios'
        # We need to construct a constraint d * V_{in,k} \leq p_k
        if env_param:
            # got self.uncertain_environment[env_name] = [reaction_name, 'increasing'/'decreasing']
            for env_name in env_param:
                reac_pos = self.reactions.index(self.uncertain_environment[env_name][0])
                if self.uncertain_environment[env_name][1] == 'decreasing':
                    Hu_decreasing = np.zeros((int(number_of_scenarios/2), self.stoich.shape[1]*number_of_scenarios))
                    Hk_decreasing = np.zeros((Hu_decreasing.shape[0]))
                    counter = 0
                    for scenario_id in scenarios:
                        if scenarios[scenario_id][env_name] ==1:
                            Hu_decreasing[counter, reac_pos + scenario_id * self.stoich.shape[1]] = 1
                            Hk_decreasing[counter] = initial_fluxes[reac_pos]*exp_grow_constant
                            counter += 1
                    optimizer_tool.add_environmental_constraint(Hu_decreasing, Hk_decreasing, 'decreasing', robust_horizon_steps,
                                                                int((degradation_time[env_name]*exact_pred_time)/stepsize))
                elif self.uncertain_environment[env_name][1] == 'increasing':
                # add a control constraint in the form of d * V_{in,k} \leq p_k
                # for increasing species this means
                # V_{in,k} \leq 0, \forall k \leq robust_horizon_steps
                    Hu_increasing = np.zeros((int(number_of_scenarios/2), self.stoich.shape[1]*number_of_scenarios))
                    Hk_increasing = np.zeros(Hu_increasing.shape[0])
                    counter = 0
                    for scenario_id in scenarios:
                        if scenarios[scenario_id][env_name] == 1:
                            Hu_increasing[counter, reac_pos + scenario_id * self.stoich.shape[1]] = 1
                            counter += 1
                    optimizer_tool.add_environmental_constraint(Hu_increasing, Hk_increasing, 'increasing', robust_horizon_steps, 0)
                else:
                    raise ValueError(
                        'environmental parameter "' + str(env_name) + '" not understood. Please check if this an increasing or '
                                                                      'decreasing environment')
        # set the solver and other parameters
        optimizer_tool.set_solver(solver)
        optimizer_tool.set_parameters(timesteps=prediction_horizon)
        optimizer_tool.set_parameters(finaltime=exact_pred_time)
        optimizer_tool.set_parameters(fixedterm=True)
        try:
            all_params = merge_dicts(
                {'keep_matrices': True}, params)
            temp_res = optimizer_tool.solve(**all_params)
        except ImportError:
            optimizer_tool.set_solver('cvxopt')
            temp_res = optimizer_tool.solve()
        time_now = stepsize
        old_states = initial_values
        states = temp_res[1].get_state(stepsize)
        res_species = np.array([initial_values[:len(self.species)], states[:len(self.species)]])
        if initial_fluxes is not None:
            res_fluxes = np.zeros((1, len(self.reactions)))
        elif self.initial_fluxes:
            res_fluxes = np.array(np.atleast_2d(self.initial_fluxes))
        else:
            res_fluxes = np.zeros((1, len(self.reactions)))
        # time_now += stepsize
        res_fluxes = np.atleast_2d(np.array(temp_res[1].get_control(0)[:len(self.reactions)]))
        finished = False
        states = np.array([i if i >= 0 else 0 for i in states])
        while not finished:
            error = False
            if env_param:
                old_fluxes = temp_res[1].get_control(0)
                new_uk = []
                for env_name in env_param:
                    # reac_pos = self.reactions.index(self.uncertain_environment[env_name][0])
                    if self.uncertain_environment[env_name][1] == 'decreasing':
                        Hk_decreasing = np.zeros((int(number_of_scenarios / 2)))
                        counter = 0
                        for scenario_id in scenarios:
                            if scenarios[scenario_id][env_name] == 1:
                                Hk_decreasing[counter] = old_fluxes[reac_pos]*exp_grow_constant
                                counter += 1
                        new_uk.append(Hk_decreasing)
                    else:
                        new_uk.append(0)
                try:
                    temp_res = optimizer_tool.solve_iteration(states, new_uk=new_uk)
                except:
                    error = True
            else:
                states = np.array(states)
                try:
                    temp_res = optimizer_tool.solve_iteration(states)
                except:
                    error = True
            if time_now >= Tend:
                finished = True
            if not error:
                time_now += stepsize
                old_states = states.copy()
                states = temp_res[1].get_state(stepsize)
                states = np.array([i if i >= 0 else 0 for i in states])
                res_species = np.append(res_species, np.atleast_2d(np.array(states[:len(self.species)])), axis=0)
                res_fluxes = np.append(res_fluxes, np.atleast_2d(np.array(temp_res[1].get_control(0)[:len(self.reactions)])), axis=0)
            else:
                finished = True
        if not error:
            times = [i * float(Tend / steps) for i in xrange(steps + 1)]
        else:
            times = [i * stepsize for i in xrange(res_species.shape[0])]
        # write results to external txt files
        if export and not error:
            export_states(times, res_species, self.species, self.scale,
                          self.numbers['ext_species'] + self.numbers['metab_species'],
                          file_name=output_name + '_robust_species')
            export_fluxes(times, res_fluxes, self.reactions, self.scale, self.numbers['metab_reactions'] + self.numbers['exch_reactions'],
                          file_name=output_name + '_robust_fluxes')
        else:
            export_states(times, res_species, self.species, self.scale, self.numbers['ext_species'] + self.numbers['metab_species'],
                          file_name='robust_error_species')
            export_fluxes(times, res_fluxes, self.reactions, self.scale, self.numbers['metab_reactions'] + self.numbers['exch_reactions'],
                          file_name='robust_error_fluxes')
            print 'There was an optimality error in the solution at time ' + str(time_now) + '. Solutions so far written to ' \
                  'robust_error_species.csv and robust_error_fluxes.csv.'
        self.results = [times, res_species, res_fluxes]
        return times, res_species, res_fluxes

    def shortterm(self, Tend=1, stepsize=1, prediction_horizon=None, control_horizon=None, external_conditions=None, RBA_init=False,
                  initial_biomass_amount=0.1, initial_biomass=None, eps=10 ** (-8), export=True, output_name='', solver='cplex',
                  calc_horizon=False, fixed_horizons=False, midpoint=False, params={}):
        """
        Iterative implementation of the deFBA. Needs additionally a prediction horizon.
        Keyword arguments:
        - Tend (float):                 end-time for simulation
        - stepsize (float):             size for time discretization
        - prediction_horizon (float):   solver planning this far ahead
        - control_horizon (float):      iteration time added to the solution curve
        - external_conditions (list):   nutrient amounts in the substrate
        - RBA_init (bool):              Decide whether to use RBA for calculation of initial values
        - initial_biomass_amount(float):representing the amount of biomass (gram dryweight) at t=0
        - initial_biomass (list)        initial values for all biomass products
        - eps (float):                  precision used in RBA_init
        - solver (str):                 string for choosing the solver. Available are cvxopt, gurobi, cplex
        - export (bool):                export to file. Usually called 'control.dat', 'species.dat'
        - output_name (str):            if export is chosen, you can change the output name of the data to 'control_'output_name'.dat'
        - calc_horizon (bool):          decide whether the automatic prediction horizon is used or a given one
        - fixed_horizons (bool)         decide whether the prediction horizon & iteration time are recalculated during simulation
        Returns:
         -tuple: times, res_species, res_fluxes
            - times (list) is the list with times
            - res_species (list) contains predictions for species
            - res_fluxes (list) contains predictions for reaction fluxes
        """
        steps = int(np.ceil(Tend / float(stepsize)))
        if not output_name and self.name:
            output_name = self.name

        if external_conditions is None:
            if self.external_conditions is not None:
                external_conditions = self.external_conditions
                print 'using initial values from SBML file'
            else:
                raise ValueError('No external conditions defined')
        elif isinstance(external_conditions, dict):
            try:
                new_ext = []
                for specie in self.species[: self.numbers['ext_species']]:
                    new_ext.append(external_conditions[specie])
                external_conditions = new_ext
            except KeyError:
                raise ValueError('Not all external species are defined. Missing initial condition for ' + specie)
        elif isinstance(external_conditions, list) or isinstance(external_conditions, np.ndarray):
            if len(external_conditions) != self.numbers['ext_species']:
                raise ValueError('The length of the external conditions vector is wrong.')
        else:
            raise ValueError('No external conditions defined')

        # Set initial values. Starts automatically if no initial biomass given by user or DefbaModel object
        if initial_biomass is None:
            if RBA_init:
                initial_biomass, initial_fluxes, mu = self.RBA(initial_biomass_amount, ext_conditions=external_conditions, eps=eps,
                                                               solver=solver)
            elif self.initial_biomass is not None:
                initial_biomass = self.initial_biomass
            else:
                print 'No initial values given. Falling back for RBA initialization'
                initial_biomass, initial_fluxes, mu = self.RBA(initial_biomass_amount, ext_conditions=external_conditions, eps=eps,
                                                               solver=solver)
        elif isinstance(initial_biomass, dict):
            temp = []
            for specie in self.species[-self.numbers['proteins']:]:
                temp.append(initial_biomass[specie])
            initial_biomass = temp
        # create loop for sdeFBA
        initial_biomass = [i if i > 0 else 0.0 for i in initial_biomass]
        control_horizon_backup = None
        if calc_horizon or not prediction_horizon:
            prediction_horizon, control_horizon_backup = self.calcPredictionHorizon(solver=solver, biomass_comp=initial_biomass,
                                                                                    environment=external_conditions)
        else:
            if isinstance(prediction_horizon, (tuple, list)):
                prediction_horizon = prediction_horizon[0]
            elif isinstance(prediction_horizon, (float, int, long)):
                pass
            else:
                raise ValueError('Format for prediction horizon ' + str(type(prediction_horizon)) + ' not understood.')
        # set control horizon
        if control_horizon is None and control_horizon_backup is None:
            control_horizon_steps = 1
        else:
            if control_horizon is not None:
                control_horizon_steps = int(np.floor(control_horizon / float(stepsize)))
            elif control_horizon_backup is not None:
                control_horizon_steps = int(np.floor(control_horizon_backup / float(stepsize))) - 2
        if prediction_horizon is None:
            raise ValueError('Could not determine prediction horizon')
        else:
            pred_steps = int(np.ceil(prediction_horizon / float(stepsize)))
        if control_horizon_steps <= 1:
            control_horizon_steps = 1
        self.preds = [[float(pred_steps) * stepsize, float(control_horizon_steps) * stepsize]]
        finished = False
        time_now = 0
        res_species = np.array(external_conditions)
        # if not self.reduced:
        res_species = np.append(res_species, np.zeros(self.numbers['metab_species']))
        res_species = np.atleast_2d(np.append(res_species, np.dot(self.scale, initial_biomass)))
        error = False
        # initial generation of collocation and save first set of result trajectory
        initial_biomass = np.array(
            [self.scale[i] * x if not np.isscalar(self.scale) else self.scale * x for i, x in enumerate(initial_biomass)])
        if midpoint:
            optimizer_tool = midopt.MidOpt(self.R, self.scaled_S, np.concatenate(
                (external_conditions, np.zeros(self.numbers['metab_species']), np.array(initial_biomass))), self.q)
        else:
            optimizer_tool = linopt.LinOpt(self.R, self.scaled_S, np.concatenate(
                (external_conditions, np.zeros(self.numbers['metab_species']), np.array(initial_biomass))), self.q)
        # set the objective \int_0^Tend b^T P dt
        optimizer_tool.set_objective(-self.objective_vector, np.zeros(len(self.reactions)), 0, np.zeros(len(self.objective_vector)))
        # construct Enzyme Capacity Constraint + Biomass Composition Constraint + Positivity constraints + Maintenance
        Gx_1 = -np.eye(len(self.species))  # all species are bound to be positive -x \leq 0
        Gu_1 = np.zeros((len(self.species), len(self.reactions)))
        Gx_2 = np.zeros((len(self.reactions) - len(self.reversible), Gx_1.shape[1]))  # constrain only irreversible reactions
        Gu_2 = np.zeros((len(self.reactions) - len(self.reversible), len(self.reactions)))
        counter = 0
        for coloumn, reaction in enumerate(self.reactions):
            if reaction not in self.reversible:
                Gu_2[counter, self.reactions.index(reaction)] = -1
                counter += 1
        # check if constraints are in the model
        # first biomass composition
        if self.HE.any():
            Gx_HE = -self.HE
            Gu_HC = self.scaled_HC
            if midpoint:
                Gx = np.vstack((Gx_HE, Gx_2))
                Gu = np.vstack((Gu_HC, Gu_2))
                optimizer_tool.add_state_constraint(Gx_1, np.zeros(Gx_1.shape[0]))
            else:
                Gx = np.vstack((Gx_HE, Gx_1, Gx_2))
                Gu = np.vstack((Gu_HC, Gu_1, Gu_2))
        elif midpoint:
            Gx = np.vstack((Gx_2))
            Gu = np.vstack((Gu_2))
            optimizer_tool.add_state_constraint(Gx_1, np.zeros(Gx_1.shape[0]))
        else:
            Gx = np.vstack((Gx_1, Gx_2))
            Gu = np.vstack((Gu_1, Gu_2))
        if self.HB.any():
            Gx_HB = self.HB
            Gx = np.vstack((Gx, Gx_HB))
            Gu_HB = np.zeros((self.HB.shape[0], len(self.reactions)))
            Gu = np.vstack((Gu, Gu_HB))
        if self.HM.any():
            Gx_5 = np.hstack((np.zeros((self.HM.shape[0], self.numbers['ext_species'] + self.numbers['metab_species'])),
                              np.dot(np.eye(self.HM.shape[0]), np.atleast_2d(self.biomass[-self.numbers['proteins']:]))))
            Gx = np.vstack((Gx, Gx_5))
            Gu_5 = -self.scaled_HM
            Gu = np.vstack((Gu, Gu_5))
        Sx = self.Sx
        optimizer_tool.add_control_constraint(Sx, np.zeros(Sx.shape[0]))
        # no Gk constraints (constant)
        Gk = np.zeros(Gx.shape[0])
        # add path constraints to linear program
        optimizer_tool.add_path_constraint(Gx, Gu, Gk)
        # set solver parameters
        optimizer_tool.set_solver(solver)
        # set solver parameters
        optimizer_tool.set_parameters(timesteps=pred_steps)
        optimizer_tool.set_parameters(finaltime=prediction_horizon)
        optimizer_tool.set_parameters(fixedterm=True)
        # optimizer_tool.set_parameters(ScaleFlag=2)
        try:
            new_params = merge_dicts({'keep_matrices': True}, params)
            temp_res = optimizer_tool.solve(**new_params)
        except ImportError:
            optimizer_tool.set_solver('cvxopt')
            solver = 'cvxopt'
            temp_res = optimizer_tool.solve()
        time_now += stepsize
        states = temp_res[1].get_state(stepsize)
        actual_biomass = states[-self.numbers['proteins']:]
        actual_ext_conditions = states[:self.numbers['ext_species']]
        res_species = np.append(res_species, np.atleast_2d(np.array(states)), axis=0)
        res_fluxes = np.array(np.atleast_2d(np.array(temp_res[1].get_control(0))))
        for i in range(control_horizon_steps - 2):
            time_now += stepsize
            states = temp_res[1].get_state((i + 2) * stepsize)
            actual_biomass = states[-self.numbers['proteins']:]
            actual_ext_conditions = states[:self.numbers['ext_species']]
            res_species = np.append(res_species, np.atleast_2d(np.array(states)), axis=0)
            res_fluxes = np.append(res_fluxes, np.atleast_2d(np.array(temp_res[1].get_control((i + 1) * stepsize))), axis=0)
        if time_now >= Tend:
            finished = True
        if fixed_horizons is True:
            try:
                while time_now < Tend:
                    new_initial_values = states
                    new_initial_values = np.array([np.abs(value) if index < self.numbers['ext_species'] or
                                                                    index >= self.numbers['ext_species'] + self.numbers['metab_species']
                                                   else 0
                                                   for index, value in enumerate(new_initial_values)])
                    temp_res = optimizer_tool.solve_iteration(new_initial_values)
                    self.preds.append([float(pred_steps) * stepsize, float(control_horizon_steps) * stepsize])
                    for i in range(control_horizon_steps):
                        time_now += stepsize
                        states = temp_res[1].get_state((i + 1) * stepsize)
                        actual_biomass = states[-self.numbers['proteins']:]
                        actual_ext_conditions = states[:self.numbers['ext_species']]
                        res_species = np.append(res_species, np.atleast_2d(np.array(states)), axis=0)
                        res_fluxes = np.append(res_fluxes, np.atleast_2d(np.array(temp_res[1].get_control(i * stepsize))), axis=0)
                        if time_now >= Tend:
                            break
            except:
                print 'An unknown error occurred in the optimization. Results so far written to error_st_species and error_st_fluxes'
                error = True
                times = [i * stepsize for i in xrange(res_species.shape[0])]
                export_states(times, res_species, self.species, self.scale, self.numbers['ext_species'] + self.numbers['metab_species'],
                              file_name='error_st_species ')
                export_fluxes(times, res_fluxes, self.reactions, self.scale,
                              self.numbers['metab_reactions'] + self.numbers['exch_reactions'],
                              file_name='error_st_fluxes')
                self.results = [times, res_species, res_fluxes]
                # end sdeFBA - loop
        else:
            while not finished:
                new_initial_values = states
                old_pred_steps = pred_steps
                new_pred, new_control = self.calcPredictionHorizon(solver=solver, biomass_comp=actual_biomass,
                                                                   environment=actual_ext_conditions)
                # new_pred = 1.5 * new_pred
                pred_steps = int(np.floor(new_pred / float(stepsize)))
                if pred_steps != old_pred_steps:
                    if control_horizon is None:
                        control_horizon_steps = int(np.floor(new_control / float(stepsize))) - 2
                        # if control_horizon_steps > 0.2 * pred_steps:
                        #     control_horizon_steps = int(np.floor(0.2 * pred_steps))
                        if control_horizon_steps <= 1:
                            control_horizon_steps = 1
                    self.preds.append([float(pred_steps) * stepsize, float(control_horizon_steps) * stepsize])
                    if midpoint:
                        optimizer_tool = midopt.MidOpt(self.R, self.scaled_S, new_initial_values, self.q)
                    else:
                        optimizer_tool = linopt.LinOpt(self.R, self.scaled_S, new_initial_values, self.q)
                    # set the objective \int_0^Tend b^T P dt
                    optimizer_tool.set_objective(-self.objective_vector, np.zeros(len(self.reactions)), 0,
                                                 np.zeros(len(self.objective_vector)))
                    # construct Enzyme Capacity Constraint + Biomass Composition Constraint + Positivity constraints + Maintenance
                    Gx_1 = -np.eye(len(self.species))  # all species are bound to be positive -x \leq 0
                    Gu_1 = np.zeros((len(self.species), len(self.reactions)))
                    Gx_2 = np.zeros((len(self.reactions) - len(self.reversible), Gx_1.shape[1]))  # constrain only irreversible reactions
                    Gu_2 = np.zeros((len(self.reactions) - len(self.reversible), len(self.reactions)))
                    counter = 0
                    for coloumn, reaction in enumerate(self.reactions):
                        if reaction not in self.reversible:
                            Gu_2[counter, self.reactions.index(reaction)] = -1
                            counter += 1
                    # check if constraints are in the model
                    # first biomass composition
                    if self.HE.any():
                        Gx_HE = -self.HE
                        Gu_HC = self.scaled_HC
                        if midpoint:
                            Gx = np.vstack((Gx_HE, Gx_2))
                            Gu = np.vstack((Gu_HC, Gu_2))
                            optimizer_tool.add_state_constraint(Gx_1, np.zeros(Gx_1.shape[0]))
                        else:
                            Gx = np.vstack((Gx_HE, Gx_1, Gx_2))
                            Gu = np.vstack((Gu_HC, Gu_1, Gu_2))
                    elif midpoint:
                        Gx = np.vstack((Gx_2))
                        Gu = np.vstack((Gu_2))
                        optimizer_tool.add_state_constraint(Gx_1, np.zeros(Gx_1.shape[0]))
                    else:
                        Gx = np.vstack((Gx_1, Gx_2))
                        Gu = np.vstack((Gu_1, Gu_2))
                    if self.HB.any():
                        Gx_HB = self.HB
                        Gx = np.vstack((Gx, Gx_HB))
                        Gu_HB = np.zeros((self.HB.shape[0], len(self.reactions)))
                        Gu = np.vstack((Gu, Gu_HB))
                    if self.HM.any():
                        Gx_5 = np.hstack((np.zeros((self.HM.shape[0], self.numbers['ext_species'] + self.numbers['metab_species'])),
                                          np.dot(np.eye(self.HM.shape[0]), np.atleast_2d(self.biomass[-self.numbers['proteins']:]))))
                        Gx = np.vstack((Gx, Gx_5))
                        Gu_5 = -self.scaled_HM
                        Gu = np.vstack((Gu, Gu_5))
                    Sx = self.Sx
                    optimizer_tool.add_control_constraint(Sx, np.zeros(self.Sx.shape[0]))
                    # no Gk constraints (constant)
                    Gk = np.zeros(Gx.shape[0])
                    # add path constraints to linear program
                    optimizer_tool.add_path_constraint(Gx, Gu, Gk)
                    # set solver parameters
                    optimizer_tool.set_solver(solver)
                    optimizer_tool.set_parameters(timesteps=pred_steps)
                    optimizer_tool.set_parameters(finaltime=new_pred)
                    optimizer_tool.set_parameters(fixedterm=True)
                    # optimizer_tool.set_parameters(ScaleFlag=2)
                    # temp_res = optimizer_tool.solve()
                    temp_res = optimizer_tool.solve(**new_params)
                else:
                    self.preds.append([float(pred_steps) * stepsize, float(control_horizon_steps) * stepsize])
                    temp_res = optimizer_tool.solve_iteration(new_initial_values)
                for i in range(control_horizon_steps + 1):
                    time_now += stepsize
                    states = temp_res[1].get_state((i + 1) * stepsize)
                    actual_biomass = states[-self.numbers['proteins']:]
                    actual_ext_conditions = states[:self.numbers['ext_species']]
                    res_species = np.append(res_species, np.atleast_2d(np.array(states)), axis=0)
                    res_fluxes = np.append(res_fluxes, np.atleast_2d(np.array(temp_res[1].get_control(i * stepsize))), axis=0)
                if time_now >= Tend:
                    break
        times = [i * stepsize for i in xrange(steps + 1)]
        # write results to external txt files
        if export and not error:
            export_states(times, res_species, self.species, self.scale, self.numbers['ext_species'] + self.numbers['metab_species'],
                          file_name=output_name + '_st_species')
            export_fluxes(times[:-1], res_fluxes, self.reactions, self.scale,
                          self.numbers['metab_reactions'] + self.numbers['exch_reactions'],
                          file_name=output_name + '_st_fluxes')
        self.results = [times, res_species, res_fluxes]
        return times, res_species, res_fluxes

    def __bisec_cplex(self, model, eps, low, high, initial_biomass_amount, down=True):
        """
        bisection method utilizing cplex. Should not be called from outside.
        """
        if down:
            new_mu = (high - low) / 2.0 + low
        else:
            new_mu = high
        for i in range(self.numbers['proteins']):
            model.linear_constraints.delete('delete_me_' + str(i))
        Sp = csr_matrix(self.scaled_S[-self.numbers['proteins']:, :])
        for i in range(Sp.shape[0]):
            start1 = Sp.indptr[i]
            end1 = Sp.indptr[i + 1]
            vari = [int(j) for j in Sp.indices[start1:end1]]
            coef = list(Sp.data[start1:end1])
            vari.append(i + len(self.reactions))
            coef.append(float(-new_mu))
            model.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=vari, val=coef)], senses="E", rhs=[0.0],
                                         names=['delete_me_' + str(i)])
        model.solve()
        if model.solution.status[model.solution.get_status()] == 'optimal':
            if np.abs(new_mu - high) < eps:
                print 'RBA predict a growth rate mu = ' + str(new_mu)
                x = np.array(model.solution.get_values()).ravel()
                initial_biomass = x[-self.numbers['proteins']:]
                initial_fluxes = x[:len(self.reactions)]
                for entry, flux in enumerate(initial_fluxes):
                    if entry > self.numbers['exch_reactions'] + self.numbers['metab_reactions']:
                        initial_fluxes[entry] = flux / self.scale
                initial_biomass = np.array([i / self.scale if i > 0 else 0.0 for i in initial_biomass])
                return initial_biomass, initial_fluxes, new_mu
            else:
                return self.__bisec_cplex(model, eps, new_mu, new_mu + (high - new_mu) / 2.0, initial_biomass_amount, down=False)
        else:
            try:
                return self.__bisec_cplex(model, eps, low, new_mu, initial_biomass_amount, down=True)
            except RuntimeError as e:
                print 'bisection encountered a problem:'
                print 'assuming system can not grow'
                return np.zeros(self.numbers['proteins']), np.zeros(len(self.reactions)), 0.0

    def __bisec_soplex(self, eps, low, high, initial_biomass_amount, down=True):
        # def __bisec_soplex(self, model, eps, low, high, initial_biomass_amount, down=True):
        """
        bisection method utilizing cplex. Should not be called from outside.
        """
        if down:
            new_mu = (high - low) / 2.0 + low
        else:
            new_mu = high
        # sol_reac, sol_prot = model.data
        # for i in sol_reac:
        #     model.releaseVar(sol_reac[i])
        # for i in sol_prot:
        #     model.releaseVar(sol_prot[i])
        # model.freeTransform()
        # cons = model.getConss()
        # print 'yes'
        # for i in range(len(cons)):
        #     if 'mu_' in cons[i].name:
        #         model.delCons(cons[i])
        # ##########
        model = soplex.Model()
        model.setParam('numerics/feastol', eps)
        model.setParam('reoptimization/enable', True)
        sol_reac = {}
        for i, reac in enumerate(self.reactions):
            if reac in self.reversible:
                sol_reac[i] = model.addVar(lb=None, ub=None, vtype="C", name=reac)
            else:
                sol_reac[i] = model.addVar(lb=0, ub=None, vtype="C", name=reac)
        sol_prot = {}
        for i, prot in enumerate(self.species[-self.numbers['proteins']:]):
            sol_prot[i] = model.addVar(lb=0, ub=None, vtype="C", name=prot)
        biomass = soplex.quicksum([sol_prot[j] * self.biomass[-self.numbers['proteins'] + j] for j in range(self.numbers['proteins'])])
        # set qss constraints \dot{x}=Sx * V = 0
        Sx = csr_matrix(self.Sx)
        for i in range(Sx.shape[0]):
            start1 = Sx.indptr[i]
            end1 = Sx.indptr[i + 1]
            variables = [np.asscalar(j) for j in Sx.indices[start1:end1]]
            coeff = list(Sx.data[start1:end1])
            model.addCons(soplex.quicksum(sol_reac[variables[j]] * coeff[j] for j in range(len(variables))) == 0.0, "qss_%s" % i)
        # Enzyme capacity constraints
        HC = csr_matrix(self.scaled_HC)
        HE = csr_matrix(self.HE[:, -self.numbers['proteins']:])
        for i in range(HC.shape[0]):
            start_reac = HC.indptr[i]
            end_reac = HC.indptr[i + 1]
            variables_reac = [np.asscalar(j) for j in HC.indices[start_reac:end_reac]]
            coeff_reac = list(HC.data[start_reac:end_reac])
            start_prot = HE.indptr[i]
            end_prot = HE.indptr[i + 1]
            variables_prot = [j for j in HE.indices[start_prot:end_prot]]
            coeff_prot = list(-1 * HE.data[start_prot:end_prot])
            # var = variables + variables2
            # coe = coeff + coeff2
            model.addCons(soplex.quicksum(
                [soplex.quicksum(sol_reac[variables_reac[j]] * coeff_reac[j] for j in range(len(variables_reac))),
                 soplex.quicksum(sol_prot[variables_prot[j]] * coeff_prot[j] for j in range(len(variables_prot)))]) <= 0.0, "ecc_%s" % i)
        # add biomass composition constraints HB * P <= 0
        if self.HB.any():
            HB = csr_matrix(self.HB[:, -self.numbers['proteins']:])
            for i in range(HB.shape[0]):
                start1 = HB.indptr[i]
                end1 = HB.indptr[i + 1]
                variables = [j for j in HB.indices[start1:end1]]
                coeff = list(HB.data[start1:end1])
                model.addCons(soplex.quicksum(sol_prot[variables[j]] * coeff[j] for j in range(len(variables))) <= 0.0, "bcc_%s" % i)

        # add maintenance constraint HM * V \geq b^T p
        if self.HM.any():
            HM = csr_matrix(self.scaled_HM)
            for i in range(HM.shape[0]):
                start1 = HM.indptr[i]
                end1 = HM.indptr[i + 1]
                variables = np.array([j for j in HM.indices[start1:end1]])
                coeff = np.array(list(-1 * (HM.data[start1:end1])))
                model.addCons(soplex.quicksum(
                    [soplex.quicksum(sol_reac[variables[j]] * coeff[j] for j in range(len(variables))), biomass]) <= 0.0, "mc_%s" % i)
        Sp = csr_matrix(self.scaled_S[-self.numbers['proteins']:, :])
        for i in range(Sp.shape[0]):
            start1 = Sp.indptr[i]
            end1 = Sp.indptr[i + 1]
            vari = [int(j) for j in Sp.indices[start1:end1]]
            coef = list(Sp.data[start1:end1])
            model.addCons(soplex.quicksum([
                soplex.quicksum(sol_reac[vari[j]] * coef[j] for j in range(len(vari))),
                -new_mu * sol_prot[i]]) == 0.0, "mu_%s" % i)

        # add external conditions constraints
        VMAX = None
        VMIN = None
        for number, reaction in enumerate(self.reactions[:self.numbers['exch_reactions']]):
            if reaction in self.uptake:
                pos_constr = False
                neg_constr = False
                for elem in self.uptake[reaction]:
                    index = self.species.index(elem[0])
                    if ext_conditions[index] <= self.eps:
                        if not pos_constr:
                            if elem[1] == 1 and reaction in self.reversible:
                                if VMIN is None:
                                    VMIN = np.zeros((1, len(self.reactions)))
                                    VMIN[0, number] = -1
                                else:
                                    temp = np.zeros((1, len(self.reactions)))
                                    temp[0, number] = -1
                                    VMIN = np.vstack((VMIN, temp))
                        if not neg_constr:
                            if elem[1] == -1:
                                if VMAX is None:
                                    VMAX = np.zeros((1, len(self.reactions)))
                                    VMAX[0, number] = 1
                                else:
                                    temp = np.zeros((1, len(self.reactions)))
                                    temp[0, number] = 1
                                    VMAX = np.vstack((VMAX, temp))
        if VMAX is not None:
            VMAX = csr_matrix(VMAX)
            for i in range(VMAX.shape[0]):
                VMAX = csr_matrix(VMAX[:, -self.numbers['proteins']:])
                for i in range(VMAX.shape[0]):
                    start1 = VMAX.indptr[i]
                    end1 = VMAX.indptr[i + 1]
                    variables = [j for j in VMAX.indices[start1:end1]]
                    coeff = list(VMAX.data[start1:end1])
                    model.addCons(soplex.quicksum(sol_prot[variables[j]] * coeff[j] for j in range(len(variables))) <= 0.0,
                                  "VMAX_%s" % i)
        if VMIN is not None:
            VMIN = csr_matrix(VMIN)
            for i in range(VMIN.shape[0]):
                VMIN = csr_matrix(VMIN[:, -self.numbers['proteins']:])
                for i in range(VMIN.shape[0]):
                    start1 = VMIN.indptr[i]
                    end1 = VMIN.indptr[i + 1]
                    variables = [j for j in VMIN.indices[start1:end1]]
                    coeff = list(VMIN.data[start1:end1])
                    model.addCons(soplex.quicksum(sol_prot[variables[j]] * coeff[j] for j in range(len(variables))) <= 0.0,
                                  "VMIN_%s" % i)

        model.optimize()
        status = model.getStatus()
        # del model
        if status == 'optimal':
            if np.abs(new_mu - high) < eps:
                print 'RBA predict a growth rate mu = ' + str(new_mu)
                initial_fluxes = np.array([model.getVal(sol_reac[j]) for j in sol_reac]).ravel()
                initial_biomass = np.array([model.getVal(sol_prot[j]) for j in sol_prot]).ravel()
                for entry, flux in enumerate(initial_fluxes):
                    if entry > self.numbers['exch_reactions'] + self.numbers['metab_reactions']:
                        initial_fluxes[entry] = flux / self.scale
                initial_biomass = np.array([i / self.scale if i > 0 else 0.0 for i in initial_biomass])
                return initial_biomass, initial_fluxes, new_mu
            else:
                return self.__bisec_soplex(eps, new_mu, new_mu + (high - new_mu) / 2.0, initial_biomass_amount, down=False)
        else:
            try:
                return self.__bisec_soplex(eps, low, new_mu, initial_biomass_amount, down=True)
            except RuntimeError as e:
                print 'bisection encountered a problem:'
                print 'assuming system can not grow'
                return np.zeros(self.numbers['proteins']), np.zeros(len(self.reactions)), 0.0

    def __bisec_cvxopt(self, mu, eps, low, high, initial_biomass_amount, ext_conditions, down=True):
        """
        bisection method utilizing cvxopt. Should not be called from outside.
        """
        # if not self.reduced:
        # G*X \leq h; X = [V, P]^T
        G1 = np.hstack((self.scaled_HC, -self.HE[:, -self.numbers['proteins']:]))  # HC constraints
        G2 = np.hstack((np.zeros((self.numbers['proteins'], len(self.reactions))),
                        -np.eye(self.numbers['proteins'])))  # positivity constraints for proteins
        G3 = np.zeros((len(self.reactions) - len(self.reversible),
                       len(self.reactions) + self.numbers['proteins']))  # flux reversibility constraints
        counter = 0
        for number, reaction in enumerate(self.reactions):
            if reaction not in self.reversible:
                G3[counter, number] = -1
                counter += 1
        if self.HB.any():
            G4 = np.hstack((np.zeros((self.HB.shape[0], len(self.reactions))), self.HB[:, -self.numbers['proteins']:]))
            G = np.vstack((G1, G2, G3, G4))
        else:
            G = np.vstack((G1, G2, G3))
        if self.HM.any():
            G5 = np.hstack((-self.HM, np.dot(np.eye(self.HM.shape[0]), np.atleast_2d(self.biomass[-self.numbers['proteins']:]))))
            G = np.vstack((G, G5))

        # add external constraints
        h = np.zeros(G.shape[0])
        h1 = []
        for index, reac_name in enumerate(self.reactions[:self.numbers['exch_reactions']]):
            coloumn = self.stoich[:self.numbers['ext_species'], index]
            temp = np.zeros(G.shape[1])
            if (coloumn > 0).any() and not (coloumn < 0).any():
                if reac_name in self.reversible:
                    temp[index] = -1
                    h1.append(ext_conditions[np.nonzero(coloumn)[0][0]])
                    G = np.vstack((G, temp))
            elif not (coloumn > 0).any() and (coloumn < 0).any():
                temp[index] = 1
                h1.append(ext_conditions[np.nonzero(coloumn)[0][0]])
                G = np.vstack((G, temp))
        h = np.concatenate((h, h1))
        # A*X = b
        A1 = np.hstack((self.Sx, np.zeros((self.Sx.shape[0], self.numbers['proteins']))))
        A2 = np.hstack((np.zeros((1, len(self.reactions))), np.atleast_2d(self.biomass[-self.numbers['proteins']:])))
        A3 = np.hstack((self.scaled_S[-self.numbers['proteins']:, :], -np.dot(mu, np.eye(self.numbers['proteins']))))
        A = np.vstack((A1, A2, A3))
        b = np.zeros(A.shape[0])
        b[A1.shape[0]] = self.scale * initial_biomass_amount
        c = np.zeros(G.shape[1])
        # Options for solver
        cvxopt.solvers.options['show_progress'] = False
        try:
            sol = cvxopt.solvers.lp(cvxopt.matrix(c), cvxopt.matrix(G), cvxopt.matrix(h), cvxopt.matrix(A), cvxopt.matrix(b))
        except ValueError:
            return self.__bisec_cvxopt(mu - (mu - low) / 2.0, eps, low, mu, initial_biomass_amount, ext_conditions)
        # if failed
        if sol['status'] == "dual infeasible" or sol['status'] == "unknown" or sol['status'] == "primal infeasible":
            return self.__bisec_cvxopt(mu - (mu - low) / 2.0, eps, low, mu, initial_biomass_amount, ext_conditions)
        else:
            x = np.array(sol['x']).ravel()
            if np.abs(mu - high) <= eps:
                print 'RBA predict a growth rate mu = ' + str(mu)
                return x, mu
            if down:
                return self.__bisec_cvxopt(mu + (high - mu) / 2.0, eps, mu, high, initial_biomass_amount, ext_conditions)
            else:
                return self.__bisec_cvxopt(2 * mu, eps, mu, 2 * high, initial_biomass_amount, ext_conditions, down=False)

    def __bisec_gurobi(self, model, mu, eps, low, high, fluxes, proteins, down=True):
        """
        Bisection method implemented in Gurobi. Should not be called from outside.
        """
        for i in range(self.numbers['proteins']):
            con = model.getConstrByName('change_me_' + str(i))
            model.remove(con)
            model.update()
        # if self.reduced:
        #     Spp = csr_matrix(self.red_S[-self.numbers['proteins']:, :])
        # else:
        Spp = csr_matrix(self.scaled_S[-self.numbers['proteins']:, :])
        for i in range(Spp.shape[0]):
            start1 = Spp.indptr[i]
            end1 = Spp.indptr[i + 1]
            variables = [fluxes[j] for j in Spp.indices[start1:end1]]
            coeff = Spp.data[start1:end1]
            model.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.EQUAL, rhs=mu * proteins[i], name="change_me_" + str(i))
        model.setParam('ScaleFlag', 0)
        model.setParam('NumericFocus', 3)
        model.update()
        model.optimize()
        if down:
            if model.status == 2:
                if mu - low <= eps:
                    return model, mu
                else:
                    return self.__bisec_gurobi(model, mu + (high - mu) / 2.0, eps, mu, high, fluxes, proteins)
            else:
                return self.__bisec_gurobi(model, mu - (mu - low) / 2.0, eps, low, mu, fluxes, proteins)
        else:
            if model.status == 2:
                if mu - high <= eps:
                    print 'RBA predict a growth rate mu = ' + str(mu)
                    return model, mu
                else:
                    return self.__bisec_gurobi(model, 2 * mu, eps, mu, 3 * mu, fluxes, proteins, down=False)
            elif model.status == 4:
                if mu - low <= eps:
                    print 'System is not solvable'
                    raise ValueError('System is not solvable')
                return self.__bisec_gurobi(model, 2 * mu, eps, mu, 3 * mu, fluxes, proteins, down=False)
            else:
                if mu - low <= eps:
                    print 'System is not solvable'
                    raise ValueError('System is not solvable')
                return self.__bisec_gurobi(model, mu - (mu - low) / 2.0, eps, low, mu, fluxes, proteins)

# def createRobustSystemMatrices(self, uncertain_values):
    #     number_of_scenarios = 1
    #     descriptor = {}
    #     # Sort uncertainties by type
    #     weight_param = {}
    #     objective_param = {}
    #     ob_weight_param = {}
    #     kcat_param = {}
    #     main_param = {}
    #     biomp_param = {}
    #     env_param = {}
    #     parameter_list = self.parameter_list
    #     for param_name in uncertain_values:
    #         if parameter_list[param_name][-1] == 'weight':
    #             weight_param[param_name] = parameter_list[param_name]
    #         elif parameter_list[param_name][-1] == 'objective':
    #             objective_param[param_name] = parameter_list[param_name]
    #         elif parameter_list[param_name][-1] == 'ob_weight':
    #             ob_weight_param[param_name] = parameter_list[param_name]
    #         elif parameter_list[param_name][-1] == 'kcat':
    #             kcat_param[param_name] = parameter_list[param_name]
    #         elif parameter_list[param_name][-1] == 'maintenance':
    #             main_param[param_name] = parameter_list[param_name]
    #         elif parameter_list[param_name][-1] == 'biom_percent':
    #             biomp_param[param_name] = parameter_list[param_name]
    #         elif parameter_list[param_name][-1] == 'environment':
    #             env_param[param_name] = parameter_list[param_name]
    #     # construct the scenarios by creating all possible parameter combinations
    #     for values in uncertain_values.itervalues():
    #         number_of_scenarios = number_of_scenarios * len(values)
    #     scenarios = {i: {} for i in range(number_of_scenarios)}
    #     block_length = 1
    #     for uncertainty in uncertain_values:
    #         step_counter = 0
    #         value_counter = 0
    #         list_with_uncertain_values = uncertain_values[uncertainty]
    #         for scenario_number in range(number_of_scenarios):
    #             scenarios[scenario_number][uncertainty] = list_with_uncertain_values[value_counter]
    #             step_counter += 1
    #             if step_counter == block_length:
    #                 step_counter = 0
    #                 value_counter += 1
    #                 if value_counter == len(list_with_uncertain_values):
    #                     value_counter = 0
    #         block_length = block_length * len(list_with_uncertain_values)
    #     # positivity
    #     Gx_positivity = -spr.eye(len(self.species) * number_of_scenarios)
    #     Gu_positivity = np.zeros((len(self.species) * number_of_scenarios, len(self.reactions) * number_of_scenarios))
    #     Gk_positivity = np.zeros(len(self.species) * number_of_scenarios)
    #     descriptor['positivity'] = [0, len(self.species) * number_of_scenarios]
    #     # reversibility
    #     temp = np.zeros((len(self.reactions) - len(self.reversible), len(self.reactions)))
    #     counter = 0
    #     for coloumn, reaction in enumerate(self.reactions):
    #         if reaction not in self.reversible:
    #             temp[counter, self.reactions.index(reaction)] = -1
    #             counter += 1
    #     temp = spr.csr_matrix(temp)
    #     Gu_reversibility = spr.block_diag([temp for i in range(number_of_scenarios)])
    #     Gx_reversibility = np.zeros((Gu_reversibility.shape[0], len(self.species) * number_of_scenarios))
    #     Gk_reversibility = np.zeros(Gu_reversibility.shape[0])
    #     # Biomass Composition Constraint
    #     Gx_final = np.vstack((Gx_positivity.todense(), Gx_reversibility))
    #     Gu_final = np.vstack((Gu_positivity, Gu_reversibility.todense()))
    #     Gk_final = np.concatenate((Gk_positivity, Gk_reversibility))
    #     descriptor['reversibility'] = [len(self.species) * number_of_scenarios, len(self.species) * number_of_scenarios
    #                                    + counter * number_of_scenarios]
    #     down = len(self.species) * number_of_scenarios + counter * number_of_scenarios
    #     if self.HB.any():
    #         HB_collection = [self.HB.copy() for i in range(number_of_scenarios)]
    #         if weight_param or ob_weight_param:
    #             for weight_name in weight_param:
    #                 for param_entry_number, maybe_obj_position in enumerate(weight_param[weight_name][0]):
    #                     if 'HB[' in maybe_obj_position:
    #                         obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
    #                         obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
    #                         for number in range(number_of_scenarios):
    #                             HB_collection[number][obj_position_row, obj_position_coloumn] = (scenarios[number][weight_name] ** (
    #                                 weight_param[weight_name][2][param_entry_number])) * weight_param[weight_name][3][param_entry_number]
    #             for weight_name in ob_weight_param:
    #                 for param_entry_number, maybe_obj_position in enumerate(ob_weight_param[weight_name][0]):
    #                     if 'HB[' in maybe_obj_position:
    #                         obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
    #                         obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
    #                         for number in range(number_of_scenarios):
    #                             HB_collection[number][obj_position_row, obj_position_coloumn] = (scenarios[number][weight_name] ** (
    #                                 ob_weight_param[weight_name][2][param_entry_number])) * ob_weight_param[weight_name][3][
    #                                                                                                 param_entry_number]
    #         if biomp_param:
    #             for biomp_name in biomp_param:
    #                 for param_entry_number, maybe_obj_position in enumerate(biomp_param[biomp_name][0]):
    #                     if 'HB[' in maybe_obj_position:
    #                         obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
    #                         obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
    #                         for number in range(number_of_scenarios):
    #                             if biomp_param[biomp_name][3][param_entry_number] == -1:
    #                                 HB_collection[number][obj_position_row, obj_position_coloumn] *= (scenarios[number][biomp_name] - 1) / (
    #                                     biomp_param[biomp_name][1] - 1)
    #                             else:
    #                                 HB_collection[number][obj_position_row, obj_position_coloumn] *= scenarios[number][biomp_name] / \
    #                                                                                                  biomp_param[biomp_name][1]
    #         Gx_biomass = spr.block_diag(HB_collection)
    #         Gu_biomass = np.zeros((Gx_biomass.shape[0], number_of_scenarios * len(self.reactions)))
    #         Gk_biomass = np.zeros(Gx_biomass.shape[0])
    #         Gx_final = np.vstack((Gx_final, Gx_biomass.todense()))
    #         Gu_final = np.vstack((Gu_final, Gu_biomass))
    #         Gk_final = np.concatenate((Gk_final, Gk_biomass))
    #         descriptor['HB'] = [down, down + Gx_biomass.shape[0]]
    #         down += Gx_biomass.shape[0]
    #     if self.HC.any():
    #         HC_collection = [self.scaled_HC.copy() for i in range(number_of_scenarios)]
    #         HE_collection = [-self.HE.copy() for i in range(number_of_scenarios)]
    #         if kcat_param:
    #             for kcat_name in kcat_param:
    #                 for param_entry_number, maybe_obj_position in enumerate(kcat_param[kcat_name][0]):
    #                     if 'HC[' in maybe_obj_position:
    #                         obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
    #                         obj_position_coloumn = int(
    #                             maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
    #                         for number in range(number_of_scenarios):
    #                             HC_collection[number][obj_position_row, obj_position_coloumn] = (scenarios[number][kcat_name] **
    #                                                                                              kcat_param[kcat_name][2][
    #                                                                                                  param_entry_number]) * \
    #                                                                                             kcat_param[kcat_name][3][
    #                                                                                                 param_entry_number]
    #         if env_param:
    #             for env_name in env_param:
    #                 for env_entry_number, maybe_env_position in enumerate(env_param[env_name][0]):
    #                     if 'HC[' in maybe_env_position:
    #                         env_position_row = int(maybe_env_position[3:maybe_env_position.index(',')])
    #                         env_position_coloumn = int(
    #                             maybe_env_position[maybe_env_position.index(',') + 1:maybe_env_position.index(']')])
    #                         for number in range(number_of_scenarios):
    #                             if self.uncertain_environment[env_name][2] == 'decreasing':
    #                                 if scenarios[number][env_name] == 0:
    #                                     HC_collection[number][env_position_row, env_position_coloumn] = 0.0
    #                                 elif 0 < scenarios[number][env_name] < 1:
    #                                     HC_collection[number][env_position_row, env_position_coloumn] = 1.0
    #                                 else:
    #                                     raise ValueError('The value for the degradation rate for the environment "' + env_name +
    #                                                      '" must be between 0 and 1.')
    #                             elif self.uncertain_environment[env_name][2] == 'increasing':
    #                                 if scenarios[number][env_name] == 0:
    #                                     HC_collection[number][env_position_row, env_position_coloumn] = 0.0
    #                                 else:
    #                                     HC_collection[number][env_position_row, env_position_coloumn] = 1.0
    #         Gx_ecc = spr.block_diag(HE_collection)
    #         Gu_ecc = spr.block_diag(HC_collection)
    #         Gk_ecc = np.zeros(Gx_ecc.shape[0])
    #         Gx_final = np.vstack((Gx_final, Gx_ecc.todense()))
    #         Gu_final = np.vstack((Gu_final, Gu_ecc.todense()))
    #         Gk_final = np.concatenate((Gk_final, Gk_ecc))
    #         descriptor['HC'] = [down, down + Gx_ecc.shape[0]]
    #         down += Gx_ecc.shape[0]
    #     if self.HM.any():
    #         HM_collection = [-self.scaled_HM.copy() for i in range(number_of_scenarios)]
    #         if main_param:
    #             for main_name in main_param:
    #                 for param_entry_number, maybe_obj_position in enumerate(main_param[main_name][0]):
    #                     if 'HM[' in maybe_obj_position:
    #                         obj_position_row = int(maybe_obj_position[3:maybe_obj_position.index(',')])
    #                         obj_position_coloumn = int(maybe_obj_position[maybe_obj_position.index(',') + 1:maybe_obj_position.index(']')])
    #                         for number in range(number_of_scenarios):
    #                             HM_collection[number][obj_position_row, obj_position_coloumn] = -(scenarios[number][main_name] **
    #                                                                                               main_param[main_name][2][
    #                                                                                                   param_entry_number]) * \
    #                                                                                             main_param[main_name][3][param_entry_number]
    #                             # not yet working part
    #                             #       if weight_param or ob_weight_param:
    #                             #           biomass_collection = biomass_matrix = np.array([self.biomass for i in range(self.HM.shape[0])])
    #                             #           for weight_name in weight_param:
    #         biomass_matrix = np.array([self.biomass for i in range(self.HM.shape[0])])
    #         Gx_main = spr.block_diag([biomass_matrix for i in range(number_of_scenarios)])
    #         Gu_main = spr.block_diag(HM_collection)
    #         Gk_main = np.zeros(Gu_main.shape[0])
    #         Gx_final = np.vstack((Gx_final, Gx_main.todense()))
    #         Gu_final = np.vstack((Gu_final, Gu_main.todense()))
    #         Gk_final = np.concatenate((Gk_final, Gk_main))
    #         descriptor['HM'] = [down, down + Gx_main.shape[0]]
    #         down += Gx_main.shape[0]
    #     # steady-state constraint
    #     Sx_collection = [
    #         self.scaled_S.copy()[self.numbers['ext_species']:self.numbers['ext_species'] + self.numbers['metab_species'], :]
    #         for i in range(number_of_scenarios)]
    #     Sx = spr.block_diag(Sx_collection)
    #     Hu_steady = spr.block_diag(Sx_collection)
    #     Hk_steady = np.zeros(Hu_steady.shape[0])
    #     return Gx_final, Gu_final, Gk_final, Sx.todense(), descriptor

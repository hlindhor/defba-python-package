##################################################################
# Copyright (C) 2018 Henning Lindhorst <henning.lindhorst@gmx.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
from __future__ import division

import numpy as np
from scipy import sparse
from scipy.sparse import csr_matrix

class Store(object):
    pass

globals = Store()


try:
    import cvxopt
    import cvxopt.solvers
    globals.have_cvxopt = True
except ImportError as err:
    globals.have_cvxopt = False
    globals.cvxopt_err = str(err)

try:
    import cplex
    globals.have_cplex = True
except ImportError as err:
    globals.have_cplex = False
    globals.cplex_err = str(err)

try:
    import gurobipy as grb
    globals.have_gurobi = True
except ImportError as err:
    globals.have_gurobi = False
    globals.gurobi_err = str(err)

try:
    import pyscipopt as soplex
    globals.have_soplex = True
except ImportError as err:
    globals.have_soplex = False
    globals.soplex_err = str(err)

def staticLP(c, b, d, Me, Mi, solver='cplex', params={}):
    """
    Solve the LP problem::

        min  c' x
        s.t. Me x  = b
             Mi x <= d

    Returns an optimal solution x and objective function value c' x.
    Raises OptimalityError if solver fails.

    Available solvers (defined by solver) with parameters:
    * 'cvxopt'
    * 'gurobi'
    * 'cplex'
    * 'soplex'
    """
    if solver == "cvxopt":
        sol = cvxopt.solvers.lp(cvxopt.matrix(c), cvxopt.matrix(Mi), cvxopt.matrix(d.ravel()), cvxopt.matrix(Me), cvxopt.matrix(b.ravel()))
        if sol['status'] == "dual infeasible":
            raise OptimalityError("Dual infeasibility encountered.")
        if sol['status'] == "primal infeasible":
            raise OptimalityError("Primal infeasibility encountered.")
        if sol['status'] == "unknown":
            raise OptimalityError("Optimizer returned with an unknown status.")
        x = np.array(sol['x']).ravel()
        return x, sol['primal objective']
    elif solver == "gurobi":
        try:
            m = grb.Model()
        except NameError:
            return staticLP(c, b, d, Me, Mi, solver='cvxopt')
        optdim = c.shape[0]
        optvar = [m.addVar(lb=-grb.GRB.INFINITY, obj=c[i]) for i in range(optdim)]
        m.update()
        # see http://stackoverflow.com/questions/22767608/sparse-matrix-lp-problems-in-gurobi-python for following code
        Me = csr_matrix(Me)
        for i in range(Me.shape[0]):
            start = Me.indptr[i]
            end   = Me.indptr[i+1]
            variables = [optvar[j] for j in Me.indices[start:end]]
            coeff     = Me.data[start:end]
            m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.EQUAL, rhs=b[i])
        Mi = csr_matrix(Mi)
        for i in range(Mi.shape[0]):
            start = Mi.indptr[i]
            end   = Mi.indptr[i+1]
            variables = [optvar[j] for j in Mi.indices[start:end]]
            coeff     = Mi.data[start:end]
            m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.LESS_EQUAL, rhs=d[i])
        m.optimize()
        if m.Status == grb.GRB.status.OPTIMAL:
            x = np.array([v.x for v in optvar])
            return x, m.getAttr('objVal')
        elif m.Status == grb.GRB.status.INFEASIBLE:
            raise OptimalityError("Primal infeasibility encountered.")
        else:
            raise OptimalityError("Gurobi returned with error status {:d}.".format(m.Status))
    elif solver == "cplex":
        model = cplex.Cplex()
        model.set_log_stream(None)
        model.set_error_stream(None)
        model.set_warning_stream(None)
        model.set_results_stream(None)
        model.parameters.read.scale = -1
        # try to populate by row
        model.objective.set_sense(model.objective.sense.minimize)
        model.variables.add(obj=c[:], lb=[-cplex.infinity for i in range(c.shape[0])])
        Me = csr_matrix(Me)
        for i in range(Me.shape[0]):
            start = Me.indptr[i]
            end   = Me.indptr[i+1]
            variables = [np.asscalar(j) for j in Me.indices[start:end]]
            coeff     = list(Me.data[start:end])
            coeff = [np.asscalar(obj) for obj in coeff]
            btemp = np.asscalar(b[i])
            model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="E", rhs=[btemp])
        Mi = csr_matrix(Mi)
        for i in range(Mi.shape[0]):
            start = Mi.indptr[i]
            end   = Mi.indptr[i+1]
            variables = [np.asscalar(j) for j in Mi.indices[start:end]]
            coeff     = list(Mi.data[start:end])
            coeff     = [np.asscalar(obj) for obj in coeff]
            dtemp = np.asscalar(d[i])
            model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="L", rhs=[dtemp])
        model.solve()
        if model.solution.status[model.solution.get_status()] == 'optimal':
            x = np.array(model.solution.get_values()).ravel()
            return x, model.solution.get_objective_value()
        # elif model.solution.get_status() == 5:
        #     print 'may contain infeasibilities'
        #     x = np.array(model.solution.get_values()).ravel()
        #     return x, model.solution.get_objective_value()
        elif model.solution.status[model.solution.get_status()] == 'infeasible':
            raise OptimalityError("Primal infeasibility encountered.")
        else:
            print model.solution.get_status()
            raise OptimalityError("Cplex returned with error status " + model.solution.status[model.solution.get_status()] + ".")
    elif solver == "soplex":
        model = soplex.Model('deFBA_LP')
        for par in params:
            model.setParam(par, params[par])
        if not 'numerics/feastol' in params:
            model.setParam('numerics/feastol', 1e-8)
            model.setParam('numerics/lpfeastol', 1e-11)
        x = {}
        for i, j in enumerate(c):
            x[i] = model.addVar(lb=None, ub=None, vtype="C", name='x_' + str(i), obj=j)
        Me = csr_matrix(Me)
        for i in range(Me.shape[0]):
            start = Me.indptr[i]
            end = Me.indptr[i + 1]
            variables = [np.asscalar(j) for j in Me.indices[start:end]]
            coeff = list(Me.data[start:end])
            coeff = [np.asscalar(obj) for obj in coeff]
            btemp = np.asscalar(b[i])
            model.addCons(soplex.quicksum(x[variables[j]] * coeff[j] for j in range(len(variables))) == btemp, "equality_%s" % i)
        Mi = csr_matrix(Mi)
        for i in range(Mi.shape[0]):
            start = Mi.indptr[i]
            end = Mi.indptr[i + 1]
            variables = [np.asscalar(j) for j in Mi.indices[start:end]]
            coeff = list(Mi.data[start:end])
            coeff = [np.asscalar(obj) for obj in coeff]
            dtemp = np.asscalar(d[i])
            model.addCons(soplex.quicksum(x[variables[j]] * coeff[j] for j in range(len(variables))) <= dtemp, "inequality_%s" % i)
        model.optimize()
        status = model.getStatus()
        if status == 'optimal':
            output = np.array([model.getVal(x[j]) for j in x]).ravel()
            return output, model.getObjVal()
        elif status == 'infeasible':
            raise OptimalityError("Primal infeasibility encountered.")
        else:
            raise OptimalityError("Soplex returned with error status " + status + ".")
    else:
        raise ValueError("Unknown solver: %s" % solver)


class OptimalityError(Exception):
    pass

class MidOpt(object):
    """
    Solves a linear program using the implicit midpoint rule as collocation.

    Given an LTI system

        dx/dt = A x + B u + u0        (LTI)

    solves the linear optimal control problem

        min_u int_0^T (C x(t) + D u(t) + E) dt
        s.t.  (LTI)
              Gx x(t) + Gu u(t) + gk <= 0
              Ku u(t) + kk = 0.
    """

    def __init__(self, *args, **kwargs):
        """
        Set up the optimizer.
            dx/dt = R x + S u + q0        (LTI)

        Usage:
        LinOpt(R,S): initialize the system with matrices R and S
        LinOpt(R,S,x0): initialize the system with matrices R and S, and initial condition x0
        LinOpt(R,S,x0,q0): initialize the system with matrices R and S, initial condition x0, and constant input q0
        x0 - initial condition (zero if left out)
        q0 - constant input (zero if left out)

        Parameters can be passed in kwargs. See LinOpt.default_params for options.
        """
        if len(args) == 2:
            self.R, self.S = args
            self.x0 = np.zeros(self.R.shape[1])
            self.q0 = np.zeros(self.R.shape[1])
        elif len(args) == 3:
            self.R, self.S, self.x0 = args
            self.q0 = np.zeros(self.R.shape[1])
        elif len(args) == 4:
            self.R, self.S, self.x0, self.q0 = args
        else:
            raise TypeError("LinOpt expected 2 to 4 arguments, got %d." % len(args))
        if isinstance(self.R, np.ndarray):
            self.R = sparse.lil_matrix(self.R)
        if isinstance(self.S, np.ndarray):
            self.S = sparse.lil_matrix(self.S)
        # objective
        self.C  = None
        self.D  = None
        self.E  = None
        # general inequality
        self.Gx = []
        self.Gu = []
        self.gk = []
        # state constraints are inequality constraints
        self.Hx = []
        self.hk = []
        # control constraints are equality constraints (Sx * V = 0)
        self.Ku = []
        self.kk = []
        # uncertain environment constraints (inequality) (d*V -uk <= 0)
        self.Uu = []
        self.uk = []
        self.U_direction = []
        self.U_degration_steps = []
        self.robust_horizon_steps = 1
        # initial constraints for robust optimization (equality) (V^1_0 - V^2_0 = 0)
        self.Lu = []
        self.lk = []
        self.L_horizon = []
        # save all locations at which x0 is present for simple update of collocation
        self.x0_locations = []
        self.u_decreasing_locations = []

        self.finaltime = None
        self.stepsize = None
        self.n_steps = None
        self.solver = 'cplex'
        self.solverparams = {}
        self.n_states = None
        self.update_size = None

    def add_environmental_constraint(self, Uui, uki, direction, robust_horizon_steps, degradation_steps):
        if robust_horizon_steps > self.robust_horizon_steps:
            self.robust_horizon_steps = robust_horizon_steps
        if direction == 'increasing':
            self.U_direction.append(direction)
            self.Uu.append(Uui)
            self.uk.append(uki)
            self.U_degration_steps.append(0)
        elif direction== 'decreasing':
            self.U_direction.append(direction)
            self.Uu.append(Uui)
            self.uk.append(uki)
            self.U_degration_steps.append(degradation_steps)
        else:
            raise AttributeError("Direction of environmental uncertainty not understood. Please use 'increasing' or 'decreasing'")

    def add_initial_control_constraint(self, Lui, Lki, r_horizon=1):
        """
        Add a initial equality constraint for the control to the optimization problem.
        Only constraining the first set of fluxes.

        The path constraint is given by:
               Lui u(0) + Lki = 0.
        Arguments:
        Lui - a k x m matrix, or scalar for all equal values.
        Lki - a vector of length k.
        """
        k = 1 if np.isscalar(Lki) else len(Lki)
        if np.isscalar(Lui):
            Lui = Lui * np.ones((k,self.m))
        if np.isscalar(Lki):
            Lki = Lki * np.ones(k)
        self.Lu.append(Lui)
        self.lk.append(Lki)
        self.L_horizon.append(r_horizon)

    def add_control_constraint(self, Kui, Kki):
        """
        Add a path equality constraint for the control to the optimization problem.

        The path constraint is given by:
               Kui u(t) + Kki = 0.
        Arguments:
        Kui - a k x m matrix, or scalar for all equal values.
        Kki - a vector of length k.
        """
        k = 1 if np.isscalar(Kki) else len(Kki)
        if np.isscalar(Kui):
            Kui = Kui * np.ones((k,self.m))
        if np.isscalar(Kki):
            Kki = Kki * np.ones(k)
        self.Ku.append(Kui)
        self.kk.append(Kki)

    def add_path_constraint(self, Gxi, Gui, Gki):
        """
        Add a path inequality constraint to the optimization problem.

        The path constraint is given by:
               Gxi x(t) + Gui u(t) + Gki <= 0.
        Arguments:
        Gxi - an k x n matrix, or scalar for all equal values.
        Gui - a k x m matrix, or scalar for all equal values.
        Gki - a vector of length k.
        """
        k = 1 if np.isscalar(Gki) else len(Gki)
        if np.isscalar(Gxi):
            Gxi = Gxi * np.ones((k,self.n))
        if np.isscalar(Gui):
            Gui = Gui * np.ones((k,self.m))
        if np.isscalar(Gki):
            Gki = Gki * np.ones(k)
        self.Gx.append(Gxi)
        self.Gu.append(Gui)
        self.gk.append(Gki)

    def add_state_constraint(self, Hxi, hki):
        """
        Add a path inequality constraint to the optimization problem.

        The path constraint is given by:
               Hxi x(t) + hki <= 0.
        Arguments:
        Hxi - an k x n matrix, or scalar for all equal values.
        hki - a vector of length k.
        """
        k = 1 if np.isscalar(hki) else len(hki)
        if np.isscalar(Hxi):
            Hxi = Hxi * np.ones((k,self.n))
        if np.isscalar(hki):
            hki = hki * np.ones(k)
        self.Hx.append(Hxi)
        self.hk.append(hki)

    def discretize(self, keep_matrices=False):
        """
        Setup the collocation matrices to transform the problem to the form

            max c x
        s.t.Me x =  b
            Mi x <= d

        :return: b, c, d, Me, Mi
        """
        if self.C is None or self.D is None or self.E is None:
            raise ValueError('Objective is not set. Please use set_objective(C,D,E).')
        # reset x0 locations and u_decreasing
        self.x0_locations = []
        self.u_decreasing_locations = []
        # length of x vector is 2 * n_steps * number of states + n_steps * number of reactions
        # Format of x vector = [x_1, xdot_1 , V_1, x_2 , xdot_2, V_2, ... , x_N, xdot_N, V_N ]
        n_states = self.S.shape[0]
        n_controls = self.S.shape[1]
        x_len = 2*n_states + n_controls
        self.n_states = n_states
        # set objective vector
        c = sparse.lil_matrix((1, x_len*self.n_steps))
        for i in range(self.n_steps):
            c[0 , 0 + i * x_len : n_states + i*x_len] = self.C
            c[0 , n_states*2 + i * x_len : (i+1) * x_len ] = self.D
        # set equality matrix Me
        # Me consists of 4 submatrices for the update rule, dynamics, control constraints, and initial control constraints
        # For update rule and dynamics we must use the initial values self.x0
        # We determine the number of rows in Me before initializing the matrix.
        Me_row_number = 2 * self.n_steps * n_states
        for Kui in self.Ku:
            Me_row_number += Kui.shape[0] * self.n_steps
        for entry, Lui in enumerate(self.Lu):
            Me_row_number += Lui.shape[0]*self.L_horizon[entry]
        # generate sparse equality matrix
        Me = sparse.lil_matrix((Me_row_number, x_len * self.n_steps))
        b = np.zeros(Me.shape[0])
        # fill equality matrix. Start with update rule
        # x_{k}-x_{k-1} - h xdot_k = 0 = b_k
        # initial part is x_1 - h xdot_1 = x0
        Me[:n_states, :n_states] = np.eye(n_states)
        Me[:n_states, n_states: 2 * n_states] = -self.stepsize*np.eye(n_states)
        b[:n_states] = self.x0
        self.x0_locations.append('self.b[:'+str(n_states)+'] = self.x0')
        # iterate over number of steps
        for i in range(1, self.n_steps):
            x_shift = i* x_len
            y_shift = i * n_states
            x_shift_1 = (i-1) * x_len
            Me[y_shift:y_shift+n_states, x_shift_1:x_shift_1 + n_states] = - np.eye(n_states) # x_{k-1}
            Me[y_shift:y_shift+n_states, x_shift:x_shift + n_states] = np.eye(n_states) # x_k
            Me[y_shift:y_shift+n_states, x_shift + n_states: x_shift + 2 * n_states] = -self.stepsize * np.eye(n_states) #xdot_k
        # total y_shift so far
        try:
            total_y_shift = y_shift + n_states
        except UnboundLocalError:
            total_y_shift = n_states
        self.update_size = total_y_shift
        # Dynamics next: R/2 x_{k-1} + R/2 x_k - x_dot_k + S V_k = -q_k
        # Initial dynamics first
        Me[total_y_shift:total_y_shift + n_states, :n_states]= 0.5*self.R
        Me[total_y_shift:total_y_shift + n_states, n_states: 2 * n_states] = -np.eye(n_states)
        Me[total_y_shift:total_y_shift + n_states, 2 * n_states : x_len] = self.S
        b[total_y_shift : total_y_shift+n_states] = -self.q0 - np.dot(0.5*self.R.todense(), self.x0)
        self.x0_locations.append('self.b['+str(total_y_shift)+': '+str(total_y_shift+n_states)+'] = -self.q0 - np.dot(0.5*self.R.todense(), self.x0)')
        # iterate over number of steps
        for i in range(1, self.n_steps):
            x_shift = i * x_len
            y_shift = total_y_shift + i * n_states
            x_shift_1 = (i-1) * x_len
            Me[y_shift:y_shift+n_states, x_shift_1: x_shift_1 + n_states] = 0.5 * self.R  # x_{k-1}
            Me[y_shift:y_shift+n_states, x_shift  : x_shift + n_states] = 0.5 * self.R    # x_k
            Me[y_shift:y_shift+n_states, x_shift + n_states: x_shift + 2 * n_states] = - np.eye(n_states) #xdot_k
            Me[y_shift:y_shift+n_states, x_shift + 2* n_states: x_shift + x_len] = self.S
            b[y_shift:y_shift + n_states] = -self.q0
        # continue
        try:
            total_y_shift = y_shift + n_states
        except UnboundLocalError:
            total_y_shift = total_y_shift + n_states
        # additional control constraints, e.g. quasi-steady-state
        for j in range(len(self.Ku)):
            Kui = self.Ku[j]
            kki = self.kk[j]
            for i in range(self.n_steps):
                x_shift = i * x_len
                Me[total_y_shift:total_y_shift+Kui.shape[0], x_shift + 2*n_states: x_shift + 2*n_states + n_controls] = Kui
                b[total_y_shift:total_y_shift+Kui.shape[0]] = -kki
                total_y_shift += Kui.shape[0]
        # initial control constraints only relevant for robust optimization
        for j in range(len(self.Lu)):
            Lui = self.Lu[j]
            lki = self.lk[j]
            r_horizon = self.L_horizon[j]
            for counter in range(r_horizon):
                Me[total_y_shift: total_y_shift+Lui.shape[0], 2*n_states+counter*(2*n_states+n_controls): (counter+1)*(2*n_states+n_controls)] = Lui
                b[total_y_shift:total_y_shift+Lui.shape[0]] = -lki
                total_y_shift += Lui.shape[0]
        # Me and b are now ready
        #########################
        # generate inequality matrix Mi and d next
        total_y_shift = 0
        Mi_row_number = 0
        for Gxi in self.Gx:
            Mi_row_number += Gxi.shape[0]*self.n_steps
        for Hxi in self.Hx:
            Mi_row_number += Hxi.shape[0] * self.n_steps
        for number, Uui in enumerate(self.Uu):
            if self.U_direction[number] == 'increasing':
                Mi_row_number += Uui.shape[0] * self.robust_horizon_steps
            else:
                Mi_row_number += Uui.shape[0] * (self.n_steps - self.robust_horizon_steps)
        Mi = sparse.lil_matrix((Mi_row_number , self.n_steps*x_len))
        d = np.zeros(Mi_row_number)
        for j in range(len(self.Gx)):
            Gxi = self.Gx[j]
            n_constr = Gxi.shape[0]
            Gui = self.Gu[j]
            gki = self.gk[j]
            # First create entry which includes x0
            Mi[total_y_shift: total_y_shift + n_constr , :n_states ] = 0.5 * Gxi
            Mi[total_y_shift: total_y_shift + n_constr , 2*n_states: 2*n_states+n_controls] = Gui
            if isinstance(Gxi, np.ndarray):
                d[total_y_shift: total_y_shift + n_constr] = -gki - np.dot(0.5 * Gxi, self.x0)
                self.x0_locations.append('self.d[' + str(total_y_shift) + ': ' + str(total_y_shift + n_constr) +
                                         '] = -self.gk[' + str(j) + '] - np.dot(0.5*self.Gx[' + str(j) + '], self.x0)')
            else:
                d[total_y_shift: total_y_shift + n_constr] = -gki - np.dot(0.5 * Gxi.todense(), self.x0)
                self.x0_locations.append('self.d[' + str(total_y_shift) + ': ' + str(total_y_shift + n_constr) +
                                         '] = -self.gk[' + str(j) + '] - np.dot(0.5*self.Gx[' + str(j) + '].todense(), self.x0)')

            for i in range(1,self.n_steps):
                y_shift = total_y_shift + i* n_constr
                x_shift = i * x_len
                x_shift_1 = (i-1) * x_len
                Mi[y_shift: y_shift + n_constr, x_shift : x_shift + n_states] = 0.5 * Gxi
                Mi[y_shift: y_shift + n_constr, x_shift_1: x_shift_1 + n_states] = 0.5 * Gxi
                Mi[y_shift: y_shift + n_constr, x_shift + 2 * n_states: x_shift + 2 * n_states + n_controls] = Gui
                d[y_shift: y_shift + n_constr] = -gki
            try:
                total_y_shift = y_shift + n_constr
            except UnboundLocalError:
                total_y_shift += n_constr
        for j in range(len(self.Hx)):
            Hxi = self.Hx[j]
            n_constr = Hxi.shape[0]
            hki = self.hk[j]
            # no zero entry needed
            for i in range(self.n_steps):
                x_shift = i * x_len
                # x_shift_1 = (i-1) * x_len
                Mi[total_y_shift: total_y_shift + n_constr, x_shift : x_shift + n_states] = Hxi
                d[total_y_shift: total_y_shift + n_constr] = -hki
                total_y_shift += n_constr
        for j in range(len(self.Uu)):
            Uui = self.Uu[j]
            n_constr = Uui.shape[0]
            uki = self.uk[j]
            if self.U_direction[j] == 'increasing':
                for i in range(self.n_steps):
                    x_shift = i * x_len
                    Mi[total_y_shift: total_y_shift + n_constr, x_shift + 2*n_states : x_shift + 2*n_states + n_controls] = Uui
                    d[total_y_shift: total_y_shift + n_constr] = uki
                    total_y_shift += n_constr
            if self.U_direction[j] == 'decreasing':
                r_steps = self.robust_horizon_steps
                for i in range(r_steps, self.U_degration_steps[j]):
                    x_shift = i * x_len
                    Mi[total_y_shift: total_y_shift + n_constr, x_shift+ 2*n_states : x_shift +2*n_states + n_controls ] = Uui
                    d[total_y_shift: total_y_shift + n_constr] = uki - (i - r_steps)*uki/ (self.U_degration_steps[j] - r_steps)
                    self.u_decreasing_locations.append('self.d['+str(total_y_shift)+':'+str(total_y_shift + n_constr)+'] = self.uk['+str(j)+'] -'
                                                       +str(i - r_steps)+'* self.uk['+str(j)+'] /'+str(self.U_degration_steps[j]
                                                                                                        - r_steps))
                    if d[total_y_shift: total_y_shift + n_constr] < 0:
                        d[total_y_shift: total_y_shift + n_constr]=0
                    total_y_shift += n_constr
                for i in range(self.U_degration_steps[j], self.n_steps):
                    x_shift = i * x_len
                    Mi[total_y_shift: total_y_shift + n_constr, x_shift+ 2*n_states : x_shift +2*n_states + n_controls ] = Uui
                    d[total_y_shift: total_y_shift + n_constr] = 0
                    total_y_shift += n_constr
        # Mi and d are now ready
        # if keep_matrices:
        if keep_matrices:
            self.b = b
            self.c = c
            self.d = d
            self.Me = Me
            self.Mi = Mi
        return b, c , d, Me, Mi

    def solve(self, **kwargs):
        """
        Solve the LTI optimization problem with cvxopt with fixed final time.

        If the optimization is successful, the method returns:

        * The optimal value.
        * A :py:class:`.CollocatedTrajectory` object representing an optimal trajectory.

        See `self.solve_lp` for available solvers and their options.
        """
        if self.stepsize is None:
            self.stepsize = self.finaltime / float(self.n_steps)
        if 'keep_matrices' in kwargs:
            b, c, d, Me, Mi = self.discretize(keep_matrices=kwargs['keep_matrices'])
            kwargs.pop('keep_matrices', None)
        else:
            b, c, d, Me , Mi = self.discretize()
        b = np.atleast_2d(b)
        d = np.atleast_2d(d)
        x, obj = self.solve_lp(c, b, d, Me, Mi, params=kwargs)
        return (obj , MidpointTrajectory(x, self.finaltime, self.n_steps, self.S.shape[1], self.S.shape[0], self.x0))

    def solve_iteration(self, new_x0, new_uk=None, **kwargs):
        """
        Use a preexisting collocation of the problem and update
        """
        b,c,d,Me,Mi = self.update_collocation(new_x0, new_uk)
        b = np.atleast_2d(b)
        d = np.atleast_2d(d)
        x, obj = self.solve_lp(c, b, d, Me, Mi, params=kwargs)
        return (obj, MidpointTrajectory(x, self.finaltime, self.n_steps, self.S.shape[1], self.S.shape[0], self.x0))

    def solve_lp(self, c, b, d, Me, Mi, params={}):
        """
        Solve the LP problem::

            min  c' x
            s.t. Me x  = b
                 Mi x <= d

        Returns an optimal solution x and objective function value c' x.
        Raises OptimalityError if solver fails.

        Available solvers (defined by params['solver']) with parameters:
        * 'cvxopt'
        * 'gurobi'
        * 'cplex'
        """
        solver = params['solver'] if 'solver' in params else self.solver
        solverparams = self.solverparams.copy()
        solverparams.update(params)
        if solver == "cvxopt":
            cvxopt.solvers.options.update(solverparams)
            cvxopt.solvers.options['show_progress'] = True
            sol = cvxopt.solvers.lp(cvxopt.matrix(self.toarray(c)).T, cvx_sparse(Mi),
                                    cvxopt.matrix(self.toarray(d).ravel()), cvx_sparse(Me),
                                    cvxopt.matrix(self.toarray(b).ravel()),
                                    solver=solverparams['solver'] if 'solver' in solverparams else None)
            if sol['status'] == "dual infeasible":
                raise OptimalityError("Dual infeasibility encountered.")
            if sol['status'] == "primal infeasible":
                raise OptimalityError("Primal infeasibility encountered.")
            if sol['status'] == "unknown":
                raise OptimalityError("Optimizer returned with an unknown status.")
            x = np.array(sol['x']).ravel()
            return x, sol['primal objective']
        elif solver == "gurobi":
            m = grb.Model()
            for i in solverparams:
                m.setParam(i, solverparams[i])
            optdim = c.shape[1]
            optvar = [m.addVar(lb=-grb.GRB.INFINITY, obj=c[0,i]) for i in range(optdim)]
            m.update()
            # see http://stackoverflow.com/questions/22767608/sparse-matrix-lp-problems-in-gurobi-python for following code
            Me = Me.tocsr()
            for i in range(Me.shape[0]):
                start = Me.indptr[i]
                end   = Me.indptr[i+1]
                variables = [optvar[j] for j in Me.indices[start:end]]
                coeff     = Me.data[start:end]
                m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.EQUAL, rhs=b[0,i])
            Mi = Mi.tocsr()
            for i in range(Mi.shape[0]):
                start = Mi.indptr[i]
                end   = Mi.indptr[i+1]
                variables = [optvar[j] for j in Mi.indices[start:end]]
                coeff     = Mi.data[start:end]
                m.addConstr(lhs=grb.LinExpr(coeff, variables), sense=grb.GRB.LESS_EQUAL, rhs=d[0,i])
            # m.setParam('NumericFocus', 3)
            # m.setParam('scaleFlag' , 2)
            # m.setParam('InfUnbdInfo', 1)
            m.optimize()
            if m.Status == grb.GRB.status.OPTIMAL:
                x = np.array([v.x for v in optvar])
                return x, m.getAttr('objVal')
            elif m.Status == grb.GRB.status.INFEASIBLE:
                raise OptimalityError("Primal infeasibility encountered.")
            else:
                raise OptimalityError("Gurobi returned with error status {:d}.".format(m.Status))
        elif solver == "cplex":
            model = cplex.Cplex()
            # try to populate by row
            model.objective.set_sense(model.objective.sense.minimize)
            model.variables.add(obj=c.toarray()[0,:], lb=[-cplex.infinity for i in range(c.shape[1])])
            Me = Me.tocsr()
            for i in range(Me.shape[0]):
                start = Me.indptr[i]
                end   = Me.indptr[i+1]
                variables = [np.asscalar(j) for j in Me.indices[start:end]]
                coeff     = list(Me.data[start:end])
                coeff = [np.asscalar(obj) for obj in coeff]
                btemp = np.asscalar(b[0,i])
                model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="E", rhs=[btemp])
            Mi = Mi.tocsr()
            for i in range(Mi.shape[0]):
                start = Mi.indptr[i]
                end   = Mi.indptr[i+1]
                variables = [np.asscalar(j) for j in Mi.indices[start:end]]
                coeff     = list(Mi.data[start:end])
                coeff     = [np.asscalar(obj) for obj in coeff]
                dtemp = np.asscalar(d[0,i])
                model.linear_constraints.add(lin_expr=[[variables, coeff]], senses="L", rhs=[dtemp])
            model.parameters.simplex.tolerances.feasibility.set(1e-08)
            model.parameters.simplex.tolerances.optimality.set(1e-08)
            model.solve()
            if model.solution.status[model.solution.get_status()] == 'optimal':
                x = np.array(model.solution.get_values()).ravel()
                return x, model.solution.get_objective_value()
            # elif model.solution.get_status() == 5:
            #     print 'may contain infeasibilities'
            #     x = np.array(model.solution.get_values()).ravel()
            #     return x, model.solution.get_objective_value()
            elif model.solution.status[model.solution.get_status()] == 'infeasible':
                # model.write('faulty_problem.lp')
                raise OptimalityError("Primal infeasibility encountered.")
            else:
                raise OptimalityError("Cplex returned with error status '"+str(model.solution.status[model.solution.get_status()])+"'.")
        elif solver == "soplex":
            model = soplex.Model('deFBA_LP')
            for par in params:
                model.setParam(par, params[par])
            if not 'numerics/feastol' in params:
                model.setParam('numerics/feastol', 1e-8)
                model.setParam('numerics/lpfeastol', 1e-11)
            x = {}
            for i, j in enumerate(c.toarray()[0,:]):
                x[i]= model.addVar(lb=None, ub=None, vtype="C", name='x_'+str(i), obj=j)
            Me = csr_matrix(Me)
            for i in range(Me.shape[0]):
                start = Me.indptr[i]
                end = Me.indptr[i + 1]
                variables = [np.asscalar(j) for j in Me.indices[start:end]]
                coeff = list(Me.data[start:end])
                coeff = [np.asscalar(obj) for obj in coeff]
                btemp = np.asscalar(b[0,i])
                model.addCons(soplex.quicksum(x[variables[j]]*coeff[j] for j in range(len(variables))) == btemp, "equality_%s"%i)
            Mi = csr_matrix(Mi)
            for i in range(Mi.shape[0]):
                start = Mi.indptr[i]
                end = Mi.indptr[i + 1]
                variables = [np.asscalar(j) for j in Mi.indices[start:end]]
                coeff = list(Mi.data[start:end])
                coeff = [np.asscalar(obj) for obj in coeff]
                dtemp = np.asscalar(d[0,i])
                model.addCons(soplex.quicksum(x[variables[j]] * coeff[j] for j in range(len(variables))) <= dtemp, "inequality_%s" % i)
            model.optimize()
            status = model.getStatus()
            if status == 'optimal':
                output = np.array([model.getVal(x[j]) for j in x]).ravel()
                return output, model.getObjVal()
            elif status == 'infeasible':
                raise OptimalityError("Primal infeasibility encountered.")
            else:
                raise OptimalityError("Soplex returned with error status " + status + ".")
        else:
            raise ValueError("Unknown solver: %s" % solver)

    def set_objective(self, C, D, E, F):
        """
        Sets the objective function.

        :param C: weights for x
        :param D: weights for u
        :param E: constant s
        :param F: constant end-time addition (useless but kept for compatibility)
        :return:
        """
        self.C, self.D, self.E = C, D, E

    def set_parameters(self, **kwargs):
        """
        Set optimization parameters.

        Available parameters are:

        timesteps - number of discretization time steps (default = 50)
        finaltime - value of T (default = 1), maximal T in the problem with free final time
        finaltimetolerance - termination tolerance on the final time (default = 0.01)
        fixedterm - whether to keep final time fixed or not (default = True)
        collocation - which collocation scheme to use (default = Collocation(3, 'lagrange', 'radau'))

        Depending on the solver, additional parameters may be available.
        cvxopt: Parameters 'solver', 'primalstart', 'dualstart' from cvxopt.solvers.lp
        """
        for k in ['timesteps', 'finaltime', 'solver']:
            if k in kwargs:
                if k == 'timesteps':
                    self.n_steps = kwargs.pop(k)
                elif k== 'finaltime':
                    self.finaltime = kwargs.pop(k)
                elif k == 'solver':
                    self.set_solver(self.params['solver']) # call to import solver's module
                elif k == 'fixedterm':
                    pass
            else:
                pass
        # self.solverparams.update(kwargs)

    def set_solver(self, solver):
        """
        Choose numerical solver to perform the optimization.

        Available solvers:
        cvxopt - Interior point solver cvxopt, requires the cvxopt module.
        gurobi - Gurobi, requires the gurobipy module.
        """
        self.solver = 'cvxopt'
        if solver == "cvxopt":
            import cvxopt
            import cvxopt.solvers
            self.cvxopt = cvxopt
        elif solver == "gurobi":
            try:
                import gurobipy
                self.grb = gurobipy
                self.solver = 'gurobi'
            except ImportError as err:
                print err
                print 'falling back to cvxopt as solver'
                import cvxopt
                import cvxopt.solvers
                self.cvxopt = cvxopt
        elif solver == "cplex":
            try:
                import cplex
                self.cplex = cplex
                self.solver = 'cplex'
            except ImportError as err:
                print err
                print 'falling back to cvxopt as solver'
                import cvxopt
                import cvxopt.solvers
                self.cvxopt = cvxopt
        elif solver == "soplex":
            try:
                import pyscipopt as soplex
                self.soplex = soplex
                self.solver = 'soplex'
            except ImportError as err:
                print err
                print 'falling back to cvxopt as solver'
                import cvxopt
                import cvxopt.solvers
                self.cvxopt = cvxopt
        else:
            raise ValueError("Unknown solver: %s" % solver)

    def update_collocation(self, new_x0, new_uk, **kwargs):
        """
        changes the initial values of the matrices representing the collocated problem
        :param new_x0: array with the updated initial values
        :return: collocated matrices
        """
        if not hasattr(self, 'Me'):
            raise ValueError('No collocation matrices created. Please run a simulation first.')
        self.x0 = new_x0
        for location in self.x0_locations:
            exec(location)
        self.uk = new_uk
        for location in self.u_decreasing_locations:
            exec(location)
        return self.b, self.c, self.d, self.Me, self.Mi

class MidpointTrajectory(object):
    """
    Represents a trajectory for an LTI system approximated by collocation.
    """
    def __init__(self, Z, *args, **kwargs): # args: finaltime, timesteps, n_controls, n_states, initialstate
        if len(args)==5:
            finaltime, timesteps, n_controls, n_states, initialstate = args
            self._x = np.zeros(( n_states, timesteps + 1 ))
            self._x[:, 0] = initialstate
            self._xdot = np.zeros(( n_states, timesteps ))
            self._u = np.zeros((n_controls, timesteps))
            x_len = 2*n_states + n_controls
            for i in range(timesteps):
                shift = i*x_len
                self._x[:, i + 1] = Z[shift:shift + n_states]
                self._xdot[:, i] = Z[shift + n_states : shift + 2*n_states]
                self._u[:,i] = Z[shift + 2*n_states: shift + 2*n_states + n_controls]
            self._m = n_controls
            self._n = n_states
            self._N = timesteps
            self._x0 = initialstate
            self._h = finaltime / timesteps
            self.finaltime = finaltime
        else:
            raise AttributeError('Wrong amount of variables')

    def get_control(self, t):
        def get_single(ti):
            ind = self.index(ti)
            if ind >= self._N:
                ind = self._N - 1
            return self._u[:, int(ind)]
        return get_single(t) if np.isscalar(t) else np.array([get_single(ti) for ti in t])

    def get_state(self, t):
        def get_single(ti):
            ind = self.index(ti)
            return self._x[:, int(ind)]
        return get_single(t) if np.isscalar(t) else np.array([get_single(ti) for ti in t])

    def get_statederivative(self, t):
        def get_single(ti):
            ind = self.index(ti)
            if ind >= self._N:
                ind = self._N - 1
            return self._xdot[:,int(ind)]
        return get_single(t) if np.isscalar(t) else np.array([get_single(ti) for ti in t])

    def index(self, t):
        return t // self._h

##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
"""
This example shows you how to construct a deFBA by directly giving the describing system matrices to the DefbaModel class.
Furthermore, the deFBA model is then exported to SBML using the RAM standard [1] and evaluated using the solver gurobi. 
If gruobi is not available cvxopt is used instead.
The last command can be included to generate a solutions according to the robust example from the examples source.
The example is taken from the paper 'Modeling metabolic networks including gene expression and uncertainties' available at the arXiv [2].

[1] https://www.fairdomhub.org/sops/304
[2] https://arxiv.org/abs/1609.08961
"""
import numpy as np
import defba

# species and reaction names for easier readability of results
species = ['Dext', 'Eext', 'Fext', 'Hext', 'O2ext', 'Carb1', 'Carb2', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'O2', 'ATP',
           'NADH', 'Eb', 'Ec', 'Ed', 'Ee', 'Ef', 'Eg', 'Eh', 'En', 'Et', 'R', 'S', 'Tc1', 'Tc2', 'Tf', 'Th']
reactions = ['v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9', 'v10', 'v11', 'v12', 'v13', 'v14', 'v15', 'v16', 'v17', 'v18', 'v19',
             'v20', 'v21', 'v22',
             'v23', 'v24', 'v25', 'v26', 'v27', 'v28', 'v29', 'v30', 'v31']
# which reactions are reversible?
reversible_fluxes = ['v5', 'v6', 'v13', 'v14', 'v15']
# construct stoichiometric matrix by sub-matrices
# exchange with substrate:
S_y = np.array([[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                # Dext
                [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                # Eext
                [0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                # Fext
                [0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                # Hext
                [0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                # O2ext
                [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                # Carb1
                [0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])  # Carb2
# metabolite dynamics:
# exchange reactions
S_yx = np.array([[1, 1, 0, 0, 0, 0, 0],  # A
                 [0, 0, 0, 0, 0, 0, 0],  # B
                 [0, 0, 0, 0, 0, 0, 0],  # C
                 [0, 0, 0, 0, -1, 0, 0],  # D
                 [0, 0, 0, 0, 0, -1, 0],  # E
                 [0, 0, 1, 0, 0, 0, 0],  # F
                 [0, 0, 0, 0, 0, 0, 0],  # G
                 [0, 0, 0, 0, 0, 0, 1],  # H
                 [0, 0, 0, 1, 0, 0, 0],  # O2
                 [0, 0, 0, 0, 0, 0, 0],  # ATP
                 [0, 0, 0, 0, 0, 0, 0]])  # NADH
# metabolic reactions
S_xx = np.array([[-1, 0, 0, 0, 0, 0, 0, 0, 0],  # A
                 [1, -1, -1, 0, 0, 0, 0, 0, 0],  # B
                 [0, 1, 0, -1, 0.8, -1, -1, 0, 0],  # C
                 [0, 0, 0, 0, 0, 3, 0, 0, 0],  # D
                 [0, 0, 0, 0, 0, 0, 3, 0, 0],  # E
                 [0, 0, 1, 0, 0, 0, 0, 0, 0],  # F
                 [0, 0, 0, 1, -1, 0, 0, -1, 0],  # G
                 [0, 0, 0, 0, 0, 0, 0, 1, 0],  # H
                 [0, 0, 0, 0, 0, 0, 0, 0, -1],  # O2
                 [-1, 2, 0, 0, 0, 2, 0, -1, 1],  # ATP
                 [0, 2, 0, 0, 2, 0, -4, -2, -1]])  # NADH

S_px = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # A
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # B
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1500, -250],  # C
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # D
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # E
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -250],  # F
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # G
                 [-400, -1500, -400, -400, -500, -500, -1000, -1000, -2000, -500, -4000, -500, -500, -4000, -250],  # H
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # O2
                 [-1600, -6000, -1600, -1600, -2000, -2000, -4000, -4000, -10000, -2000, -16000, -2000, -2000, -21000, -1500],  # ;%ATP
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])  # ; %NADH
# macromolecule dynamics
S_pp = np.array([[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # Eb
                 [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # Ec
                 [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  # Ed
                 [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],  # Ee
                 [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  # Ef
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],  # Eg
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],  # Eh
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],  # En
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],  # Et
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  # R
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],  # S
                 [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # Tc1
                 [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # Tc2
                 [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # Tf
                 [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])  # Th
# concatenate the matrices
stoich = np.vstack((S_y, np.hstack((S_yx, S_xx, S_px))))
stoich = np.vstack((stoich, np.hstack((np.zeros((15, 16)), S_pp))))
# construct matrices for additional external dynamics, s.t dz/dt = R z + S v + q
R = np.zeros((stoich.shape[0], stoich.shape[0]))
R[species.index('O2ext'), species.index('O2ext')] = -0.4
q = np.zeros(len(species))
q[species.index('O2ext')] = 20
# introduce list of constants
# kcats for exchange reactions
k1 = 3000.0
k2 = 2000.0
k3 = 3000.0
k4 = 1000.0
k5 = 1000.0
k6 = 1000.0
k7 = 3000.0
k8 = 1800.0
k9 = 1800.0
k10 = 1800.0
k11 = 1800.0
k12 = 1800.0
k13 = 1800.0
k14 = 1800.0
k15 = 1800.0
k16 = 1800.0
# kcats for production of macromolecules
k17 = 2.5
k18 = 0.67
k19 = 2.5
k20 = 2.5
k21 = 2.0
k22 = 2.0
k23 = 1.0
k24 = 1.0
k25 = 0.5
k26 = 2.0
k27 = 0.25
k28 = 2.0
k29 = 2.0
k30 = 0.2
k31 = 3.0
# construct the Enzyme Capacity Constraints H_C v <= H_E z
# Exchange reactions
H_cy = np.array([[1 / k1, 0, 0, 0, 0, 0, 0], [0, 1 / k2, 0, 0, 0, 0, 0], [0, 0, 1 / k3, 0, 0, 0, 0], [0, 0, 0, 1 / k4, 1 / k5, 1 / k6, 0],
                 [0, 0, 0, 1 / k4, -1 / k5, 1 / k6, 0], [0, 0, 0, 1 / k4, 1 / k5, -1 / k6, 0], [0, 0, 0, 1 / k4, -1 / k5, -1 / k6, 0],
                 [0, 0, 0, 0, 0, 0, 1 / k7]])

H_Ey = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]])
# Metabolic reactions
H_cx = np.array([[1 / k8, 0, 0, 0, 0, 0, 0, 0, 0], [0, 1 / k9, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1 / k10, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 1 / k11, 0, 0, 0, 0, 0], [0, 0, 0, 0, 1 / k12, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1 / k13, 0, 0, 0],
                 [0, 0, 0, 0, 0, -1 / k13, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1 / k14, 0, 0], [0, 0, 0, 0, 0, 0, -1 / k14, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 1 / k15, 0], [0, 0, 0, 0, 0, 0, 0, -1 / k15, 0], [0, 0, 0, 0, 0, 0, 0, 0, 1 / k16]])

H_Ex = np.array([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]])
# Enzyme production via ribosome R
H_cp = np.array([[1 / k17, 1 / k18, 1 / k19, 1 / k20, 1 / k21, 1 / k22, 1 / k23, 1 / k24, 1 / k25, 1 / k26, 1 / k27, 1 / k28, 1 / k29,
                  1 / k30, 1 / k31]])

H_Ep = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]])
# concatenate the matrices
Hc = np.hstack((H_cy, np.zeros((8, 24))))
Hc = np.vstack((Hc, np.hstack((np.zeros((12, H_cy.shape[1])), H_cx, np.zeros((12, H_cp.shape[1]))))))
Hc = np.vstack((Hc, np.hstack((np.zeros((1, 16)), H_cp))))

He = np.vstack((H_Ey, H_Ex, H_Ep))
He = np.hstack((np.zeros((He.shape[0], 18)), He))
# tell the DefbaModel class the amounts of external species, metabolic, species, etc.
model_numbers = {'metab_species': S_xx.shape[0], 'metab_reactions': S_xx.shape[1], 'ext_species': S_y.shape[0],
                 'exch_reactions': S_yx.shape[1], 'proteins': S_pp.shape[0], 'protein_reactions': S_px.shape[1]}
# biomass vec and biomass dict include the molecular weight of the macromolecules
biomass_vec = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 10, 10,
                        20, 5, 40, 5, 5, 60, 7.5, 4, 15, 4, 4])
biomass_dict = {'Eb': 5.0, 'Ec': 5.0, 'Ed': 10.0, 'Ee': 10.0, 'Ef': 20.0, 'Eg': 5.0, 'Eh': 40.0, 'En': 5.0, 'Et': 5.0, 'R': 60.0, 'S': 7.5,
                'Tc1': 4.0, 'Tc2': 15.0, 'Tf': 4.0, 'Th': 4.0}
# Construct biomass composition constraint (Here 35% of biomass must be of type S)
HB = np.dot(0.35, biomass_vec.copy())
HB[-5] = HB[-5] - 7.5
# Define external conditions and initial biomass
ics = {'Dext': 0, 'Eext': 0, 'Fext': 0, 'Hext': 0, 'O2ext': 50, 'Carb1': 2, 'Carb2': 30, 'Eb': 3.412387411324629e-05,
       'Ec': 3.145732765886909e-05,
       'Ed': 1.1967758585195503e-05, 'Ee': 0.0, 'Ef': 2.666546454377196e-06, 'Eg': 1.4722928904299521e-05, 'Eh': 1.4723443408691826e-05,
       'En': 0.0, 'Et': 3.3466739491569915e-05, 'R': 3.062174454205124e-05, 'S': 0.00023333333333333333, 'Tc1': 2.0474324467947774e-05,
       'Tc2': 0.0, 'Tf': 0.0, 'Th': 0.0}
# choose scaling factor for macromolecules and enzyme producing reactions
scale = 100
# choose whether to use the steady-state assumption (dx/dt = 0) as a constraint or use SVD to reduce the amount of constraints, metabolites
# and reactions. Usually, the full system (reduced=False) is very sparse and much quicker so solve.
reduced = False
# Initialize DefbaModel object
model = defba.DefbaModel(stoich, species=species, reactions=reactions, scale=100, numbers=model_numbers, biomass=biomass_dict,
                         HC=Hc, HE=He, reversible_fluxes=reversible_fluxes, HB=HB, model_name='carbon_core_net',
                         ics=ics, delete_metabolites=True)
# export the model to SBML
model.exportToSbml('carbon_core')
# We add additional O2ext dynamics. These are not saved in the SBML file as they do not belong directly to the network.
model.addLinearDynamics(R, q)
# generate initial values by using the Resource Balance Analysis (RBA)
# initial environment is chosen to include only Carb1 and oxygen (O2)
initial_values, initial_fluxes, mu = model.RBA(0.005, np.dot(1000, [0.001, 0, 0, 0, 1, 1, 0]), solver='gurobi', mu=0.1, eps=10 ** -8)
# generate results using the regular deFBA with end-time Tend=70 min and the calculated initial values. The choose a stepsize of 0.5 min.
results = model.deFBA(Tend=70, stepsize=0.5, RBA_init=False, output_name='regular', solver='gurobi', initial_biomass=ics,
                      external_conditions=ics)
# test if the short-term deFBA generates different results
short_results = model.shortterm(Tend=70, stepsize=0.5, RBA_init=False, external_conditions=ics, output_name='short-term',
                                prediction_horizon=31.7995144574, control_horizon=0.5, initial_biomass=ics, solver='gurobi')

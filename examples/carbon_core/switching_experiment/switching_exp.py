##################################################################
# Copyright (C) 2018 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################

import numpy as np
import csv
import defba

# We start off by modeling the system with Oxygen available
model_with = defba.readSBML('carbon_core_with_O2.xml', scale=1)
env_name_with = model_with.addUncertainEnvironment('v4', limiting_species='O2ext', decreasing=True)

ics = {'Dext': 0, 'Eext': 0, 'Fext': 0, 'Hext': 0, 'O2ext': 0, 'Carb1': 200, 'Carb2': 0, 'Eb': 3.412387411324629e-05,
       'Ec': 3.145732765886909e-05, 'Ed': 1.1967758585195503e-05, 'Ee': 0.0, 'Ef': 2.666546454377196e-06, 'Eg': 1.4722928904299521e-05,
       'Eh': 1.4723443408691826e-05, 'En': 0.0, 'Et': 3.3466739491569915e-05, 'R': 3.062174454205124e-05, 'S': 0.00023333333333333333,
       'Tc1': 2.0474324467947774e-05, 'Tc2': 0.0, 'Tf': 0.0, 'Th': 0.0}

model_without = defba.readSBML('carbon_core_without_O2.xml', scale=1)
env_name_without = model_without.addUncertainEnvironment('v4', limiting_species='O2ext', decreasing=False)
species_order_without = list(model_without.species)
reactions_order_without = list(model_without.reactions)
# Calculate the initial values based on a pregrowth under aerobic conditions
RBA_Init = model_without.RBA(1, ics)
next_scenario = 'without_O2.xml'
# Let the experiment run 1 days with 30 min intervals. stepsize = 2 min.
# oxygen dynamics are limited to on/off. Carb1 dynamics also disabled to not limit growth.
short = {}
# results format = [[times], [species], [reactions]]
short[0] = model_with.shortterm(Tend=30, stepsize=0.5, external_conditions=ics, initial_biomass=RBA_Init[0], solver='gurobi', output_name='0',
                                prediction_horizon=32.0, control_horizon=2, fixed_horizons=True, midpoint=False)
species_order_with = list(model_with.species)
reactions_order_with = list(model_with.reactions)


def switch_input_to_order(output_order, input_order, input_data):
    output_data = np.zeros(input_data.shape)
    for row, oo in enumerate(output_order):
        output_data[row] = input_data[input_order.index(oo)]
    return output_data

multiplier = 1
for i in range(5):
    current_biomass_short = np.dot(model_with.biomass, short[i][1][-1,:])
    multiplier_short = 1.0 # /float(current_biomass_short)
    if next_scenario == 'without_O2.xml':
        last_short_species = multiplier_short*switch_input_to_order(species_order_without, species_order_with, short[i][1][-1, :])
        last_short_fluxes = multiplier_short*switch_input_to_order(reactions_order_without, reactions_order_with, short[i][2][-1, :])
        short[i + 1] = model_without.shortterm(Tend=30, stepsize=0.5, prediction_horizon=32.0, control_horizon=2, fixed_horizons=True,
                                               external_conditions=last_short_species[:model_without.numbers['ext_species']],
                                               initial_biomass=last_short_species[-model_without.numbers['proteins']:], solver='gurobi',
                                               output_name=str(i + 1), midpoint=False)
        next_scenario = 'with_O2.xml'
    elif next_scenario == 'with_O2.xml':
        last_short_species = multiplier_short*switch_input_to_order(species_order_with, species_order_without, short[i][1][-1, :])
        last_short_fluxes  = multiplier_short*short[i][2][-1, :]
        short[i + 1] = model_with.shortterm(Tend=30, stepsize=0.5, initial_biomass=last_short_species[-model_with.numbers['proteins']:],
                                            external_conditions=last_short_species[:model_with.numbers['ext_species']], midpoint=False,
                                            output_name=str(i + 1), prediction_horizon=32.0, control_horizon=2, fixed_horizons=True,
                                            solver='gurobi',)
        next_scenario = 'without_O2.xml'


# Collect Subsolutions and combine them to a single one
short_final = [[i * 0.5 for i in range(60*len(short)+1)], np.atleast_2d(np.array(short[0][1][:, :])),
               np.atleast_2d(np.array(short[0][2][:, :]))]
for number in range(1, len(short)):
    short_final[1] = np.append(short_final[1], np.atleast_2d(short[number][1][:60,:]), axis=0)
    short_final[2] = np.append(short_final[2], np.atleast_2d(short[number][2][:60,:]), axis=0)
defba.defbamodel.export_states(short_final[0], short_final[1], model_with.species,
                              model_with.scale, model_with.numbers['ext_species'] + model_with.numbers['metab_species'], file_name='short_final')

# calculate biomass amount at all time points
short_biomass = np.zeros(len(short_final[0]))
for time in range(len(short_final[0])):
    short_biomass[time] = np.dot(model_with.biomass, short_final[1][time, :])

# calculate biomass percentages for all species
short_biomass_percent = np.zeros((len(short_final[0]), model_with.numbers['proteins']))
for time in range(len(short_final[0])):
    for position in range(model_with.numbers['proteins']):
        short_biomass_percent[time][position] = 100.0 * model_with.biomass[position -15]* short_final[1][time][position - 15] / short_biomass[time]

# export results
resfile_short = open('short_biomass_final' + '.csv', "w")
reswriter_short = csv.writer(resfile_short, delimiter="\t")
reswriter_short.writerow(['time'] + ['biomass'])
times = [i * 0.5 for i in range(60*len(short)+1)]
for i in range(len(times)):
    reswriter_short.writerow(["%.6g" % d for d in np.concatenate(([times[i]], [short_biomass[i]/float(model_with.scale)]))])
resfile_short.close()

resfile_short = open('short_biomass_percent' + '.csv', "w")
reswriter_short = csv.writer(resfile_short, delimiter="\t")
reswriter_short.writerow(['time'] + model_with.species[-15:])
times = [i * 0.5 for i in range(60*len(short)+1)]
for i in range(len(times)):
    reswriter_short.writerow(["%.6g" % d for d in np.concatenate(([times[i]], short_biomass_percent[i][:]))])
resfile_short.close()

model_with.plotResults('Et', plot_biomass=True, results=[short_final[0], short_final[1]])
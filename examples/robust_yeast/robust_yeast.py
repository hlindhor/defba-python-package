##################################################################
# Copyright (C) 2018 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
import numpy as np
import defba

# load model from SBML
model = defba.readSBML('yeast_ram.xml', scale=1000)
name_of_uncertainty = model.addUncertainEnvironment('r_1979', decreasing=True)
external_conditions = [0,100000,100000]
init_biom, init_flux, mu = model.RBA(1,external_conditions, solver='gurobi')
# run a robust deFBA with the initial values.
robust = model.robust({name_of_uncertainty:[0.3]}, Tend = 20, stepsize = 0.33333, external_conditions=external_conditions, initial_biomass=init_biom,
                        prediction_horizon=5, robust_horizon=0.33333, solver='gurobi', output_name='tend20h0.3333p10c0.5r0.5unc0.3')

# calculate how the biomass amount developes and how the biomass composition changes
import csv
robust_biomass = np.zeros(len(robust[0]))
for time in range(len(robust[0])):
    robust_biomass[time] = np.dot(model.biomass[-386:], robust[1][time, -386:])

bm_percent = np.zeros((len(robust[0]), model.numbers['proteins']))
for time in range(len(robust[0])):
    for position in range(model.numbers['proteins']):
        bm_percent[time][position] = 100.0 * model.biomass[position -386]* robust[1][time][position - 386] / robust_biomass[time]

resfile = open('robust_biomass_final' + '.csv', "w")
reswriter = csv.writer(resfile, delimiter="\t")
reswriter.writerow(['time'] + ['biomass'])
times = robust[0]
for i in range(len(times)):
    reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], [robust_biomass[i]/float(model.scale)]))])
resfile.close()

resfile_robust = open('robust_biomass_percent' + '.csv', "w")
reswriter_robust = csv.writer(resfile_robust, delimiter="\t")
reswriter_robust.writerow(['time'] + model.species[-386:])
for i in range(len(times)):
    reswriter_robust.writerow(["%.6g" % d for d in np.concatenate(([times[i]], bm_percent[i][:]))])
resfile_robust.close()

# Here we added the possibility to generate the short-term results for comparison
# short = model.shortterm(Tend = 20, stepsize = 0.33333, external_conditions=external_conditions, initial_biomass=init_biom,
#                         prediction_horizon=5, control_horizon=0.3333, solver='gurobi', midpoint=True, fixed_horizons=True,
#                         output_name='tend20p10c0.25h0.3333')
# times = [0.33333 * i for i in range(len(short[0]))]
#
# import csv
# short_biomass = np.zeros(len(short[0]))
# for time in range(len(short[0])):
#     short_biomass[time] = np.dot(model.biomass[-386:], short[1][time, -386:])
#
# bm_percent = np.zeros((len(short[0]), model.numbers['proteins']))
# for time in range(len(short[0])):
#     for position in range(model.numbers['proteins']):
#         bm_percent[time][position] = 100.0 * model.biomass[position -386]* short[1][time][position - 386] / short_biomass[time]
#
# resfile = open('short_biomass_final' + '.csv', "w")
# reswriter = csv.writer(resfile, delimiter="\t")
# reswriter.writerow(['time'] + ['biomass'])
# for i in range(len(times)):
#     reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], [short_biomass[i]/float(model.scale)]))])
# resfile.close()
#
# resfile_short = open('short_biomass_percent' + '.csv', "w")
# reswriter_short = csv.writer(resfile_short, delimiter="\t")
# reswriter_short.writerow(['time'] + model.species[-386:])
# for i in range(len(times)):
#     reswriter_short.writerow(["%.6g" % d for d in np.concatenate(([times[i]], bm_percent[i][:]))])
# resfile_short.close()
#
# import csv
# difference_biomass = np.zeros(len(difference[0]))
# for time in range(len(difference[0])):
#     difference_biomass[time] = np.dot(model.biomass[-386:], difference[1][time, -386:])
#
# bm_percent = np.zeros((len(difference[0]), model.numbers['proteins']))
# for time in range(len(difference[0])):
#     for position in range(model.numbers['proteins']):
#         bm_percent[time][position] = 100.0 * model.biomass[position -386]* difference[1][time][position - 386] / difference_biomass[time]
#
# resfile = open('d_biomass_final' + '.csv', "w")
# reswriter = csv.writer(resfile, delimiter="\t")
# reswriter.writerow(['time'] + ['biomass'])
# times = difference[0]
# for i in range(len(times)):
#     reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], [difference_biomass[i]/float(model.scale)]))])
# resfile.close()
#
# resfile_difference = open('difference_biomass_percent' + '.csv', "w")
# reswriter_difference = csv.writer(resfile_difference, delimiter="\t")
# reswriter_difference.writerow(['time'] + model.species[-386:])
# for i in range(len(times)):
#     reswriter_difference.writerow(["%.6g" % d for d in np.concatenate(([times[i]], bm_percent[i][:]))])
# resfile_difference.close()
#
# times = [i * 0.33333 for i in range(61)]
#
# import csv
# res_biomass = np.zeros(len(res[0]))
# for x, time in enumerate(times):
#     res_biomass[x] = np.dot(model.biomass[-386:], res[1][1].get_state(time)[-386:])
#
# bm_percent = np.zeros((len(res[0]), model.numbers['proteins']))
# for x, time in enumerate(times):
#     for position in range(model.numbers['proteins']):
#         bm_percent[x][position] = 100.0 * model.biomass[position -386]* res[1][1].get_state(time)[position - 386] / res_biomass[x]
#
# resfile = open('res_biomass_final' + '.csv', "w")
# reswriter = csv.writer(resfile, delimiter="\t")
# reswriter.writerow(['time'] + ['biomass'])
# for i in range(len(times)):
#     reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], [res_biomass[i]/float(model.scale)]))])
# resfile.close()
#
# resfile_res = open('res_biomass_percent' + '.csv', "w")
# reswriter_res = csv.writer(resfile_res, delimiter="\t")
# reswriter_res.writerow(['time'] + model.species[-386:])
# for i in range(len(times)):
#     reswriter_res.writerow(["%.6g" % d for d in np.concatenate(([times[i]], bm_percent[i][:]))])
# resfile_res.close()

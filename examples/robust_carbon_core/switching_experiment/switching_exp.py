##################################################################
# Copyright (C) 2018 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
"""
This file contains a switching experiment with the carbon core model
Every 30minutes the growth conditions switch from aerobic conditions to anaerobic conditions
To realize this we load two different models, one with oxygen modeled and one without.
"""


import numpy as np
import csv
import defba

# We start off by modeling the system with Oxygen available
model_with = defba.readSBML('carbon_core_with_O2.xml', scale=1)
# add uncertain environment by constraining the oxygen uptake
env_name_with = model_with.addUncertainEnvironment('v4', decreasing=True)
# Initial values were calculated by an RBA in another example. We will only use the external species part.
ics = {'Dext': 0, 'Eext': 0, 'Fext': 0, 'Hext': 0, 'O2ext': 0, 'Carb1': 2000000000, 'Carb2': 0, 'Eb': 3.412387411324629e-05,
       'Ec': 3.145732765886909e-05, 'Ed': 1.1967758585195503e-05, 'Ee': 0.0, 'Ef': 2.666546454377196e-06, 'Eg': 1.4722928904299521e-05,
       'Eh': 1.4723443408691826e-05, 'En': 0.0, 'Et': 3.3466739491569915e-05, 'R': 3.062174454205124e-05, 'S': 0.00023333333333333333,
       'Tc1': 2.0474324467947774e-05, 'Tc2': 0.0, 'Tf': 0.0, 'Th': 0.0}
# Load another model with Oxygen as dynamic species
model_without = defba.readSBML('carbon_core_with_O2.xml', scale=1)
# Calculate initial biomass
RBA_Init = model_without.RBA(1, ics)
# add uncertainty to create a scenario in which oxygen might become available again
env_name_without = model_without.addUncertainEnvironment('v4', decreasing=False)
next_scenario = 'without_O2.xml'
# Let the experiment run in 30 min intervals. stepsize = 2 min.
# oxygen dynamics are limited to on/off. Carb1 dynamics also disabled to not limit growth.
robust = {}
# results format = [[times], [species], [reactions]]
robust[0] = model_with.robust({env_name_with: [0.5]}, Tend=30, stepsize=0.5, external_conditions=ics, initial_biomass=RBA_Init[0],
                              solver='gurobi', output_name='0', prediction_horizon=33.0, export=False, robust_horizon=1.5)
# Order of species might vary when reloading models from SBML. Therefore, we ensure the order in both models is identical here
species_order_with = list(model_with.species)
reactions_order_with = list(model_with.reactions)

species_order_without = list(model_without.species)
reactions_order_without = list(model_without.reactions)

def switch_input_to_order(output_order, input_order, input_data):
    output_data = np.zeros(len(output_order))
    for row, oo in enumerate(output_order):
        try:
            output_data[row] = input_data[input_order.index(oo)]
        except ValueError:
            pass
    return output_data

# Artificial loss of biomass between switches of environment can be modeled with the multiplier variable
multiplier = 1
for i in range(5):
    current_biomass_robust = np.dot(model_with.biomass[-15:], robust[i][1][-1, -15:])
    multiplier_robust = 1
    if next_scenario == 'without_O2.xml':
        last_robust_species = multiplier_robust*switch_input_to_order(species_order_without, species_order_with, robust[i][1][-1, :])
        last_robust_fluxes = multiplier_robust*np.array(robust[i][2][-1, :])
        robust[i + 1] = model_without.robust({env_name_without: [0.5]}, Tend=30, stepsize=0.5,
                                             external_conditions=last_robust_species[:model_without.numbers['ext_species']],
                                             initial_biomass=last_robust_species[-model_without.numbers['proteins']:], solver='gurobi',
                                             initial_fluxes=last_robust_fluxes, output_name=str(i + 1), prediction_horizon=33.0, export=False, robust_horizon=1.5)
        next_scenario = 'with_O2.xml'
    elif next_scenario == 'with_O2.xml':
        last_robust_species = multiplier_robust * switch_input_to_order(species_order_with, species_order_without, robust[i][1][-1, :])
        last_robust_fluxes = multiplier_robust * robust[i][2][-1, :]
        robust[i + 1] = model_with.robust({env_name_with: [0.5]}, Tend=30, stepsize=0.5,
                                          external_conditions=last_robust_species[:model_with.numbers['ext_species']],
                                          initial_biomass=last_robust_species[-model_with.numbers['proteins']:],
                                          initial_fluxes=last_robust_fluxes,
                                          solver='gurobi', output_name=str(i + 1), prediction_horizon=33.0, export=False,
                                          robust_horizon=1.5)
        next_scenario = 'without_O2.xml'

# Combine all results and export those
robust_final = [[i * 0.5 for i in range(60*6-1)], np.atleast_2d(np.array(robust[0][1][:-2, :])), np.atleast_2d(np.array(robust[0][2][:-2, :]))]
for number in range(1, 6):
    if number % 2 == 1:
        robust_final[1] = np.append(robust_final[1], np.atleast_2d(robust[number][1][:60,:]), axis=0)
        robust_final[2] = np.append(robust_final[2], np.atleast_2d(robust[number][2][:60,:]), axis=0)
    else:
        robust_final[1] = np.append(robust_final[1], np.atleast_2d(robust[number][1][:60, :]), axis=0)
        robust_final[2] = np.append(robust_final[2], np.atleast_2d(robust[number][2][:60, :]), axis=0)
defba.defbamodel.export_states(robust_final[0], robust_final[1], model_with.species,
                              model_with.scale, model_with.numbers['ext_species'] + model_with.numbers['metab_species'],
                               file_name='robust_final')
defba.defbamodel.export_fluxes(robust_final[0], robust_final[2], model_with.reactions, model_with.scale,
                               model_with.numbers['metab_reactions'] + model_with.numbers['exch_reactions'], file_name='robust_final_reac')

robust_biomass = np.zeros(len(robust_final[0]))
for time in range(len(robust_final[0])):
    robust_biomass[time] = np.dot(model_with.biomass[-15:], robust_final[1][time, -15:])

robust_biomass_percent = np.zeros((len(robust_final[0]), model_with.numbers['proteins']))
for time in range(len(robust_final[0])):
    for position in range(model_with.numbers['proteins']):
        robust_biomass_percent[time][position] = 100.0 * model_with.biomass[position -15]* robust_final[1][time][position - 15] / robust_biomass[time]

resfile = open('robust_biomass_final' + '.csv', "w")
reswriter = csv.writer(resfile, delimiter="\t")
reswriter.writerow(['time'] + ['biomass'])
times = [i * 0.5 for i in range(60*len(robust)-1)]
for i in range(len(times)):
    reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], [robust_biomass[i]/float(model_with.scale)]))])
resfile.close()

resfile_robust = open('robust_biomass_percent' + '.csv', "w")
reswriter_robust = csv.writer(resfile_robust, delimiter="\t")
reswriter_robust.writerow(['time'] + model_with.species[-15:])
times = [i * 0.5 for i in range(60*len(robust)-1)]
for i in range(len(times)):
    reswriter_robust.writerow(["%.6g" % d for d in np.concatenate(([times[i]], robust_biomass_percent[i][:]))])
resfile_robust.close()

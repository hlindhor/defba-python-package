##################################################################
# Copyright (C) 2018 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
"""
This example uses the Carbon Core model to show how the uncertain environments are used.
The model contains two carbon sources Carb1 and Carb2.
Carb1 is the preferred source as its transporter Tc1 is more effective and cheaper.
We assume Carb1 might deplete at any time, while in reality both sources will be available at all times.
We use a modified model in which O2 is always available
"""

import numpy as np
import defba

# load the model file
model = defba.readSBML('cc_without_O2.xml', scale=100)
# add the uncertainty in the environment by constraining the relevant uptake
uncertain_environment_name = model.addUncertainEnvironment('v1', decreasing=True)
# Define external conditions with only carbon sources present
ext_cond = {'Dext':0, 'Eext':0, 'Fext':0, 'Hext':0, 'Carb1':1000000, 'Carb2':1000000}
# calculate initial values via RBA
RBA_init = model.RBA(1, ext_cond)
robust_initial_biomass = RBA_init[0]
# preparation time set to 0.067*30
results_robust = model.robust({uncertain_environment_name:[0.06666666666666667]}, Tend=50, stepsize= 0.5, RBA_init=False, external_conditions=ext_cond, solver='gurobi',
                           output_name='Carb1_depleting', initial_biomass=robust_initial_biomass, prediction_horizon = 30,
                           robust_horizon=1.5)
# plot results
model.plotResults(['Tc1','Tc2', 'Carb1', 'Carb2'], reactions=['v1', 'v2'], plot_biomass=True)

# Calculate Biomass composition and biomass increase and export those
import csv
robust_biomass = np.zeros(len(results_robust[0]))
for time in range(len(results_robust[0])):
    robust_biomass[time] = np.dot(model.biomass[-15:], results_robust[1][time, -15:])

bm_percent = np.zeros((len(results_robust[0]), model.numbers['proteins']))
for time in range(len(results_robust[0])):
    for position in range(model.numbers['proteins']):
        bm_percent[time][position] = 100.0 * model.biomass[position -15]* results_robust[1][time][position - 15] / robust_biomass[time]

resfile = open('robust_biomass_final' + '.csv', "w")
reswriter = csv.writer(resfile, delimiter="\t")
reswriter.writerow(['time'] + ['biomass'])
times = results_robust[0]
for i in range(len(times)):
    reswriter.writerow(["%.6g" % d for d in np.concatenate(([times[i]], [robust_biomass[i]/float(model.scale)]))])
resfile.close()

resfile_robust = open('robust_biomass_percent' + '.csv', "w")
reswriter_robust = csv.writer(resfile_robust, delimiter="\t")
reswriter_robust.writerow(['time'] + model.species[-15:])
for i in range(len(times)):
    reswriter_robust.writerow(["%.6g" % d for d in np.concatenate(([times[i]], bm_percent[i][:]))])
resfile_robust.close()
##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
"""
This minimal model shows you how to import a deFBA from a SBML file following the RAM standard [1].

[1] https://www.fairdomhub.org/sops/304
"""
import numpy as np
import defba
# create the DefbaModel object via the SBML import
model = defba.readSBML('enzymatic_growth.xml', scale=100)
# choose initial values
initial_values = {'M':0.1, 'E':0.1}
# generate standard deFBA results
model.external_conditions = [50000]
results = model.deFBA(stepsize = 0.1, Tend = 3, solver='gurobi', output_name='original', initial_biomass={'M':0.1, 'E':0.1},
                      external_conditions=[50000], midpoint=False)
model.plotResults(model.species, model.reactions, plot_biomass = True)
# pred, control = model.calcPredictionHorizon(biomass_comp=initial_values, solver='gurobi')
# test short-term deFBA
results_short = model.shortterm(stepsize = 0.1, Tend = 3, solver='gurobi', output_name='short', external_conditions=[500000],
                                prediction_horizon=3.25, control_horizon=1.45, fixed_horizons=True, midpoint=False, initial_biomass=[1,1])
# plot the results of the short-term deFBA
model.plotResults(model.species, model.reactions, results_short, plot_biomass=True)
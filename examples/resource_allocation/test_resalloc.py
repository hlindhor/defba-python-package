##################################################################
# Copyright (C) 2017 Henning Lindhorst <henning.lindhorst@ovgu.de>
#
# This file is part of the {DEFBA Python Package}.
#
# This project is published under (CC BY-NC-ND 3.0)
##################################################################
"""
This resource allocation model presents all features of the RAM annotation standard [1] for SBML.
Maintenance constraints and Biomass Composition Constraints are shown.
We present the RBA for calculation of initial values.

[1] https://www.fairdomhub.org/sops/304
"""
import numpy as np
import defba
# load the model
model = defba.readSBML('resalloc.xml', scale=10)
# determine amount of biomass at start of simulation in mol
initial_biomass_amount = 10
# calculate maximal growth rate and determine optimal biomass composition for a rich medium
initial_values, initial_fluxes, mu = model.RBA(initial_biomass_amount, [10000,100000], solver='gurobi', mu=1)
# automatically calculate an optimal prediction horizon for short-term deFBA applications
pred, control = model.calcPredictionHorizon(initial_values, solver='gurobi')
# run a regular deFBA and export the results to the files ram1_species.csv and ram1_fluxes.csv
results = model.deFBA(stepsize = 0.1, Tend = 1, solver='gurobi',output_name='ram1' )
# plot results
model.plotResults(model.species, reactions=model.reactions, plot_biomass=True)